package com.dcostap.game

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.dcostap.engine.utils.*
import com.dcostap.game.Main.Estado.*
import kotlin.math.absoluteValue

/** Created by Darius on 02-Dec-20. */
class Philosopher(val num: Int, val main: Main) : Runnable {

	private var ultimaComida = System.currentTimeMillis()
	private var step = 0;

	override fun run() {
		while (true) {
			simpleVersion()
		}
	}

	private fun simpleVersion() {
		meditar()

		main.mutex.acquire()
		main.estados[num] = ESPERANDO_COMER
		main.mutex.release()

		main.tenedores[num].acquire()
		main.tenedores[derecha].acquire()

		main.mutex.acquire()
		main.estados[num] = COMIENDO
		main.mutex.release()

		comer()

		main.mutex.acquire()
		main.estados[num] = MEDITANDO
		main.mutex.release()

		main.tenedores[num].release()
		main.tenedores[derecha].release()
	}

	private fun comer() {
		ultimaComida = System.currentTimeMillis()
		sleepThread()
	}

	private fun meditar() {
		sleepThread()
	}

	private fun sleepThread() {
		Thread.sleep(randomInt(1, 5) * 1500L)
	}

	fun update() {
		if (main.estados[num] == COMIENDO && main.estados[derecha] == COMIENDO)
			throw Exception("Algún filósofo ha hecho trampa con los tenedores")
	}

	override fun toString() = "Philosopher $num, in step $step, currently ${main.estados[num]} "

	fun draw(gd: GameDrawer) {
		val pos = Vector2(0f, 0f)
		val angle = (360f / 5f) * num
		pos.addAngleMovement(300f, angle)
		gd.rotation = angle

		if (main.estados[num] == MEDITANDO)
			gd.alpha = 0.4f
		gd.draw(
				"filosofo" + if (main.estados[num] == COMIENDO) "Red" else "",
				pos, scaleY = 0.4f, scaleX = 0.4f,
				centerOnXAxis = true, centerOnYAxis = true,
				centerOriginOnXAxis = true, centerOriginOnYAxis = true
		)

		gd.resetAlpha()
		when (main.estados[num]) {
			COMIENDO -> {
				gd.alpha = 0.8f
				gd.draw("tallarines", pos, scaleY = 0.3f, scaleX = 0.3f,
						centerOnXAxis = true, centerOnYAxis = true,
						centerOriginOnXAxis = true, centerOriginOnYAxis = true)
				gd.resetAlpha()
			}
			MEDITANDO -> {
				val oldRotation = gd.rotation
				gd.rotation = 0f
				gd.draw("meditating", pos, scaleY = 0.2f, scaleX = 0.2f,
						centerOnXAxis = true, centerOnYAxis = true,
						centerOriginOnXAxis = true, centerOriginOnYAxis = true)
				gd.rotation = oldRotation
			}
		}

		val timeSince = ((System.currentTimeMillis() - ultimaComida) / 1000f) * Main.speed
		val fontScale = timeSince.map(0f, 120f, 1f, 1.3f)
//		if (timeSince > 10f)
//			gd.drawText(
//					"tiempo sin comer:\n${timeSince.format(0)} segundos",
//					pos.x,
//					pos.y,
//					main.assets.fontRoboto,
//					Color.WHITE,
//					scaleX = fontScale,
//					scaleY = fontScale
//			)

//		gd.drawText("$num   step: $step", pos.x - 35f, pos.y + 95f, main.assets.fontRoboto, Color.YELLOW, 1f, 1f)
		if (step == 1)
			gd.drawText("ESPERANDO", pos.x - 30f, pos.y + 120f, main.assets.fontRoboto, Color.RED, 1.5f, 1.5f)

		pos.set(0f, 0f)
		pos.addAngleMovement(300f, angle + 35f)
		gd.draw(
				"tenedor" + if (main.estados[num] == COMIENDO || main.estados[derecha] == COMIENDO) "Red" else "",
				pos,
				scaleY = 0.3f,
				scaleX = 0.3f,
				centerOnXAxis = true,
				centerOnYAxis = true,
				centerOriginOnXAxis = true,
				centerOriginOnYAxis = true
		)

		gd.reset()
	}

	val derecha get() = (num + 1) % 5
	val izquierda: Int
		get() {
			return if (num - 1 >= 0) (num - 1) % 5
			else 5 + (num - 1)
		}

	val derechaDerecha get() = (num + 2) % 5
	val izquierdaIzquierda: Int
		get() {
			return if (num - 2 >= 0) (num - 2) % 5
			else 5 + (num - 2)
		}
}