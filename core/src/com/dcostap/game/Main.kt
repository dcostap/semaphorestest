package com.dcostap.game

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.Engine
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.screens.BaseScreenWithUI
import java.util.concurrent.Semaphore

/** Created by Darius on 28/12/2017 */
class Main(engine: Engine) : BaseScreenWithUI(engine) {
	var timeElapsed = 0L

	override fun createViewport(): Viewport {
		return ScreenViewport()
	}

	override fun createStage(): Stage {
		return Stage(ScreenViewport())
	}

	enum class Estado {
		MEDITANDO, ESPERANDO_COMER, COMIENDO
	}

	val mutex = Semaphore(1)
	val tenedores = Array(5) { Semaphore(1) }
	val estados = Array(5) { Estado.MEDITANDO }
	val philosophers = Array(5) { Philosopher(it, this) }
	val threads = mutableListOf<Thread>()
	var contador: Int = 0

	init {
		for (p in philosophers) {
			Thread(p).let {
				threads.add(it)
				it.start()
			}
		}

		stage.addActor(Table().also {
			it.setFillParent(true)

			it.bottom()
			it.add(Utils.visUI_customIntSpinner("speed", 1, { speed = it.toFloat() }, { speed.toInt() })).growX().bottom()
		})
	}

	override fun dispose() {
		super.dispose()
		threads.forEach { it.stop() }
	}

	override fun resize(width: Int, height: Int) {
		super.resize(width, height)

		if (width == 0 || height == 0) return
	}

	override fun drawWorld(gd: GameDrawer, delta: Float) {
		Utils.clearScreen(0, 0, 0)

		if (Input.Keys.R.isKeyJustPressed())
			engine.screen = Main(engine)

		for (p in philosophers) {
			p.update()
			p.draw(gd)
		}

		timeElapsed += ((delta * speed) * 1000).toLong()

//		gd.drawText(
//				"Días pasados en la simulación: ${(((timeElapsed / 1000L) / 3600L) / 24).toInt()}",
//				-440f,
//				370f,
//				assets.fontRoboto,
//				Color.WHITE,
//				1.2f,
//				1.2f
//		)
//
//		gd.drawText(
//				"Counter: $contador",
//				-300f,
//				370f,
//				assets.fontRoboto,
//				Color.WHITE,
//				1.2f,
//				1.2f
//		)
	}

	companion object {
		var speed = 1f
	}
}
