package com.dcostap

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.dcostap.engine.utils.addAction
import com.dcostap.engine.utils.ui.AnimatedExtLabel

/**
 * Created by Darius on 26/02/2018.
 */
class LoadingScreen(val engine: Engine, val nextScreen: () -> Screen) : Screen {
	var transitioning = false
	var stage: Stage
	var startLoading = false

	val mainTable = Table()

	init {
		if (!Engine.IS_RELEASE) startLoading = true

		stage = Stage(ExtendViewport(100f, 100f))

		val loadingFont = BitmapFont(Gdx.files.internal("fontsWithPng/darePixel_s10.fnt"))
		val loadingFont2 = BitmapFont(Gdx.files.internal("fontsWithPng/equipment_s16.fnt"))
		val loadingFont3 = BitmapFont(Gdx.files.internal("fontsWithPng/darePixelSmall_s7.fnt"))
		val loading = AnimatedExtLabel("{WAVE=0.8;2;1}loading...{ENDWAVE}", loadingFont, Color(1f, 1f, 1f, 1f), Align.center, false).also {
			it.color.a = 0f
			it.addAction(Actions.delay(1f), Actions.run {
				it.addAction(Actions.alpha(1f, 0.2f, Interpolation.pow2))
				it.restart()
				it.addAction(Actions.delay(2.2f, Actions.run { startLoading = true }))
			})
		}
		val name = AnimatedExtLabel("{EASE=-5;1.3;1}Pixel Imperfect Games{ENDEASE}",
				loadingFont2, Color(0.6f, 0.75f, 1f, 1f), Align.center).also {
			it.color.a = 0f
			it.addAction(Actions.delay(0.2f), Actions.run {
				it.addAction(Actions.alpha(1f, 0.2f, Interpolation.pow2))
				it.restart()
			})
		}
		stage.addActor(mainTable.also {
			it.setFillParent(true)
			it.add(AnimatedExtLabel("{EASE=0.4;0.3;1}Follow me on twitter for updates on my game projects{ENDEASE}",
					loadingFont3, Color(0.6f, 0.75f, 1f, 1f), Align.center).also {
				it.color.a = 0f
				it.addAction(Actions.delay(1f), Actions.run {
					it.addAction(Actions.alpha(1f, 0.2f, Interpolation.pow2))
					it.restart()
				})
			}).padTop(0f)
			it.row()
			it.add(AnimatedExtLabel("{EASE=0.4;0.4;0}@darencius3{ENDEASE}",
					loadingFont3, Color(0.6f, 0.75f, 1f, 1f), Align.center).also {
				it.color.a = 0f
				it.addAction(Actions.delay(2.4f), Actions.run {
					it.addAction(Actions.alpha(1f, 0.2f, Interpolation.pow2))
					it.restart()
				})
			}).padBottom(18f)
			it.row()
			it.add(name)
			it.row()
			it.add(loading).padTop(50f)
		})
	}

	override fun render(delta: Float) {
		if (startLoading) {
			if (engine.assets.processAssetLoading()) {
				startLoading = false
				engine.screen = nextScreen()
			}
		}

		// clear screen
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

		stage.act(delta)
		stage.draw()
	}

	override fun show() {

	}

	override fun resize(width: Int, height: Int) {
		stage.viewport.update(width, height)
	}

	override fun pause() {

	}

	override fun resume() {

	}

	override fun hide() {

	}

	override fun dispose() {
		stage.dispose()
	}
}
