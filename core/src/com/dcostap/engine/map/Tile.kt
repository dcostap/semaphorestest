package com.dcostap.engine.map

import com.badlogic.gdx.utils.JsonValue
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.JsonSavedObject
import com.dcostap.engine.utils.addChildValue
import com.dcostap.engine.utils.input.InputController

/**
 * Created by Darius on 11/01/2018
 */
abstract class Tile(val cell: MapCell) : DrawableSortable {
	var rotation: Int = 0

	/** If using terrains, check [MapCell.terrainDepth] */
	var depth: Int = 0

	override fun getDrawingY() = cell.yUnits
	override fun getDrawingDepth() = depth
	override fun getDrawingYDepth() = 0

	open fun save(): JsonValue {
		val json = JsonSavedObject()
		json.addChildValue("class", this::javaClass.name)
		json.addChildValue("rotation", rotation)
		return json
	}

	override fun handleTouchInput(inputController: InputController, stageInput: InputController?): Boolean {
		return false
	}
}

class SprTile(cell: MapCell, var tileSprName: String) : Tile(cell) {
	override fun draw(gd: GameDrawer, delta: Float) {
		// scale up to avoid edge bleeding, or small crevices
		gd.draw(tileSprName, cell.xUnits, cell.yUnits, rotation = rotation.toFloat(), scaleX = 1.03f, scaleY = 1.03f)
	}

	override fun save(): JsonValue {
		return super.save().also { json ->
			json.addChildValue("tileSprName", tileSprName)
		}
	}

	companion object {
		fun load(json: JsonValue, cell: MapCell): Tile {
			val tileSprName = json.getString("tileSprName")
			val tile = SprTile(cell, tileSprName)
			tile.rotation = json.getInt("rotation")

			tile.tileSprName = tileSprName
			return tile
		}
	}
}

class CustomTile(cell: MapCell, val drawFunction: (GameDrawer, Float) -> Unit) : Tile(cell) {
	override fun draw(gd: GameDrawer, delta: Float) {
		drawFunction(gd, delta)
	}
}
