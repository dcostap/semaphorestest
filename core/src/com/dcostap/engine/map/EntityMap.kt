package com.dcostap.engine.map

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.Pool
import com.badlogic.gdx.utils.Pools
import com.dcostap.Engine
import com.dcostap.engine.debug.logWarning
import com.dcostap.engine.map.EntityMap.Companion.loadEmpty
import com.dcostap.engine.map.EntityMap.Companion.loadFromJson
import com.dcostap.engine.map.entities.Entity
import com.dcostap.engine.map.map_loading.CustomProperties
import com.dcostap.engine.map.map_loading.EntityLoaderFromClass
import com.dcostap.engine.map.map_loading.JsonMapInfo
import com.dcostap.engine.map.map_loading.TiledMapLoader
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.actions.ActionsUpdater
import com.dcostap.engine.utils.input.InputController
import com.dcostap.engine.utils.screens.BaseScreen
import com.dcostap.engine.utils.screens.BaseScreenWithUI
import ktx.collections.*
import java.util.*
import kotlin.collections.HashSet

/**
 * Created by Darius on 14/09/2017.
 *
 * Create the map with [loadEmpty] or [loadFromJson]. Maps always have one default [CellLayer] but you may add more secondary ones
 * in those methods. Not recommended to add more at runtime accessing [cellLayers] (new cell layers won't have updated info on existing entities)
 *
 * When drawing, Entities and Tiles will be sorted according to their depth or y variables. (see [DrawableSortable])
 * Each mapCell may have more than one Tile. See [MapCell.tiles] for info on how tiles (graphical info on each cell) work
 *
 * For saving map info, check the serialization methods here and see [JsonMapInfo]'s description
 *
 * @param preciseDrawingCulling if true bounding boxes are checked for collision. If false only lazy checking is done, which will
 * result in drawing stuff that is actually outside of the camera (more drawing, no collision checking). Precise might be faster in
 * certain situations.
 *
 * @param collTreeSize increase the value if entity density is low. That is, you want lower values when many entities are going to end
 * closer to other entities. By default, chooses a value based on map size.
 */
open class EntityMap @JvmOverloads private constructor(val screen: BaseScreen,
													   widthCells: Int, heightCells: Int,
													   var preciseDrawingCulling: Boolean,
													   private val collTreeSize: Int)
	: Drawable, Updatable, Disposable, BaseScreen.ResizeListener, MapCell.ChangeListener {
	class SecondaryCellLayer(val cellSizePixels: Int)

	companion object {
		/** @see loadFromJson */
		fun loadEmpty(screen: BaseScreen, cellSizePixels: Int, widthCells: Int, heightCells: Int,
					  preciseDrawingCulling: Boolean = false, collTreeSize: Int = -1,
					  extraCellLayers: Array<SecondaryCellLayer>? = null): EntityMap {
			var collTreeSize = collTreeSize
			if (collTreeSize <= 0) collTreeSize = Math.max(Math.max(widthCells * cellSizePixels, heightCells * cellSizePixels) / 40f, 100.pixelsToUnits).toInt()
			return EntityMap(screen, widthCells * cellSizePixels, heightCells * cellSizePixels, preciseDrawingCulling, collTreeSize).apply {
				cellLayers.add(CellLayer(this, widthCells, heightCells, cellSizePixels))
				extraCellLayers?.forEach { addCellLayerFill(it.cellSizePixels) }
			}
		}

		/** Whenever loading, empty or from json, one default [CellLayer] is created. Note that when loading from json, each Tiled
		 * tile layer will load into that same default [CellLayer]. This makes sense since you can't specify more tile layers with different
		 * grid sizes in Tiled. These extra tile layers may add more tiles to each map cell. Define exact behavior with [JsonMapInfo.TileLoadBehavior].
		 *
		 * @param extraCellLayers To add more custom [CellLayer] on top of the default one */
		fun loadFromJson(screen: BaseScreen, jsonMapInfo: JsonMapInfo, tiledMapLoader: TiledMapLoader,
						 loadMapProperties: Boolean = true, loadInvisibleLayers: Boolean = false,
						 preciseDrawingCulling: Boolean = false, collTreeSize: Int = -1,
						 tileLoadBehavior: JsonMapInfo.TileLoadBehavior = JsonMapInfo.TileLoadBehavior.APPEND_NEW_TILE_TO_EXISTING_ONES,
						 extraCellLayers: Array<SecondaryCellLayer>? = null,
						 allowLayer: (JsonMapInfo.JsonMapLayer) -> Boolean = { true }): EntityMap {
			return loadEmpty(screen, jsonMapInfo.mapCellSize, jsonMapInfo.mapWidth, jsonMapInfo.mapHeight,
					preciseDrawingCulling, collTreeSize, extraCellLayers).apply {
				jsonMapInfo.loadMap(this, tiledMapLoader, 0f, 0f,
						loadMapProperties, loadInvisibleLayers, tileLoadBehavior, allowLayer = allowLayer, cellLayer = mainCellLayer)
			}
		}
	}

	fun appendFromJson(jsonMapInfo: JsonMapInfo, tiledMapLoader: TiledMapLoader,
					   offsetXPixels: Float, offsetYPixels: Float,
					   tileLoadBehavior: JsonMapInfo.TileLoadBehavior,
					   loadInvisibleLayers: Boolean = false,
					   destinationCellLayer: CellLayer = mainCellLayer,
					   allowLayer: (JsonMapInfo.JsonMapLayer) -> Boolean = { true }) {
		if (!cellLayers.contains(destinationCellLayer)) cellLayers.add(destinationCellLayer)
		jsonMapInfo.loadMap(this, tiledMapLoader, offsetXPixels, offsetYPixels,
				false, loadInvisibleLayers, tileLoadBehavior, destinationCellLayer, allowLayer)
	}

	private fun addCellLayerFill(cellSizePixels: Int) {
		cellLayers.add(CellLayer(this,
				widthCells * (this.cellSizePixels / cellSizePixels),
				heightCells * (this.cellSizePixels / cellSizePixels), cellSizePixels))
	}

	val cells get() = mainCellLayer.cells

	val cellSizePixels get() = mainCellLayer.cellSizePixels
	val widthPixels get() = mainCellLayer.widthPixels
	val heightPixels get() = mainCellLayer.heightPixels
	val widthUnits get() = mainCellLayer.widthUnits
	val heightUnits get() = mainCellLayer.heightUnits
	val widthCells get() = mainCellLayer.widthCells
	val heightCells get() = mainCellLayer.heightCells

	fun isInsideMapUnits(x: Number, y: Number): Boolean {
		return mainCellLayer.isUnitPosInsideMap(x, y)
	}

	fun isInsideMapCells(x: Number, y: Number): Boolean {
		return mainCellLayer.isCellPosInsideMap(x, y)
	}

	fun getMapCellFromUnits(x: Number, y: Number): MapCell? {
		return mainCellLayer.getMapCellFromUnits(x, y)
	}

	fun getMapCellFromUnits(pos: Vector2): MapCell? {
		return mainCellLayer.getMapCellFromUnits(pos.x, pos.y)
	}

	var secondsPassed = 0.0
	val worldViewport get() = screen.worldViewport
	private val worldCamera: OrthographicCamera = worldViewport.camera as OrthographicCamera
	val worldInput: InputController get() = screen.worldInput
	val stageInput: InputController? get() = (screen as? BaseScreenWithUI)?.stageInput

	val entityList = GdxSet<Entity>()

	val actions = ActionsUpdater()

	private val dynamicEntityList = GdxSet<Entity>()
	private val dynamicEntityListDraw = GdxArray<Entity>()
	private val toBeRemoved = GdxSet<Entity>()
	private val toBeDisposed = GdxSet<Entity>()
	private val toBeAdded = GdxSet<Entity>()

	var customProperties = CustomProperties()

	var drawingCameraPadding = 80.pixelsToUnits

	val cellLayers = GdxArray<CellLayer>()
	val mainCellLayer get() = cellLayers.first()

	/** This collTree won't include entities with [Entity.providesCollidingInfo] set to false, so it's more efficient when
	 * it's desired to ignore Entities with colliding info not useful for others. */
	val collTreeForEntityColliding: CollisionTree

	/** This collTree is used internally by the map for culling and drawing, and includes all Entities */
	val collTreeForCullingAndDrawing: CollisionTree

	init {
		collTreeForEntityColliding = CollisionTree(collTreeSize, widthCells, heightCells, this)
		collTreeForCullingAndDrawing = CollisionTree(collTreeSize, widthCells, heightCells, this)
	}

	private val cameraRectangle = Rectangle()

	val notifierEndOfFrame = Notifier<ListenerEndOfFrame>()
	val notifierStartOfFrame = Notifier<ListenerStartOfFrame>()

	/** Allows you to draw something in worldViewport (not stage viewport) after all other stuff in the map is drawn.
	 * To draw in stage viewport use BaseScreenWithUI's drawOnUIBeforeStageNotifier */
	val notifierEndOfFrameDrawing = Notifier<ListenerEndOfFrameDrawing>()

	val notifierEntityAdditionRemoval = Notifier<ListenerEntityAdditionRemoval>()

	/** Notifies of any change on any map cell */
	val notifierGlobalCellInfo = Notifier<MapCell.ChangeListener>()

	val extraMapDrawables = GdxArray<DrawableSortable>()
	private val frameDrawOrders = GdxArray<PoolableDrawable>()

	private class PoolableDrawable : DrawableBase(), Pool.Poolable {
		var draw: (GameDrawer) -> Unit = {}
		var depth = 0
		var drawOrderY = 0f
		var yDepth: Int = 0

		override fun reset() {
			draw = {}
			depth = 0
			drawOrderY = 0f
			yDepth = 0
		}

		override fun getDrawingY(): Float {
			return drawOrderY
		}

		override fun getDrawingDepth(): Int {
			return depth
		}

		override fun getDrawingYDepth(): Int {
			return yDepth
		}

		override fun draw(gd: GameDrawer, delta: Float) {
			draw(gd)

			Pools.free(this)
		}
	}

	init {
		Pools.set(PoolableDrawable::class.java, object : Pool<PoolableDrawable>(5) {
			override fun newObject(): PoolableDrawable {
				return PoolableDrawable()
			}
		})
	}

	/** Easy way to draw something with custom depth / drawingY ([DrawableSortable.getDrawingY]) this frame.
	 * Alternative to using [extraMapDrawables], but this will create objects every frame (for the lambda created) */
	fun drawThisFrame(depth: Int = 0, drawOrderY: Float = 0f, yDepth: Int = 0, draw: (GameDrawer) -> Unit) {
		frameDrawOrders.add(Pools.obtain(PoolableDrawable::class.java).also { it.depth = depth; it.draw = draw; it.drawOrderY = drawOrderY; it.yDepth = yDepth })
	}

	private val activatedEntities = HashSet<Entity>()
	private val activatedEntitiesTemp = GdxArray<Entity>()

	private var wasUpdated = false

	var isUpdating = false
		private set

	override fun update(delta: Float) {
		wasUpdated = true
		isUpdating = true

		actions.update(delta)
		secondsPassed += delta

		updateCameraRectangle()
		collTreeForCullingAndDrawing.updateDynamicEntitiesThatChanged()
		collTreeForEntityColliding.updateDynamicEntitiesThatChanged()
		removeAndAddEntities()

		if (!notifierStartOfFrame.listeners.isEmpty)
			notifierStartOfFrame.notifyListeners { it.startOfFrameUpdate(delta) }

		activatedEntitiesTemp.clear()
		activatedEntitiesTemp.addAll(activatedEntities)
		for (ent in activatedEntitiesTemp) {
			if (!ent.isInMap || ent.map !== this || ent.isDisposed) continue
			ent.update(delta)

			ent.isInsideCamera = false

			if (!ent.isStatic && (ent.hasMoved || ent.boundingBoxes.values().any { it.wasModified })) {
				if (ent.providesCollidingInfo)
					collTreeForEntityColliding.addDynamicEntityThatChanged(ent)

				collTreeForCullingAndDrawing.addDynamicEntityThatChanged(ent)
			}

			ent.boundingBoxes.values().forEach { it.wasModified = false }
		}

		isUpdating = false
	}

	private val dummyEntArray2 = GdxArray<Entity>()

	/** Only call when the activation state actually changed value */
	fun updateEntityActivationState(ent: Entity) {
		if (ent.isInMap && ent.map == this) {
			if (ent.isActivated) {
				activatedEntities.add(ent)
			} else {
				activatedEntities.remove(ent)
			}
		}
	}

	/** Call this to force update of entityList */
	open fun removeAndAddEntities() {
		for (ent in toBeDisposed) ent.dispose()
		toBeDisposed.clear()

		val removedEnts = dummyEntArray2
		removedEnts.clear()
		removedEnts.addAll(toBeRemoved)
		toBeRemoved.clear()
		for (ent in removedEnts) {
			entityList.remove(ent)

			if (ent.isActivated) {
				activatedEntities.remove(ent)
			}

			if (ent.isStatic) {
				if (ent.providesCollidingInfo)
					collTreeForEntityColliding.removeStaticEntity(ent)

				collTreeForCullingAndDrawing.removeStaticEntity(ent)
			} else {
				if (ent.providesCollidingInfo) {
					dynamicEntityList.remove(ent)
					if (ent.hasMoved) collTreeForEntityColliding.removeDynamicEntityThatChanged(ent)
					collTreeForEntityColliding.removeEntity(ent, true)
				}

				dynamicEntityListDraw.removeValue(ent, true)
				collTreeForCullingAndDrawing.removeDynamicEntityThatChanged(ent)
				collTreeForCullingAndDrawing.removeEntity(ent, true)
			}

			cellLayers.forEach { it.updateCellsDueToEntity(ent, true) }

			ent.justRemovedFromMap(this)
			notifierEndOfFrame.listeners.removeAll { it == ent }
			notifierEndOfFrameDrawing.listeners.removeAll { it == ent }
			notifierEntityAdditionRemoval.listeners.removeAll { it == ent }
			notifierGlobalCellInfo.listeners.removeAll { it == ent }
			notifierStartOfFrame.listeners.removeAll { it == ent }

			if (ent.isInMap) throw RuntimeException("Entity $ent couldn't be removed from map." +
					" Make sure it doesn't override justRemovedFromMap() without calling super()")
			for (list in notifierEntityAdditionRemoval.listeners) list.entityRemoved(ent)
		}

		val addedEnts = removedEnts
		addedEnts.clear()
		addedEnts.addAll(toBeAdded)
		toBeAdded.clear()
		for (ent in addedEnts) {
			if (ent.isInMap) throw RuntimeException("Entity $ent was added to a map twice")

			ent.justBeforeAddingToMap(this)

			// entities with weird positions can have problems with shaky drawing when camera moves. This is a one-time fix when being added to a map
			ent.position.x = MathUtils.round(ent.position.x * Engine.PPU).pixelsToUnits
			ent.position.y = MathUtils.round(ent.position.y * Engine.PPU).pixelsToUnits

			entityList.add(ent)

			if (ent.isActivated) {
				activatedEntities.add(ent)
			}

			if (ent.isStatic) {
				if (ent.providesCollidingInfo)
					collTreeForEntityColliding.addStaticEntity(ent)

				collTreeForCullingAndDrawing.addStaticEntity(ent)
			} else {
				if (ent.providesCollidingInfo) {
					dynamicEntityList.add(ent)
					collTreeForEntityColliding.addEntity(ent, true)
				}

				dynamicEntityListDraw.add(ent)
				collTreeForCullingAndDrawing.addEntity(ent, true)
			}

			cellLayers.forEach { it.updateCellsDueToEntity(ent, false) }
			ent.justAddedToMap(this)
			if (!ent.isInMap) throw RuntimeException("Entity $ent couldn't be added to map." +
					" Make sure it doesn't override justAddedToMap() without calling super()")

			for (list in notifierEntityAdditionRemoval.listeners) list.entityAdded(ent)
		}
	}

	val pathfinder = Pathfinder()

	private val drawables = GdxArray<DrawableSortable>()
	private val orderComparator = ComparatorByDepthAndYPosition()

	var isDrawing = false
		private set

	/** Draws map cells and Entities; uses culling on both  */
	override fun draw(gd: GameDrawer, delta: Float) {
		if (!wasUpdated) {
			logWarning("Draw function is called but the EntityMap wasn't updated!")
			wasUpdated = true // warns only one time
		}

		isDrawing = true

		updateCameraRectangle()

		cellLayers.forEach { it.getTilesDrawn(drawables) }

		getEntitiesDrawn()

		if (extraMapDrawables.size > 0)
			drawables.addAll(extraMapDrawables)

		if (frameDrawOrders.size > 0) {
			drawables.addAll(frameDrawOrders)
			frameDrawOrders.clear()
		}

		drawables.sort(orderComparator)

		gd.reset()
		for (drawable in drawables) {
			drawable.draw(gd, delta)
			gd.reset()
		}

		if (Engine.DEBUG && Engine.DEBUG_ENTITIES_BB && Engine.DEBUG_ENTITIES_BB_X_RAY) {
			for (drawable in drawables) {
				if (drawable is Entity) {
					gd.reset()
					drawable.drawDebug(gd, delta)
				}
			}
		}

		gd.reset()

		// todo optimization: remove this reversed loop. Use observer pattern, entities register to handle the touch input
		// loop reversed
		run loop@{
			drawables.forEachReversed { drawable ->
				if (Engine.DEBUG && drawable is Entity) drawable.debugTouchInput(worldInput)
				if (drawable.handleTouchInput(worldInput, stageInput)) return@loop // break
			}
		}

		cellLayers.forEachWithoutIterator { it.update(delta); it.drawCellsDebug(gd, delta) }

		if (Engine.DEBUG_COLLISION_TREE_CELLS) {
			collTreeForEntityColliding.debugDrawCellBounds(gd)

			gd.alpha = 0.5f
			gd.color = Color.RED
			gd.drawRectangle(cameraRectangle, false, Engine.DEBUG_LINE_THICKNESS * 1.1f)
			gd.resetColor()
			gd.resetAlpha()
		}

		if (screen.debugDrawMousePosition)
			Engine.debugUI.drawTextInWorld("cellX: ${worldInput.mouseWorld.x.unitsToPixels.toInt() / cellSizePixels}" +
					"\ncellY: ${worldInput.mouseWorld.y.unitsToPixels.toInt() / cellSizePixels}",
					worldInput.mouseWorld.x, worldInput.mouseWorld.y - 7.pixelsToUnits, worldViewport, disappearTime = 0f)

		drawables.clear()

		if (!notifierEndOfFrame.listeners.isEmpty)
			notifierEndOfFrame.notifyListeners { it.endOfFrameUpdate(delta) }

		drawLights(gd, delta)

		notifierEndOfFrameDrawing.notifyListeners { it.endOfFrameDraw(delta) }

		isDrawing = false
	}

	val lights = GdxSet<Light>()
	var ambientColor = Color(1f, 1f, 1f, 0f)

	fun drawLights(gd: GameDrawer, delta: Float) {
		if (ambientColor.a == 0f || ambientColor == Color.WHITE || ambientColor == Color.CLEAR) return

		// draw to the light buffer
		gd.batch.end()
		screen.gameFbo.end()

		gd.batch.projectionMatrix = screen.camera.combined
		gd.batch.enableBlending()

		screen.lightFbo.use {
			// set the ambient color values, this is the "global" light of your scene
			Gdx.gl.glClearColor(ambientColor.r, ambientColor.g, ambientColor.b, ambientColor.a)
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

			// draw lights
			gd.batch.setBlendFunction(GL20.GL_BLEND_SRC_ALPHA, GL20.GL_ONE);

//            gd.batch.projectionMatrix = screen.camera.combined
			gd.batch.use {
				for (light in lights) {
					light.draw(gd, delta)
				}
			}
		}

		val oldBatch = gd.batch

		// now blend the light buffer with the already drawn game
		gd.batch = screen.engine.fboBatch
		screen.gameFbo.begin()

		gd.batch.projectionMatrix = screen.dummyFullScreenViewport.camera.combined
		gd.batch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
		gd.batch.setColor(1f, 1f, 1f, 1f)
		gd.batch.use {
			it.draw(screen.lightFbo.colorBufferTexture,
					0f, //MathUtils.floor((Gdx.graphics.width - width) / 2f).toFloat(),
					0f, //MathUtils.floor((Gdx.graphics.height - height) / 2f).toFloat(),
					0f, 0f, screen.lightFbo.width.float, screen.lightFbo.height.float,
//                    screen.lightBuffer.colorBufferTexture.width.toFloat(), screen.lightBuffer.colorBufferTexture.height.toFloat(),
					1f, 1f, 0f, 0, 0,
					screen.lightFbo.width, screen.lightFbo.height, false, true)
		}

		// reset blending function to default
		gd.batch.setBlendFunctionSeparate(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
		gd.batch = oldBatch
		gd.batch.setBlendFunctionSeparate(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)

		screen.worldViewport.apply()
		gd.batch.projectionMatrix = screen.camera.combined
		gd.batch.begin()
	}

	/** Uses culling */
	private fun getEntitiesDrawn() {
		for (ent in collTreeForCullingAndDrawing.getPossibleCollidingEnts(getCameraRectangle(), null, true)) {
			drawables.add(ent)
			ent.isInsideCamera = true
		}
	}

	private val dummyRectangle = Rectangle()

	/**
	 * Returned camera rectangle is updated at the start of the map's update cycle, so be careful about posterior camera
	 * modifications (use some border)
	 */
	fun getCameraRectangle(): Rectangle {
		val rect = dummyRectangle
		rect.set(cameraRectangle)

		if (drawingCameraPadding == 0f) return rect

		rect.x -= drawingCameraPadding
		rect.y -= drawingCameraPadding
		rect.width += drawingCameraPadding * 2
		rect.height += drawingCameraPadding * 2

		return rect
	}

	private fun updateCameraRectangle() {
		val width = worldCamera.viewportWidth * worldCamera.zoom
		val height = worldCamera.viewportHeight * worldCamera.zoom

		cameraRectangle.set(worldCamera.position.x - width / 2, worldCamera.position.y - height / 2, width, height)
	}

	operator fun contains(ent: Entity): Boolean {
		return ent in entityList
	}

	operator fun plusAssign(entity: Entity) {
		this.addEntity(entity)
	}

	fun addEntity(ent: Entity) {
		if (ent.isDisposed) {
			throw RuntimeException("Tried to add an Entity that was disposed: $ent")
		}

		if (ent.isInMap) {
			throw RuntimeException("$ent already in map: ${ent.map}")
		}

		toBeAdded.add(ent)
	}

	/** @param forever if false the entity may be added again to another map, otherwise it'll be disposed */
	fun removeEntity(ent: Entity, forever: Boolean = true) {
		if (!ent.isInMap) return
		if (toBeRemoved.contains(ent)) return

		toBeRemoved.add(ent)
		if (forever) {
			toBeDisposed.add(ent)
		}
	}

	open fun clearEntities(forever: Boolean) {
		removeAndAddEntities()
		for (ent in entityList) ent.removeFromMap(forever)
		removeAndAddEntities()
	}

	private val json get() = Engine.json

	open fun saveEntities(info: JsonSavedObject, allowEntity: (Entity) -> Boolean = { true }) {
		removeAndAddEntities()

		info.setChildValue("entities", JsonSavedObject().also {
			for (ent in entityList) {
				if (allowEntity(ent))
					it.addChildValue("ent", ent.saveEntity())
			}
		})
	}

//	open fun saveSizeInfo(info: JsonSavedObject) {
//		info.setChildValue("width", width)
//		info.setChildValue("height", height)
//	}
//
//	open fun saveProperties(info: JsonSavedObject) {
//		info.setChildValue("customProperties", json.toJson(customProperties))
//	}
//
//	open fun saveCells(info: JsonSavedObject) {
//		info.setChildValue("cells", JsonSavedObject().also {
//			for (cell in mapCells) {
//				it.addChild("cell", cell.save())
//			}
//		})
//	}

	/** @param allowEntityClass lets you ignore Entities *before they are created*
	 * @param allowEntity lets you ignore Entities *after they are created* */
	open fun loadEntities(info: JsonSavedObject, entityLoaderFromClass: EntityLoaderFromClass? = null, saveVersion: String,
						  allowEntityClass: (Class<Entity>) -> Boolean = { true }, allowEntity: (Entity) -> Boolean = { true }) {
		val ents = info.get("entities")
		if (ents != null) {
			for (entInfo in ents) {
				val clazz = Class.forName(entInfo.getString("class")) as Class<Entity>
				if (allowEntityClass(clazz)) {
					var ent: Entity? = entityLoaderFromClass?.loadEntity(clazz, entInfo)
					if (ent == null) ent = clazz.newInstance()
					ent!!.loadEntity(entInfo, saveVersion)

					if (allowEntity(ent))
						addEntity(ent)
				}
			}
		}
	}

//	fun getSavedWidthInfo(info: JsonSavedObject) = info.getInt("width", 0)
//	fun getSavedHeightInfo(info: JsonSavedObject) = info.getInt("height", 0)
//
//	open fun loadSizeInfoAndCreateEmptyMap(info: JsonSavedObject) {
//		loadEmpty(getSavedWidthInfo(info), getSavedHeightInfo(info))
//	}

	open fun loadProperties(info: JsonSavedObject) {
		customProperties = json.fromJson(CustomProperties::class.java, info.getString("customProperties"))
				?: customProperties
	}

//	open fun loadCells(info: JsonSavedObject) {
//		val cells = info.get("cells")
//		if (cells != null) {
//			var i = 0
//			for (cellInfo in cells) {
//				i++
//				mapCells[i] = MapCell.load(cellInfo, this)
//			}
//		}
//	}

	override fun dispose() {

	}

	override fun resize(width: Int, height: Int) {

	}

	override fun changedTerrain(cell: MapCell) {
		notifierGlobalCellInfo.notifyListeners { it.changedTerrain(cell) }
	}

	override fun changedTerrainTilesetMapping(cell: MapCell) {
		notifierGlobalCellInfo.notifyListeners { it.changedTerrainTilesetMapping(cell) }
	}

	override fun changedIsSolid(cell: MapCell) {
		notifierGlobalCellInfo.notifyListeners { it.changedIsSolid(cell) }
	}

	override fun changedStaticEntitiesAbove(cell: MapCell) {
		notifierGlobalCellInfo.notifyListeners { it.changedStaticEntitiesAbove(cell) }
	}

	/**
	 * Created by Darius on 28/10/2017.
	 *
	 * Orders entities based on depth, and then y-position if depth is equal. Higher depth means further away from camera.
	 * Entity with -10 depth is drawn above than another with 30 depth
	 */
	class ComparatorByDepthAndYPosition : Comparator<DrawableSortable> {
		override fun compare(o1: DrawableSortable, o2: DrawableSortable): Int {
			var comparison = o2.getDrawingDepth().compareTo(o1.getDrawingDepth())

			if (comparison == 0) comparison = o2.getDrawingY().compareTo(o1.getDrawingY())

			if (comparison == 0) // same y position
				comparison = o2.getDrawingYDepth().compareTo(o1.getDrawingYDepth())

			return comparison
		}
	}

	interface ListenerEndOfFrame {
		fun endOfFrameUpdate(delta: Float)
	}

	interface ListenerStartOfFrame {
		fun startOfFrameUpdate(delta: Float)
	}

	interface ListenerEndOfFrameDrawing {
		fun endOfFrameDraw(delta: Float)
	}

	interface ListenerEntityAdditionRemoval {
		fun entityAdded(ent: Entity)
		fun entityRemoved(ent: Entity)
	}

	fun debugDrawTextInWorld(text: Any?, worldX: Float, worldY: Float,
							 color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawTextInWorld(text, worldX, worldY, worldViewport, color, alpha, disappearTime)
	}

	fun debugDrawCircleInWorld(worldX: Float, worldY: Float, radius: Float,
							   color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawCircleInWorld(worldX, worldY, radius, worldViewport, color, alpha, disappearTime)
	}

	fun debugDrawLineInWorld(x1: Float, y1: Float, x2: Float, y2: Float,
							 thickness: Float = 1.1f, isArrow: Boolean = false, color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawLineInWorld(x1, y1, x2, y2, worldViewport, thickness, isArrow, color, alpha, disappearTime)
	}

	fun debugDrawRectInWorld(x: Number = 0f, y: Number = 0f, width: Number = 0f, height: Number = 0f, fill: Boolean = true,
							 borderThickness: Float = Engine.DEBUG_LINE_THICKNESS, color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawRectInWorld(x, y, width, height, worldViewport, fill, borderThickness, color, alpha, disappearTime)
	}

	fun debugDrawRectInWorld(rectangle: Rectangle, fill: Boolean = true,
							 borderThickness: Float = Engine.DEBUG_LINE_THICKNESS, color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawRectInWorld(rectangle, worldViewport, fill, borderThickness, color, alpha, disappearTime)
	}

//	fun addDebugWindow() {
//		Engine.debugUI.stage.addActor(object : VisWindow("debug map") {
//			lateinit var pane: VisScrollPane
//			init {
//				titleLabel.setAlignment(Align.center)
//				isMovable = true
//
//				addCloseButton()
//
//				clearChildren()
//
//				ExtLabel.defaultFont = VisUI.getSkin().getFont("small-font")
//
//				updateIt()
//				add(pane).maxHeight(200f).maxWidth(300f)
//				pack()
//			}
//
//			fun updateIt() {
//				if (this::pane.isInitialized) pane.clearChildren()
//				pane = VisScrollPane(Table().also {
//					for (ent in entityList) {
//						it.add(ExtLabel(ent.javaClass.simpleName))
//						it.row()
//					}
//				})
//				pack()
//			}
//
//			override fun act(delta: Float) {
//				super.act(delta)
//
//				if (chance(delta * 2f)) updateIt()
//			}
//		})
//	}
}

