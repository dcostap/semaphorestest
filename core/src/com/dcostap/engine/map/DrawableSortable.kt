package com.dcostap.engine.map

import com.dcostap.engine.utils.Drawable
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.input.InputController

/** Created by Darius on 21-Aug-18.
 *
 * Something that can be sorted to specify drawing order. Depth is the most important. If 2 things have same depth then y is compared */
open interface DrawableSortable : Drawable {
	/** Higher depth = further away from the camera */
	fun getDrawingDepth(): Int

	/** Used to compare drawing order when depth of two drawables is equal. Lower y = drawn after */
	fun getDrawingY(): Float

	/** Used to compare when both depth and y position is equal */
	fun getDrawingYDepth(): Int

	/** Called on all visible Drawable after drawing in a [EntityMap]
	 * Use this for correct input handling on a EntityMap: since it happens after drawing the call order will
	 * correspond to the sorting order. So something that is drawn above something else will receive the input first.
	 *
	 * Also it is only called when the InputProcessor assigned to the [EntityMap] is relayed input info, so when using
	 * InputMultiplexer this won't propagate past UI elements that catch it
	 *
	 * @return true to handle the input and avoid any other Drawable to receive the function */
	fun handleTouchInput(worldInput: InputController, stageInput: InputController?): Boolean
}

abstract class DrawableBase : DrawableSortable {
	override fun getDrawingDepth(): Int {
		return 0
	}

	override fun getDrawingY(): Float {
		return 0f
	}

	override fun getDrawingYDepth(): Int {
		return 0
	}

	override fun handleTouchInput(inputController: InputController, stageInput: InputController?): Boolean {
		return false
	}

	override fun draw(gd: GameDrawer, delta: Float) {

	}
}