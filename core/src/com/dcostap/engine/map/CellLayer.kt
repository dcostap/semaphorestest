package com.dcostap.engine.map

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.*
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Pools
import com.dcostap.Engine
import com.dcostap.engine.debug.log
import com.dcostap.engine.map.entities.Entity
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.ui.ExtLabel
import com.dcostap.engine.utils.ui.ResizableActorTable
import com.kotcrab.vis.ui.VisUI
import ktx.collections.*
import kotlin.math.floor
import kotlin.system.measureTimeMillis

/** Created by Darius on 28-Mar-20. */
class CellLayer(
		val map: EntityMap, val widthCells: Int, val heightCells: Int, val cellSizePixels: Int,
		/** Limits flood fill algorithm to regions, so that changes in a cell's solid will have to update at most this number ^ 2.
		 * This helps with performance in huge maps (nothing cycles through all tiles of the map).
		 * All of this at the expense of pathfinding between regions not taking advantage of the flood filling optimization to detect unreachable zones */
		private val floodRegionSize: Int = 50,
		doFloodFillForSolidCellsPathfinding: Boolean = true
) : Updatable {
	val cells: Array<MapCell>

	val widthPixels get() = widthCells * cellSizePixels
	val heightPixels get() = heightCells * cellSizePixels

	val widthUnits get() = widthPixels.pixelsToUnits
	val heightUnits get() = heightPixels.pixelsToUnits

	private val debugCellInfoTable = ResizableActorTable()
	private var debugCell: MapCell? = null

	private val dummyCellArray = GdxArray<MapCell>()

	init {
		cells = Array(widthCells * heightCells) { i ->
			MapCell(xFromCellIndex(i), yFromCellIndex(i), this)
		}

		ExtLabel.defaultFont = VisUI.getSkin().getFont("small-font")
		ExtLabel.defaultColor = Color.WHITE

		debugCellInfoTable.setPosition(-999999f, -999999f)
		debugCellInfoTable.isVisible = false
	}

	fun clearCellTiles() {
		for (cell in cells) {
			cell.tiles = null
		}
	}

	fun getMapCellFromUnits(x: Number, y: Number): MapCell? {
		if (!isUnitPosInsideMap(x, y)) return null
		val x = (x.unitsToPixels / cellSizePixels).floor()
		val y = (y.unitsToPixels / cellSizePixels).floor()
		return cells.get(cellIndexFromXY(x, y))
	}

	fun getMapCellFromUnits(pos: Vector2): MapCell? = this.getMapCellFromUnits(pos.x, pos.y)

	fun getMapCellFromCellCoords(x: Number, y: Number): MapCell? {
		return getMapCellFromCellCoords(MathUtils.floor(x.toFloat()), MathUtils.floor(y.toFloat()))
	}

	fun getMapCellFromCellCoords(x: Int, y: Int): MapCell? {
		if (!isCellPosInsideMap(x, y)) return null
		return cells[cellIndexFromXY(x, y)]
	}

	fun xFromCellIndex(i: Int) = i % widthCells
	fun yFromCellIndex(i: Int) = i / widthCells
	fun cellIndexFromXY(x: Int, y: Int) = x + y * widthCells

	fun isUnitPosInsideMap(x: Number, y: Number): Boolean {
		return x.toFloat() >= 0 && y.toFloat() >= 0 && x.toFloat() < widthUnits && y.toFloat() < heightUnits
	}

	fun isCellPosInsideMap(x: Number, y: Number): Boolean {
		return x.toFloat() >= 0 && y.toFloat() >= 0 && x.toFloat() < widthCells && y.toFloat() < heightCells
	}

	fun isCellPosInsideMap(x: Int, y: Int): Boolean {
		return x >= 0 && y >= 0 && x < widthCells && y < heightCells
	}

	fun getCellsOccupiedBy(rectangle: Rectangle): GdxArray<MapCell> {
		val cells = dummyCellArray
		cells.clear()

		poolVector { origin ->
			poolVector { end ->
				origin.set(floor(rectangle.x), floor(rectangle.y))
				end.set(floor((rectangle.x + rectangle.width - 1.pixelsToUnits)), floor((rectangle.y + rectangle.height - 1.pixelsToUnits)))

				origin.scl(Engine.PPU.float).scl(1f / cellSizePixels)
				end.scl(Engine.PPU.float).scl(1f / cellSizePixels)

				val ox = origin.x.toInt()
				val oy = origin.y.toInt()
				val ex = end.x.toInt()
				val ey = end.y.toInt()

				var xx = ox
				while (xx <= ex) {
					var yy = oy
					while (yy <= ey) {
						if (isCellPosInsideMap(xx, yy))
							cells.add(this.cells[cellIndexFromXY(xx, yy)])
						yy++
					}
					xx++
				}
			}
		}

		return cells
	}

	fun getCellsOccupiedBy(polygon: Polygon): GdxArray<MapCell> {
		return getCellsOccupiedBy(tmpRect.set(polygon.boundingRectangle))
	}

	private val tmpRect = Rectangle()

	fun getCellsOccupiedBy(circle: Circle): GdxArray<MapCell> {
		tmpRect.set(circle.x - circle.radius, circle.y - circle.radius, circle.radius * 2, circle.radius * 2)
		return getCellsOccupiedBy(tmpRect)
	}

	fun getCellsOccupiedBy(shape2D: Shape2D): GdxArray<MapCell> {
		if (shape2D is Rectangle)
			return getCellsOccupiedBy(shape2D)
		if (shape2D is Circle)
			return getCellsOccupiedBy(shape2D)
		if (shape2D is Polygon)
			return getCellsOccupiedBy(shape2D)

		throw RuntimeException("Invalid shape: $shape2D")
	}

	/** Updates cell variables on cells affected by Entity's bounding box  */
	fun updateCellsDueToEntity(ent: Entity, entityIsRemoved: Boolean) {
		if (!ent.isStatic || !ent.providesStaticInfoToCells) return

		// will ignore Static entities that overlap outside the map
		for (cell in getCellsOccupiedBy(ent.boundingBox.shape2D)) {
			if (entityIsRemoved) {
				cell.staticEntitiesAbove.removeValue(ent, true)
				cell.changedStaticEntities()
			} else {
				cell.staticEntitiesAbove.add(ent)
				cell.changedStaticEntities()
			}

			if (Engine.DEBUG && Engine.DEBUG_MAP_CELLS)
				cell.debugFlashingRect = FlashingThing(Color.YELLOW, 0.6f, 0.1f)

			if (ent.isSolid) {
				cell.updateHasSolid()

				if (Engine.DEBUG && Engine.DEBUG_MAP_CELLS)
					cell.debugFlashingRect = FlashingThing(Color.RED, 0.6f, 0.1f)
			}
		}
	}

	/** Uses culling */
	fun getTilesDrawn(drawables: GdxArray<DrawableSortable>) {
		for (cell in getCellsOccupiedBy(map.getCameraRectangle())) {
			cell.tiles.ifNotNull { drawables.addAll(*it) }
		}
	}

	fun drawCellsDebug(gd: GameDrawer, delta: Float) {
		if (!Engine.DEBUG) return
		if (!Engine.DEBUG_MAP_CELLS && !Engine.DEBUG_CELL_FLOOD_FILL) return

		val rectangle = Pools.obtain(Rectangle::class.java)

		val colorSize = if (Engine.DEBUG_CELL_FLOOD_FILL) Utils.colors.values().toArray().size else 0

		for (cell in getCellsOccupiedBy(map.getCameraRectangle())) {
			if (Engine.DEBUG_MAP_CELLS) {
				if (!(Engine.DEBUG_MAP_CELLS_SOLID_ONLY && !cell.isSolid)) {
					if (cell.isSolid) {
						gd.alpha = Engine.DEBUG_TRANSPARENCY / 2f
						gd.color = Color.BLACK
						gd.drawRectangle(cell.xUnits, cell.yUnits,
								cell.layer.cellSizePixels.pixelsToUnits, cell.layer.cellSizePixels.pixelsToUnits, true, 0f)
						gd.resetColorAndAlpha()
					}

					cell.debugFlashingRect.ifNotNull {
						if (it.isFlashing) {
							it.update()
							it.setupGameDrawer(gd)
							gd.drawRectangle(cell.xUnits, cell.yUnits,
									cell.layer.cellSizePixels.pixelsToUnits, cell.layer.cellSizePixels.pixelsToUnits, true, 0f)
							it.resetGameDrawer(gd)
						} else {
							cell.debugFlashingRect = null
						}
					}

					gd.color = Color.BLACK
					gd.alpha = Engine.DEBUG_TRANSPARENCY / 2.4f
					gd.drawRectangle(cell.xUnits, cell.yUnits,
							cell.layer.cellSizePixels.pixelsToUnits, cell.layer.cellSizePixels.pixelsToUnits, false,
							Engine.DEBUG_LINE_THICKNESS / 2f)

					gd.resetColorAndAlpha()
				}
			}

			if (Engine.DEBUG_CELL_FLOOD_FILL) {
				if (cell.pathfindingFloodIndex != -1L) {
					gd.alpha = Engine.DEBUG_TRANSPARENCY / (if (cell.isSolid) 1f else 3f)
					gd.color = Utils.colors.values().elementAt((cell.pathfindingFloodIndex % colorSize).toInt())
					gd.drawRectangle(cell.xUnits, cell.yUnits,
							cell.layer.cellSizePixels.pixelsToUnits, cell.layer.cellSizePixels.pixelsToUnits, true, 0f)
					gd.drawText(cell.floodRegion.toString(), cell.middleX, cell.yCell.toFloat() + Engine.PPU * 0.8f, Engine.debugFont, Color.BLACK)
					gd.resetColorAndAlpha()
				}
			}
		}

		Pools.free(rectangle)
	}

	fun getClosestValidCell(startingCell: MapCell, allowSolidCells: Boolean, allowCellsWithTiledEntity: Boolean): MapCell? {
		if (isCellValid(startingCell, allowSolidCells, allowCellsWithTiledEntity))
			return startingCell

		var x = 0
		var y = 0
		var amount = 1
		var sign = 1
		var yTurn = false

		// spiral loop around the start cell
		while (true) {
			// if cell is inside map
			val posX: Int = startingCell.xCell + x
			val posY: Int = startingCell.yCell + y

			if (isCellPosInsideMap(posX.toFloat(), posY.toFloat())) {
				val cell = getMapCellFromCellCoords(posX, posY)

				if (cell != null && isCellValid(cell, allowSolidCells, allowCellsWithTiledEntity)) {
					return cell
				}
			}

			// make a spiral loop
			if (yTurn)
				y += sign
			else
				x += sign

			if (!yTurn && x == sign * amount) {
				yTurn = true
			} else if (yTurn && y == sign * amount) {
				yTurn = false
				sign *= -1

				if (sign == 1)
					amount++
			}
		}
	}

	fun isCellValid(mapCell: MapCell, allowSolidCells: Boolean, allowCellsWithTiledEntity: Boolean): Boolean {
		return (allowSolidCells || mapCell.isSolid) && (allowCellsWithTiledEntity || mapCell.staticEntitiesAbove.size == 0)
	}

	private val tmpArray = GdxArray<MapCell>()

	/** Finds what cells the line represented overlaps */
	fun raytraceCellsWithLine(x0: Number, y0: Number, x1: Number, y1: Number): GdxArray<MapCell> {
		val x0 = x0.toFloat()
		val x1 = x1.toFloat()
		val y0 = y0.toFloat()
		val y1 = y1.toFloat()
		tmpArray.clear()
		var dx = Math.abs(x1 - x0)
		var dy = Math.abs(y1 - y0)
		var x = x0
		var y = y0
		var n = 1 + dx + dy
		val x_inc = if (x1 > x0) 1 else -1
		val y_inc = if (y1 > y0) 1 else -1
		var error = dx - dy
		dx *= 2
		dy *= 2

		TODO("since refactoring of EntityMap, this probably works wrong. Correctly translate coords to appropiate units")
		while (n > 0) {
			tmpArray.add(getMapCellFromUnits(x, y))

			if (error > 0) {
				x += x_inc
				error -= dy
			} else {
				y += y_inc
				error += dx
			}
			--n
		}

		return tmpArray
	}

	override fun update(delta: Float) {
		if (!didFullFloodFill && doFloodFillForSolidCellsPathfinding) {
			floodFillFull()
			didFullFloodFill = true
		}

		val wasVisible = debugCellInfoTable.isVisible

		debugCellInfoTable.isVisible = false
		if (Engine.DEBUG && Engine.DEBUG_MAP_CELLS_INFO && Engine.DEBUG_MAP_CELLS) {
			if (isUnitPosInsideMap(map.worldInput.mouseWorld.x, map.worldInput.mouseWorld.y)) {
				debugCellInfoTable.isVisible = true
			}
		}

		debugCell.ifNotNull {
			Engine.debugUI.drawRectInWorld(it.xUnits, it.yUnits,
					it.layer.cellSizePixels.pixelsToUnits, it.layer.cellSizePixels.pixelsToUnits,
					map.worldViewport, false, Engine.DEBUG_LINE_THICKNESS, Color.FIREBRICK, 1f, 0f)
		}

		if (debugCellInfoTable.isVisible != wasVisible) {
			if (debugCellInfoTable.isVisible) {
				getMapCellFromUnits(map.worldInput.mouseWorld.x, map.worldInput.mouseWorld.y).ifNotNull { cell ->
					Engine.debugUI.stage.addActor(debugCellInfoTable)
					debugCellInfoTable.clearChildren()
					if (cell != debugCell) {
						debugCell = cell
					}
					debugCellInfoTable.add(Table().also {
						it.align(Align.top)
						it.top()
						it.background = VisUI.getSkin().getDrawable("window-bg")
						it.pad(4f)

						it.add(ExtLabel("cell in x: ${cell.xCell}; y: ${cell.yCell}"))
						it.row()
						it.add(ExtLabel("markedAsSolid: ${cell.markedAsSolid}"))
						it.row()
						it.add(ExtLabel("isSolid: ${cell.isSolid}"))
						it.row()
						it.add(ExtLabel("tiles: ${cell.tiles?.size ?: 0}"))
						it.row()

						cell.tiles.ifNotNull { tiles ->
							for ((i, tile) in tiles.withIndex()) {
								it.add(ExtLabel(" -> ${tile::class.simpleName} ${if (tile is SprTile) "; " +
										"spr: " + tile.tileSprName else ""}; depth: ${tile.depth}"))
								it.row()
							}
						}

						it.add(ExtLabel("terrain id: ${cell.terrainId}"))
						it.row()
						it.add(ExtLabel("static entities above: ${cell.staticEntitiesAbove.size}"))
						it.row()
						it.add(ExtLabel("pathfindingFloodIndex: ${cell.pathfindingFloodIndex}"))
					})
				}
			} else
				debugCellInfoTable.remove()
		}

		if (debugCellInfoTable.isVisible) {
			val coords = Pools.obtain(Vector2::class.java)
			debugCellInfoTable.pack()

			val cell = getMapCellFromUnits(map.worldInput.mouseWorld.x, map.worldInput.mouseWorld.y)!!

			if (cell != debugCell) {
				debugCellInfoTable.isVisible = false
				return
			}

			coords.set(Utils.projectPosition(cell.middleX, cell.middleY, map.worldViewport, Engine.debugUI.stage.viewport))

			val index = map.cellLayers.indexOf(this)
			debugCellInfoTable.setPosition(coords.x + 230 * index, coords.y - 88f)

			Pools.free(coords)
		}
	}

	// region flooding
	/** If true, whenever there is a change in solid state in any cell, [floodFill] will run once in current or next frame.
	 * Also, [floodFillFull] (performed on all cells of the map) is called at the start of
	 * @see MapCell.pathfindingFloodIndex
	 * @see floodRegionSize */
	var doFloodFillForSolidCellsPathfinding = doFloodFillForSolidCellsPathfinding
		set(value) {
			if (field != value && value) {
				didFullFloodFill = false
			}

			field = value
		}

	private var didFullFloodFill = false

	private var maxUsedIndex = 0L
		set(value) {
			if (value <= -1L) { // avoid overflow
				field = 0L // dummy value in case it's in the middle of a floodFill
				didFullFloodFill = false // reset it in update
			} else {
				field = value
			}
		}

	private fun floodFillFull() {
		val unexploredCells = GdxMap<Int, MapCell>()
		var indexFill = 0L

		val floodList = GdxArray<MapCell>()
		var originCell: MapCell? = null
		fun flood(x: Int, y: Int) {
			val cell = getMapCellFromCellCoords(x, y)!!
			if (cell.floodRegion != originCell!!.floodRegion) return
			if (cell.pathfindingFloodIndex != -1L) return
			if (cell.isSolid != originCell!!.isSolid) return

			unexploredCells.remove(cellIndexFromXY(x, y))

			cell.pathfindingFloodIndex = indexFill

			if (isCellPosInsideMap(x, y + 1))
				floodList.add(getMapCellFromCellCoords(x + 0, y + 1))

			if (isCellPosInsideMap(x + 1, y))
				floodList.add(getMapCellFromCellCoords(x + 1, y + 0))

			if (isCellPosInsideMap(x - 1, y))
				floodList.add(getMapCellFromCellCoords(x - 1, y + 0))

			if (isCellPosInsideMap(x, y - 1))
				floodList.add(getMapCellFromCellCoords(x + 0, y - 1))
		}

		val time = measureTimeMillis {
			for (i in 0 until widthCells * heightCells) {
				val cell = cells[i]
				cell.floodRegion = MathUtils.floor(xFromCellIndex(i) / floodRegionSize.toFloat()) +
						MathUtils.floor(yFromCellIndex(i) / floodRegionSize.toFloat()) * (MathUtils.floor(widthCells / floodRegionSize.toFloat()) + 1)
				cell.pathfindingFloodIndex = -1L
				unexploredCells.put(i, cell)
			}

			while (!unexploredCells.isEmpty) {
				val chosen = unexploredCells.first()
				originCell = chosen.value

				floodList.add(chosen.value)
				while (!floodList.isEmpty) {
					val cell = floodList.pop()
					flood(cell.xCell, cell.yCell)
				}

				maxUsedIndex = indexFill
				indexFill++
			}
		}

		log("Did full flood fill on cellLayer with ${widthCells * heightCells} cells. Took ${time / 1000f} seconds")
	}

	fun floodFill(motherCell: MapCell) {
		if (!didFullFloodFill) return
		if (!doFloodFillForSolidCellsPathfinding) return

		var index = 0L
		maxUsedIndex++
		val startingMaxIndex = maxUsedIndex

		var originIsSolid = false
		var originRegion = -1
		val floodList = GdxArray<MapCell>()

		fun flood(x: Int, y: Int) {
			val thisCell = getMapCellFromCellCoords(x, y)!!
			if (originRegion != thisCell.floodRegion) return
			if (thisCell.pathfindingFloodIndex == index) return
			if (thisCell.pathfindingFloodIndex > startingMaxIndex) return
			if (originIsSolid != thisCell.isSolid) return

			if (Engine.DEBUG_CELL_FLOOD_FILL)
				thisCell.debugFlashingRect = FlashingThing(Color.RED, 0.4f)

			thisCell.pathfindingFloodIndex = index

			if (isCellPosInsideMap(x, y + 1))
				floodList.add(getMapCellFromCellCoords(x + 0, y + 1))

			if (isCellPosInsideMap(x + 1, y))
				floodList.add(getMapCellFromCellCoords(x + 1, y + 0))

			if (isCellPosInsideMap(x - 1, y))
				floodList.add(getMapCellFromCellCoords(x - 1, y + 0))

			if (isCellPosInsideMap(x, y - 1))
				floodList.add(getMapCellFromCellCoords(x + 0, y - 1))
		}

		val x = motherCell.xCell;
		val y = motherCell.yCell

		fun floodStart(x: Int, y: Int) {
			maxUsedIndex++
			index = maxUsedIndex

			if (isCellPosInsideMap(x, y)) {
				val cell = getMapCellFromCellCoords(x, y)!!
				if (motherCell.floodRegion == cell.floodRegion) {
					floodList.add(cell)
					originIsSolid = cell.isSolid
					originRegion = cell.floodRegion
				}
			}

			while (!floodList.isEmpty) {
				val cell = floodList.pop()
				flood(cell.xCell, cell.yCell)
			}
		}

		motherCell.pathfindingFloodIndex = maxUsedIndex
		floodStart(x + 0, y + 1)
		floodStart(x + 1, y + 0)
		floodStart(x + 0, y - 1)
		floodStart(x - 1, y - 0)
	}
	// endregion
}