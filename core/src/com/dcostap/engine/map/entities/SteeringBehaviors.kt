package com.dcostap.engine.map.entities

import com.badlogic.gdx.math.Vector2
import com.dcostap.engine.utils.*
import ktx.collections.*

/**
 * Created by Darius on 08/09/2017.
 */
class SteeringBehaviors(var entity: Entity, var maxSpeed: Float = 100f, var maxForce: Float = 700f) {
	private val behaviors = GdxArray<Behavior>()

	fun removeAllBehaviors() {
		behaviors.clear()
	}

	fun add(behavior: Behavior) {
		behaviors.add(behavior)
		behavior.steering = this
	}

	var disable = false

	private val desiredSpeed = Vector2()

	/** Steering behaviors will use this goal if its local goal is null */
	val globalGoal: Vector2? = null

	private val returned = Vector2()

	operator fun plusAssign(behavior: Behavior) {
		add(behavior)
	}

	/** If true, it won't happen that desired speed is 0 (no behaviors) and Entity
	 * is forced to always slow down to 0. Entity's speed will be considered a baseline desired speed */
	var alwaysAddEntitySpeed = false

	fun update(delta: Float) {
		if (disable) return
		val finalDesiredSpeed = returned.setZero()
		for (b in behaviors) {
			if (b.strength <= 0) continue

			desiredSpeed.setZero()
			b.updateDesiredSpeed(delta, desiredSpeed)
			finalDesiredSpeed.add(desiredSpeed.scl(b.strength))
		}

		if (alwaysAddEntitySpeed)
		finalDesiredSpeed.add(entity.speed)

//		if (!zeroSpeedMeansZeroDesired && finalDesiredSpeed.isZero) return

		val steeringForce = finalDesiredSpeed.sub(entity.speed) // steering = desired - velocity
		steeringForce.scl(if (delta <= 0f) 0f else 1 / delta)
		steeringForce.limit(maxForce)

		entity.acceleration.add(steeringForce)
	}

	abstract class Behavior(var strength: Float) {
		lateinit var steering: SteeringBehaviors

		var localGoal: Vector2? = null
		private val tmpV = Vector2()
		internal val entity get() = steering.entity

		internal open fun getTmpGoal(delta: Float): Vector2? {
			if (localGoal == null) {
				if (steering.globalGoal != null)
					tmpV.set(steering.globalGoal)
				else
					return null
			} else tmpV.set(localGoal)

			return tmpV
		}

		abstract fun updateDesiredSpeed(delta: Float, desiredSpeed: Vector2)
	}

	open class Seek(strength: Float) : Behavior(strength) {
		override fun updateDesiredSpeed(delta: Float, desiredSpeed: Vector2) {
			getTmpGoal(delta).ifNotNull {tmpGoal ->
				desiredSpeed.set(tmpGoal.sub(entity.position))
				desiredSpeed.nor()
				desiredSpeed.scl(steering.maxSpeed)
			}
		}
	}

	/** Keep in mind the difference between just setting the entity.speed outside and using this specialized steering behavior:
	 * With this steering behavior the speed added will be part of the "desired speed". If you just modify the external speed, it
	 * will be constantly adjusted in this class to get closer to the "desired" result of all the behaviors. */
	open class ConstantSpeed(strength: Float, val getSpeed: () -> Vector2) : Behavior(strength) {
		constructor(strength: Float, speed: Vector2) : this(strength, { speed })

		override fun updateDesiredSpeed(delta: Float, desiredSpeed: Vector2) {
			desiredSpeed.set(getSpeed())
		}
	}

	open class Arrive(
			strength: Float,
			goal: Vector2? = null,
			val mapDistanceToSpeed: (Float) -> Float = { it }
	) : Behavior(strength) {
		init {
			this.localGoal = goal
		}

		override fun updateDesiredSpeed(delta: Float, desiredSpeed: Vector2) {
			getTmpGoal(delta).ifNotNull { tmpGoal ->
				val distance = Utils.distanceBetween(entity.x, entity.y, tmpGoal.x, tmpGoal.y)
				desiredSpeed.set(tmpGoal.sub(entity.position))
				desiredSpeed.nor()
				desiredSpeed.scl(mapDistanceToSpeed(distance))
				desiredSpeed.limit(steering.maxSpeed)
			}
		}
	}

	/** [Arrive] behavior with a goal that can change every frame, in a handy function */
	class Pursuit(
			strength: Float,
			val getGoal: (Float) -> Vector2,
			mapDistanceToSpeed: (Float) -> Float = { it * 2f }
	) : Arrive(strength, null, mapDistanceToSpeed) {
		private val tmpVec = Vector2()
		override fun getTmpGoal(delta: Float): Vector2 {
			return tmpVec.set(getGoal(delta))
		}
	}

	/** Tries to adapt its speed (length and direction) to the average speed of nearby Entities (length will never be more than [maxSpeed])
	 *
	 * @param speedAlignmentMapping For each neighbor's speed, transform it into a final speed you try to align to.
	 *                        This way you can ignore small speeds or even stationary Entities so they won't have an influence
	 *                        and slow you down too much
	 * @param weighDistance Map a weight (from 0 to 1) to a neighbor's distance
	 * */
	open class Alignment(
			strength: Float,
			val getNeighbors: () -> Iterable<Entity>,
			var speedAlignmentMapping: ((Float) -> Float)? = null,
			val weighDistance: (Float) -> Float = { map(it, 0f, 3f, 1f, 0f) }
	) : Behavior(strength) {
		private val tmpV1 = Vector2()
		override fun updateDesiredSpeed(delta: Float, desiredSpeed: Vector2) {
			if (speedAlignmentMapping == null)
				speedAlignmentMapping = { it.map(0f, steering.maxSpeed, steering.maxSpeed / 2f, steering.maxSpeed) }
			var count = 0
			var totalSpeed = 0f
			for (n in getNeighbors()) {
				count++
				val dist = Utils.distanceBetween(entity.position, n.position)
				val weight = weighDistance(dist)

				if (weight == 0f) continue
				tmpV1.set(n.speed)
				tmpV1.scl(weight)
				totalSpeed += speedAlignmentMapping!!.invoke(tmpV1.len())
				desiredSpeed.add(tmpV1)
			}
//            Engine.debugUI.drawTextInWorld(count.toString(), entity.x, entity.y, entity.worldViewport, disappearTime = 0f)
			if (count == 0) return
			desiredSpeed.scl(1f / count)
			desiredSpeed.nor()
			desiredSpeed.scl(Math.min(steering.maxSpeed, totalSpeed / count))
		}
	}

	/** @param weighDistance assign a weight (from 0 to 1, how strongly you wanna separate from it) depending on the distance a neighbor is */
	open class Separation(
			strength: Float,
			val getNeighbors: () -> Iterable<Entity>,
			val weighDistance: (Float) -> Float = { map(it, 3f, 6f, 1f, 0f) }
	) : Behavior(strength) {
		private val tmpV1 = Vector2()

		override fun updateDesiredSpeed(delta: Float, desiredSpeed: Vector2) {
			var count = 0
			for (n in getNeighbors()) {
				count++
				val dist = Utils.distanceBetween(entity.position, n.position)
				val weight = weighDistance(dist)
				if (weight == 0f) continue
				tmpV1.set(entity.position)
				tmpV1.sub(n.position)
				// they are in the same spot? then subtraction gave 0, in that case we can't have angle out so pick one random
				if (tmpV1.isZero) tmpV1.setAngleMovement(1f, randomAngle())
				tmpV1.nor()
				tmpV1.scl(steering.maxSpeed * weight)
				desiredSpeed.add(tmpV1)
			}

			if (count == 0) return
			desiredSpeed.scl(1f / count)
		}
	}

	/** @param weighDistance assign a weight (from 0 to 1, how strongly you wanna separate from it) depending on the distance a neighbor is */
	open class Join(
			strength: Float,
			val getNeighbors: () -> Iterable<Entity>,
			val weighDistance: (Float) -> Float = { map(it, 3f, 6f, 1f, 0f) }
	) : Behavior(strength) {
		private val goal = Vector2()

		override fun updateDesiredSpeed(delta: Float, desiredSpeed: Vector2) {
			var count = 0
			goal.set(entity.position)
			for (n in getNeighbors()) {
				if (n == entity) return
				count++
				var dist = Utils.distanceBetween(entity.position, n.position)
				val weight = weighDistance(dist)
				dist *= weight
				if (dist <= 0.01f) continue
				val angle = Utils.angleBetween(entity.position, n.position)
				goal.addAngleMovement(dist, angle)
			}

			if (count == 0 || goal == entity.position || Utils.distanceBetween(entity.position, goal) < 0.05) return
			desiredSpeed.set(goal.sub(entity.position))
			desiredSpeed.limit(steering.maxSpeed)
		}
	}

//
//    private fun flee(goal: Vector2): Vector2 {
//        desiredForce = entity.position.sub(goal)
//        desiredForce.setLength(maxSpeed)
//        //Vector2 result = desiredForce.sub(velocityForce);
//
//        //result.limit(maxForce);
//        return desiredForce
//    }

//
//    private fun cohesion(): Vector2 {
//        val averagePosition = tmpV1.set(0f, 0f)
//p
//        var count = 0
//        for (sc in getClosestValidEnts()) {
//            count++
//            averagePosition.add(sc.position)
//        }
//
//        averagePosition.scl(1f / count.toFloat())
//        //averagePosition.nor();
//
//        val distance = averagePosition.sub(position)
//        distance.nor()
//        distance.scl(maxSpeed)
//
//        //Vector2 result = distance.sub(velocityForce);
//
//        //result.limit(maxForce);
//        return distance
//    }
}
