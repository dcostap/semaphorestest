package com.dcostap.engine.map.entities

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.JsonValue
import com.dcostap.Engine
import com.dcostap.engine.map.MapCell
import com.dcostap.engine.utils.*
import ktx.collections.*
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sign

/**
 * Created by Darius on 15/09/2017.
 *
 * Define which Entities to check for in [filterEntityForPossibleCollision] (defaults to solid entities)
 *
 * Optimization guide: by default many collidingEntities together might slow down the game. It's up to you to apply these optimizations:
 * - If the collision response will only be done against solid static Entities, consider using [checkCollisionWithMapCells]
 * and deactivating [checkCollisionWithEntities] for a performance boost.
 * - Consider restricting [suddenCollisionResponse] for non-important Entities.
 * - Use [isInsideCamera] to optimize some variables only when that non-important Entity is outside of camera.
 * - Disable [isActivated] when that Entity does nothing important in [update]. For [CollidingEntity] though, it's better to only deactivate
 * it when not [isInsideCamera], if ever.
 * - Consider stopping the movement of non-important Entities when not [isInsideCamera]. No movement = no collision checking (if [suddenCollisionResponse]
 * is correctly limited)
 *
 * [doContinuousMovement] provides continuous collision checking, so if going very fast you won't jump over any of those Entities.
 *
 * If [doCollisionResponse], provides collision response against those Entities.
 *
 * Only movement set in [speed] is used, and will be processed when calling [update].
 *
 * During collision response the Entity will move in segments of the total speed (controlled with [collisionDetectionMinPrecision]),
 * and will check for collisions against the specified Entities; if collisions exist, this Entity will try to go back to where
 * the collision first happens.
 *
 * You can override or modify this default behavior by manually calling [moveColliding].
 * Movement using that method will always perform collision response.
 *
 * Note that the Entity will check for possible collisions while doing the colliding movement, so consider re-using [tmpPossibleCollidingEnts]
 * afterwards (will include Entities as specified in [filterEntityForPossibleCollision]).
 *
 * Sliding & pushing: make sure you don't slide & push the same Entities. Filter in [allowPushingEnt] & [allowSlidingOnEntity].
 * Default behavior: won't slide on Entities that can be pushed & Entities can be pushed if those Entities push too.
 *
 * Collision checking is done with [collisionResponseBB] against [othersCollisionResponseBB].
 */
open class CollidingEntity @JvmOverloads constructor(position: Vector2 = Vector2(),
													 isSolid: Boolean = false, var collisionResponseIncludesDynamicEnts: Boolean = true,
													 providesCollidingInfo: Boolean = Engine.ENTITIES_PROVIDE_COLL_INFO_DEFAULT)
	: Entity(position, null, isSolid, false, providesCollidingInfo) {
	/** Before a collision is found, how much the Entity moves at once until it finds a collision or until it moves fully ([speed]).
	 * As a minimum, it will move in speed / 2 increments. */
	var collisionDetectionMinPrecision = 1.pixelsToUnits

	/** Whether in this frame, after processing movement, the Entity had a collision with any valid Entity */
	var hasCollided = false
		private set

	/** Whether in this frame it has done collision response in X coordinate with an entity. 0 if false, 1 if true going to the right, -1 if true going to the left */
	var hasCollidedX = 0
		private set

	/** Whether in this frame it has done collision response in Y coordinate with a solid entity. 0 if false, 1 if true going upwards, -1 if true going downwards */
	var hasCollidedY = 0
		private set

	var doCollisionResponse = true

	/** Will avoid the Entity from surpassing relevant colliding entities when going too fast */
	var doContinuousMovement = false

	/** Whenever you are colliding before moving, it's considered a "sudden" collision, which means any other entity
	 * overlapped you (this happens if that Entity moved without collision response, or if its bounding box suddenly
	 * changed size or even rotation in [PolyBoundingBox]).
	 *
	 * While sudden-collision response isn't done, the entity might get stuck inside these other Entities */
	var suddenCollisionResponse = SuddenCollisionResponse.ONLY_WHEN_MOVING

	enum class SuddenCollisionResponse {
		ONLY_WHEN_MOVING, WHEN_MOVING_AND_WHEN_STOPPED_CHECK_EVERY_FRAME, WHEN_MOVING_AND_WHEN_STOPPED_CHECK_AFTER_RANDOM_FRAMES, NEVER
	}

	var slideOnAngledSurfaces = true // todo bug when this is true, when sliding the collidingEntities list is empty, it is never filled with the entities it just collided

	/** When walking and colliding, and near to a rectangle BB corner, entity will gently move towards that corner.
	 * Adds more overhead to [moveColliding]. Choose which entities to slide on [allowSlidingOnEntity]. By default,
	 * if the other Entity can be pushed, sliding will be disabled */
	var slideOnCorners = false
	/** smaller = needs to be closer to a corner to start sliding */
	var slideMarginPixels = 10

	var pushEntities = false

	/** Speed loss whenever this Entity is pushed */
	var bePushedMult = 0.6f

	/** Speed loss any time this Entity is pushing */
	var pushMult = 1f

	var tmpPossibleCollidingEnts = GdxArray<Entity>()
	private var tmpCollidingEnts = GdxArray<Entity>()

	/** (Only with [doCollisionResponse]) List of valid (only solid ones by default) entities that the object collided with in this frame, while resolving collision
	 * Use it with hasCollidedX to find out in which direction it collided with those entities
	 *
	 * Example: if [hasCollidedX] returns 0, (or [hasCollided] returns false) this will never return anything since
	 * there was no collision response in this axis. If it returns 1, check this method.
	 * All those entities collided with this entity and were to its right.
	 *
	 * Note that this applies to collision-response valid entities only, just like the other methods
	 * related to collision response flags */
	val collidingEntitiesX = GdxSet<Entity>()

	/** @see [collidingEntitiesX] */
	val collidingEntitiesY = GdxSet<Entity>()

	val collidingEntities = GdxSet<Entity>()
	val collidingCells = GdxSet<MapCell>()

	/** If true collisionResponse will check for MapCells filtered in [filterMapCellForCollision]
	 * @see MapCell */
	var checkCollisionWithMapCells = true
	var checkCollisionWithEntities = true

	open var collisionResponseBB: String = "default"

	/** Will check collision with other Entities which have this BB */
	open var othersCollisionResponseBB: String = "default"

	override fun update(delta: Float) {
		super.update(delta)
	}

	override fun applyMovement(delta: Float) {
		super.applyMovement(delta)

		position.sub(speed.x * delta, speed.y * delta)
		moveColliding(speed.x, speed.y, delta)
	}

	fun updateCollidingState() {
		tmpPossibleCollidingEnts = collTree.getPossibleCollidingEnts(getBoundingBox(collisionResponseBB), this, collisionResponseIncludesDynamicEnts, othersCollisionResponseBB)
	}

	open fun justGotPushed(xAmount: Float, yAmount: Float, otherEnt: CollidingEntity, delta: Float) {
		moveColliding(xAmount, yAmount, 1f)
	}

	private var checksTimer = 0f

	open fun moveColliding(xAdd: Float, yAdd: Float, delta: Float) {
		hasCollidedX = 0
		hasCollidedY = 0
		hasCollided = false
		collidingEntitiesX.clear()
		collidingEntitiesY.clear()
		collidingEntities.clear()
		collidingCells.clear()

		val origPosX = position.x
		val origPosY = position.y
		val xAdd = xAdd * delta
		val yAdd = yAdd * delta

		if (xAdd == 0f && yAdd == 0f) {
			// update the state; even if not moving other entities might have moved and you may be stuck
			if (doCollisionResponse
					&& suddenCollisionResponse != SuddenCollisionResponse.NEVER
					&& suddenCollisionResponse != SuddenCollisionResponse.ONLY_WHEN_MOVING) {
				checksTimer += delta

				// will force the collision checking after a kinda random amount of time
				var forceIt = false
				if (suddenCollisionResponse == SuddenCollisionResponse.WHEN_MOVING_AND_WHEN_STOPPED_CHECK_AFTER_RANDOM_FRAMES) {
					if (checksTimer > 0.3f) {
						checksTimer = 0f
						forceIt = randomChance(0.5f)
					}
				}

				if (suddenCollisionResponse == SuddenCollisionResponse.WHEN_MOVING_AND_WHEN_STOPPED_CHECK_EVERY_FRAME || forceIt) {
					if (checkCollisionWithMapCells) {
						val collCells = getCollidingMapCells(collisionResponseBB) { filterMapCellForCollision(it) }
						if (!collCells.isEmpty) {
							hasCollided = true
							collidingCells.addAll(collCells)
						}
					}

					if (checkCollisionWithEntities && !hasCollided) {
						updateCollidingState()
						if (isCollidingWithOneEntityValidForCollisionResponse()) {
							hasCollided = true
						}
					}

					if (hasCollided) {
						fixCollisionCausedBeforeMovement(xAdd, yAdd)
					}
				}
			}

			return
		}

		if (!doContinuousMovement && !doCollisionResponse) {
			position.add(xAdd, yAdd)
			return
		}

		if (doContinuousMovement) {
			var finalXADD = 0f
			var finalYADD = 0f
			val precision = Math.min(collisionDetectionMinPrecision, Math.max(Math.abs(xAdd), Math.abs(yAdd)) / 2f)

			fun goBack() {
				position.add(finalXADD * Math.signum(xAdd) * -1f, finalYADD * Math.signum(yAdd) * -1f)
			}

			var foundSomething = false
			while (finalXADD < Math.abs(xAdd) || finalYADD < Math.abs(yAdd)) {
				goBack()
				finalXADD += precision
				if (finalXADD > Math.abs(xAdd)) finalXADD = Math.abs(xAdd)
				finalYADD += precision
				if (finalYADD > Math.abs(yAdd)) finalYADD = Math.abs(yAdd)
				position.add(finalXADD * Math.signum(xAdd), finalYADD * Math.signum(yAdd))

				if (checkCollisionWithMapCells) {
					val collCells = getCollidingMapCells(collisionResponseBB) { filterMapCellForCollision(it) }
					if (!collCells.isEmpty) {
						hasCollided = true
						collidingCells.addAll(collCells)
					}
				}

				if (checkCollisionWithEntities && !hasCollided) {
					updateCollidingState()
					if (isCollidingWithOneEntityValidForCollisionResponse()) {
						hasCollided = true
					}
				}

				if (hasCollided) {
					if (doCollisionResponse) {
						// go back to original position
						val x = finalXADD * sign(xAdd)
						val y = finalYADD * sign(yAdd)
						position.add(-x, -y)

						fixCollision(xAdd, yAdd, delta, false)
					} else {
						goBack()
						position.add(xAdd, yAdd)
					}

					foundSomething = true
					break
				}
			}

			if (!foundSomething) {
				position.set(origPosX, origPosY)
				position.add(xAdd, yAdd)
			}
		} else {
			position.add(xAdd, yAdd)

			if (checkCollisionWithMapCells) {
				val collCells = getCollidingMapCells(collisionResponseBB) { filterMapCellForCollision(it) }
				if (!collCells.isEmpty) {
					hasCollided = true
					collidingCells.addAll(collCells)
				}
			}

			if (checkCollisionWithEntities && !hasCollided) {
				updateCollidingState()
				if (isCollidingWithOneEntityValidForCollisionResponse()) {
					hasCollided = true
				}
			}

			if (hasCollided) {
				position.add(-xAdd, -yAdd)
				fixCollision(xAdd, yAdd, delta, false)
			}
		}
	}

	open fun filterMapCellForCollision(cell: MapCell): Boolean {
		return cell.isSolid
	}

	/** Which entities are ignored / allowed *before* doing collision check
	 * @return whether to allow the Entity for collision checking */
	open fun filterEntityForPossibleCollision(entity: Entity): Boolean {
		return entity.isSolid
	}

	private fun isCollidingWithOneEntityValidForCollisionResponse(): Boolean {
		for (entity in tmpPossibleCollidingEnts) {
			if (filterEntityForPossibleCollision(entity) && this.isCollidingWith(entity, collisionResponseBB, othersCollisionResponseBB)) {
				return true
			}
		}

		return false
	}

	private fun findAllCollidingEntitiesValidForCollisionResponse(arrayToPopulate: GdxSet<Entity>) {
		for (entity in tmpPossibleCollidingEnts) {
			if (filterEntityForPossibleCollision(entity) && this.isCollidingWith(entity, collisionResponseBB, othersCollisionResponseBB)) {
				arrayToPopulate.add(entity)
			}
		}
	}

	/** [updateCollidingState] first */
	fun isCollidingWithValidEntitiesOrMapCell(): Boolean {
		return (checkCollisionWithEntities && isCollidingWithOneEntityValidForCollisionResponse())
				|| (checkCollisionWithMapCells && isCollidingWithMapCell())
	}

	/** Default behavior: Can slide on Entities that [Entity.isSolidSurfaceThatAllowsSliding] but not on any [CollidingEntity] that would be pushed when moving */
	open fun allowSlidingOnEntity(ent: Entity): Boolean = ent.isSolidSurfaceThatAllowsSliding && !(pushEntities && ent is CollidingEntity && allowPushingEnt(ent))

	/** Default behavior: Entities can be pushed if those Entities push too */
	open fun allowPushingEnt(ent: CollidingEntity): Boolean = ent.pushEntities

	private var isDoingCollisionResponse: Boolean = false

	private fun precision(xAdd: Float, yAdd: Float): Float {
		return if (xAdd == 0f && yAdd == 0f)
			0.25.pixelsToUnits
		else
			Math.min(0.25.pixelsToUnits, Math.max(Math.abs(xAdd), Math.abs(yAdd)) / 25f)
	}

	private fun fixCollision(xAdd: Float, yAdd: Float, delta: Float, disableSlidingCheck: Boolean) {
		if (isDoingCollisionResponse) return
		isDoingCollisionResponse = true
		val signX = sign(xAdd).toInt()
		val signY = sign(yAdd).toInt()

		val precision = precision(xAdd, yAdd)

		if (!disableSlidingCheck && slideOnCorners) {
			var cancel = false

			// go back to collision position, check if you can slide on the entities found there
			position.add(xAdd, yAdd)
			for (ent in tmpPossibleCollidingEnts) {
				if (!allowSlidingOnEntity(ent))
					if (isCollidingWith(ent, collisionResponseBB, othersCollisionResponseBB)) {
						cancel = true
						break
					}
			}

			position.add(-xAdd, -yAdd)

			if (!cancel) {
				val origX = position.x
				val origY = position.y

				fun slide(slideOnX: Boolean): Boolean {
					//todo add sliding to solid map cells
					//todo fix sliding. Get correct bbSize
					// add the correspondent bb size to the qty to check. Assumes the entity is centered on its BB
					val bbSize = if (slideOnX) getBoundingBox(collisionResponseBB).width else getBoundingBox(collisionResponseBB).height
					val qty = slideMarginPixels + Math.abs((bbSize / 2f).unitsToPixels.toInt())

					// only slide when you are moving in the relevant axis at least x times more than the other
					if (Math.abs(if (slideOnX) yAdd else xAdd) / Math.abs(if (slideOnX) xAdd else yAdd) > 2f) {
						var slidingSign = 1

						var finished = -1
						for (i in 1..qty step 3) {
							repeat(2) {
								if (finished == -1) {
									val add = i.pixelsToUnits * slidingSign
									position.add(if (slideOnX) add else xAdd, if (slideOnX) yAdd else add)

									if (checkCollisionWithEntities)
										updateCollidingState()

									if (!isCollidingWithValidEntitiesOrMapCell()) {
										finished = i
									} else {
										position.set(origX, origY)
										slidingSign *= -1
									}
								}
							}

							if (finished != -1) break
						}

						position.set(origX, origY)

						if (finished != -1) {
							val slidingSpeed = map(finished, 1, qty, 2.7f, 0.8f) * delta * slidingSign
//                            Engine.debugUI.drawDebugTextInWorldPosition(slidingSpeed.toString(), x, y, worldViewport)
							fixCollision(if (slideOnX) slidingSpeed else xAdd,
									if (slideOnX) yAdd else slidingSpeed, delta, true)
							isDoingCollisionResponse = false
							return true
						}
					}

					isDoingCollisionResponse = false
					return false
				}

				if (slide(true)) {
					isDoingCollisionResponse = false
					return
				}
				if (slide(false)) {
					isDoingCollisionResponse = false
					return
				}
			}
		}

		if (checkCollisionWithEntities)
			updateCollidingState()

		// was already colliding with solid?
		if (isCollidingWithValidEntitiesOrMapCell()) {
			if (suddenCollisionResponse == SuddenCollisionResponse.NEVER) {
				position.add(xAdd, yAdd)
				isDoingCollisionResponse = false
				return
			}

			fixCollisionCausedBeforeMovement(xAdd, yAdd)
		} else {
			// xAdded is used to check for the end on an axis movement, but it may not represent the actual movement done in that axis
			var xAdded = 0f
			var yAdded = 0f
			// these 2 actually represent the movement on each axis
			var actualXAdded = 0f
			var actualYAdded = 0f

			var xEnded = signX == 0
			var yEnded = signY == 0

			var attemptedPushX = false
			var attemptedPushY = false

			// first move precision amount on x, then on y. Stop on each coordinate when moved enough or when collided
			var yTurn = true
			var count = 0
			while (true) {
				count++
				yTurn = !yTurn
				val sign = if (!yTurn) signX else signY
				if ((!yTurn && !xEnded) || (yTurn && !yEnded)) {

					if (!yTurn) {
						xAdded += precision
						actualXAdded += precision
						position.add(precision * sign, 0f)
					} else {
						yAdded += precision
						actualYAdded += precision
						position.add(0f, precision * sign)
					}

					if (checkCollisionWithEntities)
						updateCollidingState()

					val collidedEnts = (checkCollisionWithEntities && isCollidingWithOneEntityValidForCollisionResponse())
					val collidedCells = (checkCollisionWithMapCells && isCollidingWithMapCell())
					val collided = collidedCells || collidedEnts

					val movedAllQty = if (!yTurn) xAdded >= abs(xAdd) else yAdded >= abs(yAdd)

					// stop when collided with solid or when you moved all the original quantity moved
					if (collided || movedAllQty) {
						// check if, after colliding in one axis, you could keep moving without collision in the other axis
						// this allows for sliding against irregular surfaces
						var canContinue = true // if slide was successful, this becomes false to cancel the finishing of movement
						if (slideOnAngledSurfaces && collided && !movedAllQty) {
							var sign = 1
							repeat(2) {
								val factor = 3.5f // for allowing to slide when direction is perpendicular, precision needs to be slightly bigger. Bit of a hack
								val friction = 1.55f
								if (yTurn)
									position.x += (precision * factor) * sign
								else
									position.y += (precision * factor) * sign

								if ((!checkCollisionWithEntities || !isCollidingWithOneEntityValidForCollisionResponse())
										&& (!checkCollisionWithMapCells || !isCollidingWithMapCell())) {
									if (yTurn) {
//										position.x += precision * factor * sign
										yAdded += precision * factor * friction
									} else {
//										position.y += precision * factor * sign
										xAdded += precision * factor * friction
									}

									canContinue = false
									return@repeat
								}

								if (yTurn)
									position.x -= (precision * factor) * sign
								else
									position.y -= (precision * factor) * sign

								sign *= -1
							}
						}

						if (canContinue) {
							if (!yTurn)
								xEnded = true
							else
								yEnded = true

							if (collided) { // stopped because of solid collision
								if (!yTurn) {
									hasCollidedX = sign
									findAllCollidingEntitiesValidForCollisionResponse(collidingEntitiesX)
									// go back before the collision
									position.add(-precision * sign, 0f)
								} else {
									hasCollidedY = sign
									findAllCollidingEntitiesValidForCollisionResponse(collidingEntitiesY)

									// go back before the collision
									position.add(0f, -precision * sign)
								}

								if (pushEntities && checkCollisionWithEntities && !movedAllQty) {
									updateCollidingState()
									if (!yTurn && !attemptedPushX) {
										actualXAdded -= precision * sign
										attemptedPushX = true
										xEnded = false
										for (ent in collidingEntitiesX) {
											if (ent is CollidingEntity)
												if (allowPushingEnt(ent))
													ent.justGotPushed((abs(xAdd) - actualXAdded) * sign * pushMult * ent.bePushedMult, 0f,
															this, delta)
//                                            Engine.debugUI.drawDebugTextInWorldPosition(((xTotalMoved - xAdded) * sign).toString(),
//                                                    x, y, worldViewport)
//                                            Engine.debugUI.drawDebugTextInWorldPosition((xAdd).toString(),
//                                                    x, y - 1f, worldViewport)
										}
									}

									if (yTurn && !attemptedPushY) {
										actualYAdded -= precision * sign
										attemptedPushY = true
										yEnded = false
										for (ent in collidingEntitiesY) {
											if (ent is CollidingEntity)
												if (allowPushingEnt(ent))
													ent.justGotPushed(0f, (abs(yAdd) - actualYAdded) * sign * pushMult * ent.bePushedMult,
															this, delta)
										}
									}
								}
							}
						}
					}
				}

				if (xEnded && yEnded) {
					break
				}
			}

//            Engine.debugUI.drawTextInWorld(count.toString(), x + Utils.randomFloat(0f, 2f, true), y + Utils.randomFloat(0f, 2f, true), worldViewport, Color.RED, disappearTime = 1f)
		}

		isDoingCollisionResponse = false

		collidingEntities.addAll(collidingEntitiesX)
		collidingEntities.addAll(collidingEntitiesY)
	}

	private fun fixCollisionCausedBeforeMovement(xAdd: Float, yAdd: Float) {
		var count = 0

		if (checkCollisionWithEntities) {
			findAllCollidingEntitiesValidForCollisionResponse(collidingEntitiesX)
			collidingEntitiesY.addAll(collidingEntitiesX)
		}

		var precision = precision(xAdd, yAdd)

		var angle = Utils.angleBetween(0f, 0f, xAdd, yAdd)
		while (count < 330) {
			var finish = false
			count++

			if (count > 180) precision *= 1.5f

			val dist = count.float.pow(1.25f)
			repeat(8) {
				if (!finish) {
					val xx = precision * Utils.angleMovementX(dist, angle)
					val yy = precision * Utils.angleMovementY(dist, angle)
					x += xx
					y += yy

					if (checkCollisionWithEntities)
						updateCollidingState()

					if (!isCollidingWithValidEntitiesOrMapCell()) {
						finish = true
						return@repeat
					} else {
						x -= xx
						y -= yy
						angle += 360 / 8f
					}
				}
			}
			if (finish) break
		}
	}

	private fun isCollidingWithMapCell(): Boolean {
		return !getCollidingMapCells(collisionResponseBB) { filterMapCellForCollision(it) }.isEmpty
	}

	override fun saveEntity(): JsonValue {
		return super.saveEntity().also {
			it.addChildValue("speed", Engine.json.toJson(speed))
			it.addChildValue("collisionWithMapCells", checkCollisionWithMapCells)
			it.addChildValue("doCollisionResponse", doCollisionResponse)
			it.addChildValue("collisionResponseIncludesDynamicEnts", collisionResponseIncludesDynamicEnts)
//			it.addChildValue("jumpWhenInsideCollision", suddenCollisionResponse)
		}
	}

	override fun loadEntity(jsonValue: JsonValue, saveVersion: String) {
		super.loadEntity(jsonValue, saveVersion)
		speed.set(Engine.json.fromJson(Vector2::class.java, jsonValue.getString("speed")))
		checkCollisionWithMapCells = jsonValue.getBoolean("collisionWithMapCells")
		doCollisionResponse = jsonValue.getBoolean("doCollisionResponse")
		collisionResponseIncludesDynamicEnts = jsonValue.getBoolean("collisionResponseIncludesDynamicEnts")
//		suddenCollisionResponse = jsonValue.getBoolean("jumpWhenInsideCollision")
	}
}
