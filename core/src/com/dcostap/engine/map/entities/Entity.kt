package com.dcostap.engine.map.entities

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.*
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.*
import com.badlogic.gdx.utils.Array
import com.dcostap.Engine
import com.dcostap.engine.debug.log
import com.dcostap.engine.map.DrawableSortable
import com.dcostap.engine.map.EntityMap
import com.dcostap.engine.map.MapCell
import com.dcostap.engine.map.map_loading.CustomProperties
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.actions.ActionsUpdater
import com.dcostap.engine.utils.input.InputController
import com.dcostap.engine.utils.ui.ExtLabel
import com.dcostap.engine.utils.ui.ExtTable
import com.dcostap.engine.utils.ui.ResizableActorTable
import com.kotcrab.vis.ui.VisUI
import com.kotcrab.vis.ui.widget.CollapsibleWidget
import com.kotcrab.vis.ui.widget.VisTextButton
import com.kotcrab.vis.ui.widget.VisWindow
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel
import com.kotcrab.vis.ui.widget.spinner.SimpleFloatSpinnerModel
import ktx.actors.onChange
import ktx.collections.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.sign

/**
 * Created by Darius on 14/09/2017.
 *
 * Entities are instances of the game world, stored and updated on a [EntityMap].
 *
 * Default constructor creates a non-solid non-static Entity positioned at 0, 0 with boundingBox of size almost 0
 *
 * BoundingBox created in the constructor is the default BB; Entities may have more than one, each one identified by a
 * name (default BB's name is "default").
 *
 * Any BB may be added / modified later (use [addBB]), however if the Entity is
 * static any modification will throw an Exception unless it is done before [isInMap] (map will actually
 * add the Entity later after its creation, normally 1 frame later - can be forced with [EntityMap.removeAndAddEntities])
 *
 * Position modification on static entities is forbidden as well in the same way
 *
 * **If [providesCollidingInfo] is false entity will never be included in the map's colliding tree for entities.
 * This means no other Entity will be able to find this one using that colliding tree, thus reducing overhead.
 * This applies to collision checking or just finding close Entities. Note you can always use [EntityMap.collTreeForCullingAndDrawing]
 * whenever you need all Entities**. Note that not providing collInfo doesn't mean the Entity itself can't check for collisions!
 *
 *
 * Static entities:
 * - can't move from initial position.
 * - will be included in map's quadTree as a static entity, with information which won't change until it is removed;
 * they are more efficient than dynamic entities, specially if those are _actually_ moving.
 * - MapCells occupied by its default BB will store references to the Entity if [providesStaticInfoToCells]. These Static Entities
 * can listen to changes from the occupied MapCells implementing [MapCell.ChangeListener].
 *
 * Dynamic entities:
 * - can move!
 * - will be included in map's collisionTree as a dynamic entity, which means it will be updated each frame on the collisionTree
 * whenever it moves. A Dynamic Entity not moving is _almost_, but not quite, as good as a Static Entity in terms of performance.
 *
 * @param isSolid         Flag that may be used for collision response in [CollidingEntity].
 */
open class Entity @JvmOverloads constructor(val position: Vector2 = Vector2(), rectBB: Rectangle? = null,
											var isSolid: Boolean = false, isStatic: Boolean = false,
											providesCollidingInfo: Boolean = Engine.ENTITIES_PROVIDE_COLL_INFO_DEFAULT,
											isActivated: Boolean = true,
											providesStaticInfoToCells: Boolean = true)
	: Updatable, DrawableSortable, Disposable {
	var isStatic = isStatic
		set(value) {
			if (isInMap) throw RuntimeException("isStatic value can't be changed once added to a map!")
			field = value
		}

	var providesCollidingInfo = providesCollidingInfo
		set(value) {
			if (isInMap) throw RuntimeException("providesCollidingInfo value can't be changed once added to a map!")
			field = value
		}

	var isDebugFocus = false
	var isDebugPriority = false

	/** Assumes the Entity is still being drawn each frame. Perfect for usage with [isActivated] set to false */
	var secondsSinceLastUpdate = 0.0
		private set

	var isInsideCamera = true

	/** If false, avoids [CollidingEntity] with [CollidingEntity.slideOnCorners] set to true from sliding on this Entity
	 * Only intended for entities with a [RectBoundingBox] */
	var isSolidSurfaceThatAllowsSliding: Boolean = false

	var drawingScale = Vector2(1f, 1f)
	var drawingOffset = Vector2()
	var drawingAlpha = 1f
	var drawingRotation = 0f
	var drawingColor: Color? = null

	var speed = Vector2()
		private set

	var acceleration = Vector2()
		private set

	var maxSpeed = Float.MAX_VALUE

	var friction
		set(value) {
			frictionY = value
			frictionX = value
		}
		get() = (frictionX + frictionY) / 2f

	var frictionX = 0f
	var frictionY = 0f

	private var _lastFramePosition: Vector2? = null

	val lastFramePosition: Vector2
		get() {
			return _lastFramePosition ?: position
		}

	/** Unlike [speed], which represents how much the Entity wants to move, [lastFrameMovement] represents how much it _actually_ moved */
	val lastFrameMovement = Vector2()
		get() {
			field.set(position)
			return field.sub(lastFramePosition)
		}

	/** Specifies custom [getDrawingY], or [position.y] will be used if this is null */
	var drawY: Float? = null

//	/** todo: actual usages? if false entity will always be drawn even if outside the camera rectangle */
//	private var providesCullingInfo: Boolean = true
//		set(value) {
//			if (isInMap) throw RuntimeException("providesCullingInfo value can't be changed once added to a map!")
//			field = value
//		}

	/** Deactivated entities aren't updated. That can save resources when there are tons of entities (update loop gets really big) */
	var isActivated: Boolean = isActivated
		set(value) {
			if (field != value) {
				field = value

				if (isInMap) {
					map.updateEntityActivationState(this)
				}
			}
		}

	/** if true, static entities will modify colliding mapCells when added / removed to the map */
	var providesStaticInfoToCells: Boolean = providesStaticInfoToCells
		set(value) {
			if (isInMap) throw RuntimeException("providesStaticInfoToCells value can't be changed once added to a map!")
			field = value
		}

	val collTree get() = map.collTreeForEntityColliding
	val worldViewport get() = map.worldViewport

	/** Higher depth = further away from the camera. If two entities have same depth, y position is used to determine who is drawn first
	 * @see DrawableSortable.getDrawingDepth */
	var depth = 0

	/** @see DrawableSortable.getDrawingYDepth */
	var yDepth = ++Engine.globalEntityCounter

	private var currentMap: EntityMap? = null

	val map: EntityMap
		get() = currentMap ?: throw RuntimeException("Entity $this has no map")

	val isInMap get() = currentMap != null

	var actions = ActionsUpdater()
		private set

	var customProperties = CustomProperties()

	var isDisposed = false
		private set

	var hasMoved = true

	var boundingBoxes: GdxMap<String, BoundingBox>
		protected set

	var disableInternalPhysics = false

	/** @return The **default** bounding box */
	val boundingBox: BoundingBox
		get() = getBoundingBox("default")

	init {
		boundingBoxes = GdxMap()
		addDefaultBB(rectBB ?: newRectanglePixels(0f, 0f, 2, 2, true, true))
	}

	fun addBB(bb: BoundingBox) {
		boundingBoxes.put(bb.name, bb)
	}

	fun addBB(name: String, shape2D: Shape2D) {
		when (shape2D) {
			is Rectangle -> this.addBB(RectBoundingBox(this, name, shape2D))
			is Circle -> this.addBB(CircleBoundingBox(this, name, shape2D))
			is Polygon -> this.addBB(PolyBoundingBox(this, name, shape2D))
		}
	}

	fun addDefaultBB(shape2D: Shape2D) {
		this.addBB("default", shape2D)
	}

	/** @param name Identifier of the bounding box, by default the **default** BB
	 * @return The absolute positioned Rectangle that represents the bounding box */
	fun getBoundingBox(name: String = "default"): BoundingBox {
		return boundingBoxes.get(name)!!
	}

	fun getBoundingBoxOrNull(name: String = "default"): BoundingBox? {
		return boundingBoxes.get(name)
	}

	fun removeBoundingBox(name: String) {
		boundingBoxes.remove(name)
	}

	fun isCollidingWith(otherEntity: Entity, thisBB: String = "default", otherBB: String = "default"): Boolean {
		return getBoundingBox(thisBB).collidesWith(otherEntity.getBoundingBox(otherBB))
	}

	open fun justAddedToMap(map: EntityMap) {
		currentMap = map
	}

	open fun justBeforeAddingToMap(map: EntityMap) {

	}

	open fun justRemovedFromMap(map: EntityMap) {
		currentMap = null
	}

	private var wasUpdated = true

	open fun firstUpdate(delta: Float) {

	}

	private var firstUpdate = true

//	private var steeringDelayUpdate = 0f//todo remove it or improve it
//	private var steeringTimer = 0f

	override fun update(delta: Float) {
		if (_lastFramePosition == null) _lastFramePosition = Vector2(position)

		wasUpdated = true
		secondsSinceLastUpdate = delta.toDouble()

		if (isInMap && _lastFramePosition != position) {
			hasMoved = true

			if (isStatic)
				throw RuntimeException("Moved a static entity which is already added to a map: " + javaClass.simpleName)
		}

		if (firstUpdate) {
			firstUpdate(delta)
			firstUpdate = false
		}

		_lastFramePosition!!.set(position)

		actions.update(delta)

//		if (steeringDelayUpdate > 0f) {
//			steeringTimer += delta
//			if (steeringTimer >= steeringDelayUpdate) {
//				steering?.update(steeringTimer)
//				steeringTimer = 0f
//			}
//		} else {
//			steering?.update(delta)
//		}
		applyMovement(delta)
	}

	open fun applyMovement(delta: Float) {
		if (!disableInternalPhysics) {
			if (!acceleration.isZero) {
				acceleration.scl(delta)
				speed.add(acceleration)
			}

			if (frictionX != 0f) speed.x = (max(abs(speed.x) - frictionX * delta, 0f)) * sign(speed.x)
			if (frictionY != 0f) speed.y = (max(abs(speed.y) - frictionY * delta, 0f)) * sign(speed.y)

			if (!speed.isZero) {
				speed.clamp(0f, maxSpeed)
				position.add(speed.x * delta, speed.y * delta)
			}
		}

		if (Engine.DEBUG_ENTITY_PHYSICS) {
			val usedDelta = if (delta <= 0f) 0.001f else delta
			if (!acceleration.isZero)
				Engine.debugUI.drawLineInWorld(x, y, x + acceleration.x * (1 / usedDelta), y + acceleration.y * (1 / usedDelta),
						worldViewport, isArrow = true, disappearTime = 0f, color = Color.RED)
			if (!speed.isZero)
				Engine.debugUI.drawLineInWorld(x, y, x + speed.x * (1 / usedDelta) / 60f, y + speed.y * (1 / usedDelta) / 60f,
						worldViewport, isArrow = true, disappearTime = 0f, color = Color.CYAN)
		}

		acceleration.setZero()
	}

	override fun draw(gd: GameDrawer, delta: Float) {
		gd.scaleX *= drawingScale.x
		gd.scaleY *= drawingScale.y
		gd.rotation += drawingRotation
		gd.alpha *= drawingAlpha
		drawingColor.ifNotNull { gd.color.set(it) }
		gd.drawingOffset.add(drawingOffset)

		if (!wasUpdated)
			secondsSinceLastUpdate += delta

		wasUpdated = false

		if (!Engine.DEBUG_ENTITIES_BB_X_RAY && Engine.DEBUG_ENTITIES_BB) drawDebug(gd, delta)
		debugUpdate()
	}

	//region debug
	private fun debugUpdate() {
		if (!Engine.DEBUG) return

		if (initDebug) {
			if (!this::debugTable.isInitialized) debugTable = ResizableActorTable()
			if (!this::debugTableChild.isInitialized) debugTableChild = Table()

			initDebug = false

			debugTableChild.defaults().left()

			val previousColor = ExtLabel.defaultColor
			ExtLabel.defaultColor = Color.WHITE

			// debug UI table
			ExtLabel.defaultFont = VisUI.getSkin().getFont("small-font")
			ExtLabel.defaultColor = Color.WHITE

			debugTable.setPosition(-999999f, -999999f)
			debugTable.add(ExtTable().also {
				it.align(Align.top)
				it.top()
				it.background = VisUI.getSkin().getDrawable("window-bg")
				it.pad(4f)

				it.updateFunction = { _ ->
					if (debugTablePinned) it.setColor(1f, 1f, 0f, 1f) else it.setColor(1f, 1f, 1f, 1f)
				}

				it.add(ExtTable().also {
					it.top()
					val previousColor = ExtLabel.defaultColor
					ExtLabel.defaultColor = Color.WHITE
					it.add(ExtLabel(this.javaClass.simpleName))
					it.add(Utils.visUI_customCheckBox("Hide", { false }).also {
						it.onChange {
							debugTableTempInvisible = true
						}
					}).padLeft(8f)
					ExtLabel.defaultColor = previousColor
				})

				it.row()

				it.add(debugTableChild).grow()
			}).top()

			debugTable.isVisible = false

			createDebugInfoTable(debugTableChild)

			ExtLabel.defaultColor = previousColor
		}

		if (Engine.DEBUG_UI_ENTITY_INFO_NAMES && !(Engine.DEBUG_ONLY_DYNAMIC_ENTS && isStatic)
				&& !(Engine.DEBUG_ONLY_PRIORITY_ENTS && !isDebugPriority) && isInMap) {
			val xRef = Utils.projectPosition(x, y, worldViewport, Engine.debugUI.stage.viewport).x
			var size = xRef - Utils.projectPosition(x + 1f, y, worldViewport, Engine.debugUI.stage.viewport).x
			size = 1 / size
			Engine.debugUI.drawTextInWorld(this.javaClass.simpleName,
					x - size, y + size, worldViewport, Color.BLACK, 0.7f, 0f)
			Engine.debugUI.drawTextInWorld(this.javaClass.simpleName, x, y, worldViewport, Color.SKY, 1f, 0f)
		}

		if (debugTableTempInvisible && !Engine.DEBUG_UI_ENTITY_INFO_ABOVE) debugTableTempInvisible = false
		if (debugTablePinned && (!Engine.DEBUG_UI_ENTITY_INFO_POPUP || Engine.DEBUG_UI_ENTITY_INFO_ABOVE)) debugTablePinned = false

		val wasVisible = debugTable.isVisible
		debugTable.isVisible = debugInfoIsVisible()
		if (debugTable.isVisible != wasVisible) {
			if (debugTable.isVisible)
				Engine.debugUI.stage.addActor(debugTable)
			else
				debugTable.remove()
		}

		debugTableMouseOver = false

		if (debugTable.ancestorsVisible()) {
			isDebugFocus = true
			if (isInMap) {
				val coords = Pools.obtain(Vector2::class.java)
				debugTable.pack()

				if (Engine.DEBUG_UI_ENTITY_INFO_POPUP && !Engine.DEBUG_UI_ENTITY_INFO_ABOVE) {
					if (!debugTablePinned)
						coords.set(Utils.projectPosition(map.worldInput.mouseWorld.x,
								map.worldInput.mouseWorld.y, map.worldViewport, Engine.debugUI.stage.viewport))
					if (!Engine.keyboardFocusInDebugUI && Gdx.input.isKeyJustPressed(Engine.DEBUG_UI_ENTITY_POPUP_PIN_KEY)) {
//						debugTablePinned = !debugTablePinned
						Engine.debugUI.stage.addActor(VisWindow(javaClass.simpleName).also {
							it.top()
							it.add(ExtTable().also {
								it.add(VisTextButton("locate").also {
									it.onChange {
										debugFlashingThing.flashColor(Color.RED, 1f, 1f)
										debugDrawCircleInWorld(x, y, 10f, Color.RED, disappearTime = 0.1f)
									}
								})
								it.row()
								createDebugInfoTable(it)
								it.updateFunction = { isDebugFocus = true }
							})
							it.setPosition(debugTable.x, debugTable.y)
							it.addCloseButton()
							it.isResizable = true
							it.setResizeBorder(10)
							it.pack()
							it.width *= 1.5f
							it.height *= 1.5f
							it.pack()
						})
					}
				} else {
					coords.set(Utils.projectPosition(x, y, map.worldViewport, Engine.debugUI.stage.viewport))
				}

				if (!debugTablePinned)
					debugTable.setPosition(coords.x, coords.y - 88f)

				Pools.free(coords)
			}
		} else
			isDebugFocus = false
	}

	var debugFlashingThing = FlashingThing()

	open fun drawDebug(gd: GameDrawer, delta: Float) {
		val prevAlpha = gd.alpha
		val prevColor = gd.color
		gd.alpha = Engine.DEBUG_TRANSPARENCY
		var i = 0
		for (bb in boundingBoxes.values()) {
			gd.color = (if (isStatic) debugStaticColors else debugDynamicColors).get(i % 3)
			gd.drawShape(bb.shape2D, Engine.DEBUG_LINE_THICKNESS, false)
			if (isSolid) gd.drawShape(bb.shape2D, Engine.DEBUG_LINE_THICKNESS, false, scaleX = 0.7f, scaleY = 0.7f)

			i++
		}

		gd.color = Color.CHARTREUSE
		gd.alpha = 0.5f
		gd.drawCross(x, y, Engine.DEBUG_LINE_THICKNESS * 4f, Engine.DEBUG_LINE_THICKNESS * 0.9f)

		gd.resetAlpha()
		gd.resetColor()

		debugFlashingThing.update()

		if (debugFlashingThing.isFlashing) {
			debugFlashingThing.setupGameDrawer(gd)

			gd.drawShape(boundingBox.shape2D, 0f, true)

			debugFlashingThing.resetGameDrawer(gd)
		}

		gd.alpha = prevAlpha
		gd.color = prevColor
	}

	private val dummyCellArray = Array<MapCell>()

	fun debugTouchInput(inputController: InputController) {
		if (Engine.DEBUG && showDebugTable && Engine.DEBUG_UI_ENTITY_INFO_POPUP) {
			if (boundingBox is RectBoundingBox)
				pool(Rectangle::class) {
					it.set((boundingBox as RectBoundingBox).rect)
					if (it.width <= 10.pixelsToUnits) {
						it.width = 10.pixelsToUnits
						it.x = x - it.width / 2f
					}
					if (it.height <= 10.pixelsToUnits) {
						it.height = 10.pixelsToUnits
						it.y = y - it.height / 2f
					}
					if (it.contains(inputController.mouseWorld.x, inputController.mouseWorld.y)) {
						debugTableMouseOver = true
					}
				}

			if (boundingBox is CircleBoundingBox)
				pool(Circle::class) {
					it.set((boundingBox as CircleBoundingBox).circle)
					if (it.radius < 5.pixelsToUnits) it.radius = 5.pixelsToUnits
					if (it.contains(inputController.mouseWorld.x, inputController.mouseWorld.y)) {
						debugTableMouseOver = true
					}
				}

			if (boundingBox is PolyBoundingBox)
				if (boundingBox.collidesWith(inputController.mouseWorld.x, inputController.mouseWorld.y)) {
					debugTableMouseOver = true
				}
		}
	}
	//endregion

	/**
	 * Use this to check for solid collision without having to do the expensive call to the CollisionTree,
	 * if the Entity will only collide with the boundaries of the map cells
	 *
	 *
	 * Limitations compared to checking collision against Entities:
	 *  * Solid collision limited to mapCell's boundaries, no precise collision checking on arbitrary positions
	 *  * MapCells only provide info about Static Entities above them (provided they [providesStaticInfoToCells]),
	 *  or those where [isSolid] is true
	 *
	 *  @see [MapCell.isSolid]
	 */
	@JvmOverloads
	fun getCollidingMapCells(thisBB: String = "default", filter: (MapCell) -> Boolean = { true }): GdxArray<MapCell> {
		dummyCellArray.clear()
		for (mapCell in map.mainCellLayer.getCellsOccupiedBy(getBoundingBox(thisBB).shape2D)) {
			if (filter(mapCell)) {
				dummyCellArray.add(mapCell)
			}
		}

		return dummyCellArray
	}

	var x: Float
		get() = position.x
		set(value) {
			position.x = value
		}

	var y: Float
		get() = position.y
		set(value) {
			position.y = value
		}

	fun getTiledPosition(vectorToModify: Vector2): Vector2 {
		return vectorToModify.set(tiledX.toFloat(), tiledY.toFloat())
	}

	fun getTiledPosition(gridPointToModify: GridPoint2): GridPoint2 {
		return gridPointToModify.set(tiledX, tiledY)
	}

	val tiledX: Int
		get() = position.x.floor()

	val tiledY: Int
		get() = position.y.floor()

	override fun toString(): String {
		return (javaClass.simpleName + "(" + (if (isSolid) "solid" else "notSolid") + ", "
				+ (if (isStatic) "static" else "notStatic") + ", " + (if (isDisposed) "disposed" else "notDisposed") + ")"
				+ "; hash: " + hashCode())
	}

	/** @param forever if false the entity may be added again to another map, otherwise it'll be disposed */
	open fun removeFromMap(forever: Boolean = true) {
		currentMap?.removeEntity(this, forever)
	}

	fun addToMap(map: EntityMap) {
		map.addEntity(this)
	}

	override fun getDrawingY(): Float {
		return drawY ?: y
	}

	override fun getDrawingDepth(): Int {
		return depth
	}

	override fun getDrawingYDepth(): Int {
		return yDepth
	}

	/** Useful function for correctly handling touch input in a [EntityMap].
	 * Whenever this method is called it doesn't mean that input is above the Entity,
	 * it just asks whether you wanna handle it (return true) and thus stop the propagation
	 * @see [DrawableSortable.handleTouchInput]
	 * @return if true, input propagation will stop */
	override fun handleTouchInput(worldInput: InputController, stageInput: InputController?): Boolean {
		return false
	}

	var showDebugTable: Boolean = true

	private lateinit var debugTable: ResizableActorTable
	private var debugTableMouseOver = false
	private var debugTableTempInvisible = false

	private var initDebug = true
	private lateinit var debugTableChild: Table

	open fun createDebugInfoTable(contents: Table) {
		contents.add(Table().also {
			it.add(ExtLabel(textUpdateDelay = 0.15f) { "x" })

			if (isStatic) {
				it.add(ExtLabel(textUpdateDelay = 0.15f, color = newColorFrom255RGB(255, 200, 200)) { " " + Utils.formatNumber(x, 2) })
			} else {
				val spinner = SimpleFloatSpinnerModel(x, -100000000f, 100000000f, 0.25f, 3)
				it.add(Utils.visUI_customFloatSpinner(
						"", spinner, { x = it }, { x }
				)).minWidth(91f)
			}

			it.row()
			it.add(ExtLabel(textUpdateDelay = 0.15f) { "y" })

			if (isStatic) {
				it.add(ExtLabel(textUpdateDelay = 0.15f) { " " + Utils.formatNumber(y, 2) })
			} else {
				val spinner = SimpleFloatSpinnerModel(y, -100000000f, 100000000f, 0.25f, 3)
				it.add(Utils.visUI_customFloatSpinner(
						"", spinner, { y = it }, { y }
				)).padLeft(10f).minWidth(91f)
			}
		})

		contents.row()
		contents.add(Table().also {
			it.add(Utils.visUI_customCheckBox("solid", { isSolid }).also {
				it.onChange { isSolid = !isSolid }
			})

			it.add(Utils.visUI_customCheckBox("static", { isStatic }).also {
				it.onChange { log("isStatic can't be changed once added to a map") }
			}).padLeft(8f)
		})

		contents.row()
		contents.add(Table().also {
			it.add(ExtLabel().also {
				it.textUpdateFunction = { "depth: " }
			})

			val spinner = IntSpinnerModel(depth, -100000, 100000, 1)
			it.add(Utils.visUI_customIntSpinner("depth", spinner, { depth = it }, { depth })).padLeft(10f)
		})
		contents.row()
		contents.add(CollapsibleWidget(Table().also {
			it.add(ExtLabel { "speed: ${speed.x.format(2, false)}" +
					", ${speed.y.format(2, false)}" })
			it.row()
			it.add(ExtLabel { "acceleration: ${acceleration.x.format(2, false)}" +
					", ${acceleration.y.format(2, false)}" })
			it.row()
		}, false))
	}

	private var previousDebugStageUIEntInfo = false
	private var debugTablePinned = false

	private fun debugInfoIsVisible() = (Engine.DEBUG && isInMap && !debugTableTempInvisible && showDebugTable &&
			(Engine.DEBUG_UI_ENTITY_INFO_ABOVE || (Engine.DEBUG_UI_ENTITY_INFO_POPUP && debugTableMouseOver || debugTablePinned)) &&
			!(isStatic && Engine.DEBUG_ONLY_DYNAMIC_ENTS) && !(!isDebugPriority && Engine.DEBUG_ONLY_PRIORITY_ENTS))

	/** todo: Saving Actions in [ActionsUpdater] */
	open fun saveEntity(): JsonValue {
		val json = Engine.json
		val jsonObj = JsonSavedObject()

		jsonObj.addChildValue("class", this.javaClass.name)

		jsonObj.addChildValue("bbs", JsonSavedObject().also {
			for (bb in boundingBoxes.values()) {
				it.addChild("bb", bb.save())
			}
		})

		jsonObj.addChildValue("isStatic", isStatic)
		jsonObj.addChildValue("isSolid", isSolid)
		jsonObj.addChildValue("depth", depth)

		jsonObj.addChildValue("position", json.toJson(position))

		jsonObj.addChildValue("isDisposed", isDisposed)

		jsonObj.addChildValue("customProperties", json.toJson(customProperties))

		jsonObj.addChildValue("providesCollidingInfo", providesCollidingInfo)
//		jsonObj.addChildValue("providesCullingInfo", providesCullingInfo)
		jsonObj.addChildValue("providesStaticInfoToCells", providesStaticInfoToCells)
		jsonObj.addChildValue("depth", depth)
		jsonObj.addChildValue("yDepth", yDepth)
		jsonObj.addChildValue("isActivated", isActivated)

		return jsonObj
	}

	open fun loadEntity(jsonValue: JsonValue, saveVersion: String) {
		boundingBoxes.clear()

		for (bb in jsonValue.get("bbs")) {
			val name = bb.getString("name")
			//todo redo save bb code now that there's circles
//            boundingBoxes.put(name, BoundingBox(this, name).also {
//                it.load(bb, "0")
//            })
		}

		isStatic = jsonValue.getBoolean("isStatic")
		isSolid = jsonValue.getBoolean("isSolid")
		isDisposed = jsonValue.getBoolean("isDisposed")
		depth = jsonValue.getInt("depth")

		providesCollidingInfo = jsonValue.getBoolean("providesCollidingInfo")
//		providesCullingInfo = jsonValue.getBoolean("providesCullingInfo")
		providesStaticInfoToCells = jsonValue.getBoolean("providesStaticInfoToCells")
		depth = jsonValue.getInt("depth")
		yDepth = jsonValue.getInt("yDepth")
		isActivated = jsonValue.getBoolean("isActivated")

		customProperties = Engine.json.fromJson(CustomProperties::class.java, jsonValue.getString("customProperties"))

		position.set(Engine.json.fromJson(Vector2::class.java, jsonValue.getString("position")))
	}

	fun removeForever() {
		currentMap?.removeEntity(this, true) ?: dispose()
	}

	/** Don't call this directly. Use [removeForever] or [removeFromMap] */
	override fun dispose() {
		isDisposed = true

		if (this::debugTable.isInitialized)
			debugTable.remove()
		if (this::debugTableChild.isInitialized)
			debugTableChild.remove()
	}

	fun debugDrawTextInWorld(text: Any?, worldX: Float = x, worldY: Float = y,
							 color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawTextInWorld(text, worldX, worldY, worldViewport, color, alpha, disappearTime)
	}

	fun debugDrawCircleInWorld(worldX: Float = x, worldY: Float = y, radius: Float,
							   color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawCircleInWorld(worldX, worldY, radius, worldViewport, color, alpha, disappearTime)
	}

	fun debugDrawLineInWorld(x1: Float, y1: Float, x2: Float, y2: Float,
							 thickness: Float = 1.1f, isArrow: Boolean = false, color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawLineInWorld(x1, y1, x2, y2, worldViewport, thickness, isArrow, color, alpha, disappearTime)
	}

	fun debugDrawRectInWorld(x: Number = 0f, y: Number = 0f, width: Number = 0f, height: Number = 0f, fill: Boolean = true,
							 borderThickness: Float = Engine.DEBUG_LINE_THICKNESS, color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawRectInWorld(x, y, width, height, worldViewport, fill, borderThickness, color, alpha, disappearTime)
	}

	fun debugDrawRectInWorld(rectangle: Rectangle, fill: Boolean = true,
							 borderThickness: Float = Engine.DEBUG_LINE_THICKNESS, color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		Engine.debugUI.drawRectInWorld(rectangle, worldViewport, fill, borderThickness, color, alpha, disappearTime)
	}

	companion object {
		val debugStaticColors = arrayOf(Color.ROYAL, Color.SKY, Color.TEAL)
		val debugDynamicColors = arrayOf(Color.RED, Color.MAROON, Color.PINK)
	}
}
