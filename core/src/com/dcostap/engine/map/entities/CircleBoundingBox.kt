package com.dcostap.engine.map.entities

import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.JsonValue
import com.dcostap.Engine
import com.dcostap.engine.utils.JsonSavedObject
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.addChildValue
import com.dcostap.engine.utils.pixelsToUnits

/**
 * Created by Darius on 21/03/2018.
 *
 * @see RectBoundingBox
 */
open class CircleBoundingBox(entity: Entity, name: String, val internalCircle: Circle) : BoundingBox(entity, name) {
	private val internalCircleInWorld = Circle(0f, 0f, 0f)

	val circle: Circle
		get() {
			updateWorldBB()
			return internalCircleInWorld
		}

	override fun collidesWith(rectangle: Rectangle): Boolean {
		return Intersector.overlaps(circle, rectangle)
	}

	//    val internalPoly = Polygon(FloatArray(8))
	override fun collidesWith(polygon: Polygon): Boolean {
//        internalPoly.vertices.let {
//            it[0] = circle.x - circle.radius
//            it[1] = circle.y - circle.radius
//
//            it[2] = circle.x - circle.radius
//            it[3] = circle.y + circle.radius
//
//            it[4] = circle.x + circle.radius
//            it[5] = circle.y + circle.radius
//
//            it[6] = circle.x + circle.radius
//            it[7] = circle.y - circle.radius
//        }
//
//        return Intersector.overlapConvexPolygons(polygon, internalPoly)

		return Utils.polygonOverlaps(polygon, circle)
	}

	override fun collidesWith(circle: Circle): Boolean {
		return this.circle.overlaps(circle)
	}

	override fun collidesWith(x: Float, y: Float): Boolean {
		return circle.contains(x, y)
	}

	override val shape2D get() = circle

	private var stopAdjustments = false

	override var width
		get() = internalCircle.radius * 2
		set(value) {
			checkIfAllowedToModify()
			internalCircle.radius = value / 2f
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			if (value < 0) throw RuntimeException("For some reason I didn't bother to check for now, negative bb's sizes give problems")
			wasModified = true
		}

	override var height
		get() = internalCircle.radius * 2
		set(value) {
			checkIfAllowedToModify()
			internalCircle.radius = value / 2f
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			if (value < 0) throw RuntimeException("For some reason I didn't bother to check for now, negative bb's sizes give problems")
			wasModified = true
		}

	override var offsetX
		get() = internalCircle.x
		set(value) {
			checkIfAllowedToModify()
			internalCircle.x = value
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			wasModified = true
		}

	override var offsetY
		get() = internalCircle.y
		set(value) {
			checkIfAllowedToModify()
			internalCircle.y = value
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			wasModified = true
		}

	var radius
		get() = internalCircle.radius
		set(value) {
			checkIfAllowedToModify()
			internalCircle.radius = value
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			wasModified = true
		}

	fun modify(circle: Circle) {
		stopAdjustments = true
		width = circle.radius * 2
		height = circle.radius * 2
		offsetX = circle.x
		offsetY = circle.x
		stopAdjustments = false
	}

	fun modifyPixels(offsetX: Number, offsetY: Number, radius: Number) {
		stopAdjustments = true
		this.width = radius.pixelsToUnits * 2f
		this.height = radius.pixelsToUnits * 2f
		this.offsetX = offsetX.pixelsToUnits
		this.offsetY = offsetY.pixelsToUnits
		stopAdjustments = false
	}

	/** world coordinates, when applying [offsetX] to [Entity.x] */
	override val x get() = internalCircleInWorld.x
	/** world coordinates, when applying [offsetY] to [Entity.y] */
	override val y get() = internalCircleInWorld.y

	private fun updateWorldBB() {
		internalCircleInWorld.x = entity.x + internalCircle.x
		internalCircleInWorld.y = entity.y + internalCircle.y
		internalCircleInWorld.radius = internalCircle.radius
	}

	/** BoundingBox size is reduced a bit to avoid BBs with size of 1 unit to occupy 2 cells, when the BB is snapped to the grid
	 *
	 *  (because when checking for occupied cells, if BB is at position 2, 2 with size of 1, 1; the algorithm will
	 * return cells 2, 2 and (2 + 1), (2 + 1) as occupied. But BB of size 1 is normally meant to occupy 1 cell only; this
	 * fixes it)  */
	private fun adjustBBSize() {
		if (internalCircle.radius == 0f) return  // don't do it if size is 0
		val bbSizeMargin = 0.01f
		if (internalCircle.radius % 1 == 0f)
			internalCircle.radius -= bbSizeMargin
	}

	override fun save(): JsonValue {
		val json = Engine.json
		return JsonSavedObject().also {
			it.addChildValue("name", name)
			it.addChildValue("internalCircle", json.toJson(internalCircle))
			it.addChildValue("internalCircleInWorld", json.toJson(internalCircleInWorld))
		}
	}

	override fun load(jsonValue: JsonValue, saveVersion: String) {
		val json = Engine.json
		internalCircle.set(json.fromJson(Circle::class.java, jsonValue.getString("internalCircle")))
		internalCircleInWorld.set(json.fromJson(Circle::class.java, jsonValue.getString("internalCircleInWorld")))
	}
}
