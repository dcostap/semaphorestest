package com.dcostap.engine.map.entities

import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.JsonValue
import com.dcostap.Engine
import com.dcostap.engine.utils.JsonSavedObject
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.addChildValue
import com.dcostap.engine.utils.pixelsToUnits

/**
 * Created by Darius on 21/03/2018.
 *
 * Holds information about the BB's width, height and offset in [internalRect].
 *
 * [rect] represents the BB in world, adapted to offset and entity's position.
 *
 * Modifying the relative BB (the only way to modify the actual BB) raises
 * exception if it is marked as [isStatic].
 *
 * When first creating the BoundingBox, set the size first, then activate the [isStatic] flag later (this
 * avoids raising exceptions while building the BB).
 */
open class RectBoundingBox(entity: Entity, name: String, val internalRect: Rectangle) : BoundingBox(entity, name) {
	private val internalRectInWorld = Rectangle(0f, 0f, 0f, 0f)

	override fun collidesWith(rectangle: Rectangle): Boolean {
		return rect.overlaps(rectangle)
	}

	override fun collidesWith(circle: Circle): Boolean {
		return Intersector.overlaps(circle, rect)
	}

	override fun collidesWith(x: Float, y: Float): Boolean {
		return rect.contains(x, y)
	}

	override fun collidesWith(polygon: Polygon): Boolean {
		return Utils.polygonOverlaps(polygon, rect)
	}

	override val shape2D get() = rect

	/** The BB rectangle positioned in the world according to [internalRect] */
	val rect: Rectangle
		get() {
			updateWorldBB()
			return internalRectInWorld
		}

	private var stopAdjustments = false

	override var width
		get() = internalRect.width
		set(value) {
			checkIfAllowedToModify()
			internalRect.width = value
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			wasModified = true
			if (value < 0) throw RuntimeException("For some reason I didn't bother to check for now, negative bb's sizes give problems")
		}

	override var height
		get() = internalRect.height
		set(value) {
			checkIfAllowedToModify()
			internalRect.height = value
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			wasModified = true
			if (value < 0) throw RuntimeException("For some reason I didn't bother to check for now, negative bb's sizes give problems")
		}

	override var offsetX
		get() = internalRect.x
		set(value) {
			checkIfAllowedToModify()
			internalRect.x = value
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			wasModified = true
		}

	override var offsetY
		get() = internalRect.y
		set(value) {
			checkIfAllowedToModify()
			internalRect.y = value
			if (!stopAdjustments) {
				adjustBBSize()
				updateWorldBB()
			}
			wasModified = true
		}

	fun modify(rectangle: Rectangle) {
		stopAdjustments = true
		width = rectangle.width
		height = rectangle.height
		offsetX = rectangle.x
		offsetY = rectangle.y
		stopAdjustments = false
	}

	fun modifyPixels(offsetX: Number, offsetY: Number, width: Number, height: Number, centerX: Boolean = false, centerY: Boolean = false) {
		stopAdjustments = true
		this.width = width.pixelsToUnits
		this.height = height.pixelsToUnits
		this.offsetX = offsetX.pixelsToUnits - if (centerX) this.width / 2f else 0f
		this.offsetY = offsetY.pixelsToUnits - if (centerY) this.height / 2f else 0f
		stopAdjustments = false
	}

	/** world coordinates, when applying [offsetX] to [Entity.x] */
	override val x get() = internalRectInWorld.x
	/** world coordinates, when applying [offsetY] to [Entity.y] */
	override val y get() = internalRectInWorld.y

	private fun updateWorldBB() {
		internalRectInWorld.x = entity.x + internalRect.x
		internalRectInWorld.y = entity.y + internalRect.y
		internalRectInWorld.width = internalRect.width
		internalRectInWorld.height = internalRect.height
	}

	/** BoundingBox size is reduced a bit to avoid BBs with size of 1 unit to occupy 2 cells, when the BB is snapped to the grid
	 *
	 *  (because when checking for occupied cells, if BB is at position 2, 2 with size of 1, 1; the algorithm will
	 * return cells 2, 2 and (2 + 1), (2 + 1) as occupied. But BB of size 1 is normally meant to occupy 1 cell only; this
	 * fixes it)  */
	private fun adjustBBSize() {
		if (internalRect.width == 0f && internalRect.height == 0f) return  // don't do it if size is 0
		val bbSizeMargin = 0.01f
		if (internalRect.width % 1 == 0f)
			internalRect.width -= bbSizeMargin

		if (internalRect.height % 1 == 0f)
			internalRect.height -= bbSizeMargin
	}

	override fun save(): JsonValue {
		val json = Engine.json
		return JsonSavedObject().also {
			it.addChildValue("name", name)
			it.addChildValue("internalRect", json.toJson(internalRect))
			it.addChildValue("internalRectInWorld", json.toJson(internalRectInWorld))
		}
	}

	override fun load(jsonValue: JsonValue, saveVersion: String) {
		val json = Engine.json
		internalRect.set(json.fromJson(Rectangle::class.java, jsonValue.getString("internalRect")))
		internalRectInWorld.set(json.fromJson(Rectangle::class.java, jsonValue.getString("internalRectInWorld")))
	}
}
