package com.dcostap.engine.map.entities

import com.badlogic.gdx.math.*
import com.dcostap.engine.utils.Saveable

abstract class BoundingBox constructor(val entity: Entity, val name: String) : Saveable {
	val isStatic get() = entity.isStatic

	abstract var width: Float
	abstract var height: Float
	abstract var offsetX: Float
	abstract var offsetY: Float

	/** world coordinates, when applying [offsetX] to [Entity.x] */
	abstract val x: Float
	/** world coordinates, when applying [offsetY] to [Entity.y] */
	abstract val y: Float

	var wasModified = false

	protected fun checkIfAllowedToModify() {
		if (isStatic && entity.isInMap)
			throw UnsupportedOperationException("Tried to modify bounding box of static Entity: " + javaClass.simpleName
					+ "\nThis is unsupported as it leads to bugs")
	}

	fun collidesWith(boundingBox: BoundingBox): Boolean {
		return when (boundingBox) {
			is RectBoundingBox -> collidesWith(boundingBox.rect)
			is CircleBoundingBox -> collidesWith(boundingBox.circle)
			is PolyBoundingBox -> collidesWith(boundingBox.poly)
			else -> false
		}
	}

	fun collidesWith(shape2D: Shape2D): Boolean {
		return when (shape2D) {
			is Rectangle -> collidesWith(shape2D)
			is Circle -> collidesWith(shape2D)
			is Polygon -> collidesWith(shape2D)
			else -> false
		}
	}

	abstract fun collidesWith(rectangle: Rectangle): Boolean
	abstract fun collidesWith(circle: Circle): Boolean
	abstract fun collidesWith(polygon: Polygon): Boolean
	abstract fun collidesWith(x: Float, y: Float): Boolean
	fun collidesWith(pos: Vector2) = collidesWith(pos.x, pos.y)
	abstract val shape2D: Shape2D
}
