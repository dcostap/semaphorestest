package com.dcostap.engine.map.entities

import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.JsonValue
import com.dcostap.Engine
import com.dcostap.engine.utils.JsonSavedObject
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.addChildValue

// todo finish this, polygons don't even get recognized by the collision trees
open class PolyBoundingBox(entity: Entity, name: String, vertices: FloatArray) : BoundingBox(entity, name) {
	private lateinit var internalPolygon: Polygon
	private lateinit var worldPolygon: Polygon

	private var thisWidth: Float = 0f
	private var thisHeight: Float = 0f

	constructor(entity: Entity, name: String, polygon: Polygon) : this(entity, name, polygon.transformedVertices)
	constructor(entity: Entity, name: String, vertices: Array<Pair<Float, Float>>) : this(entity, name, floatArrayOf(*vertices.flatMap { it.toList() }.toFloatArray()))

	init {
		changePolygon(vertices)
	}

	override fun collidesWith(rectangle: Rectangle): Boolean {
		return Utils.polygonOverlaps(poly, rectangle)
	}

	override fun collidesWith(circle: Circle): Boolean {
		return Utils.polygonOverlaps(poly, circle)
	}

	override fun collidesWith(polygon: Polygon): Boolean {
		return Intersector.overlapConvexPolygons(poly, polygon)
	}

	override fun collidesWith(x: Float, y: Float): Boolean {
		return poly.contains(x, y)
	}

	fun changePolygon(vertices: FloatArray) {
		var minX = Float.MAX_VALUE
		var maxX = Float.MIN_VALUE

		var minY = Float.MAX_VALUE
		var maxY = Float.MIN_VALUE

		var even = true
		vertices.forEachIndexed { i, p ->
			if (even)
				when {
					p < minX -> minX = p
					p > maxX -> maxX = p
				}
			else
				when {
					p < minY -> minY = p
					p > maxY -> maxY = p
				}

			even = !even
		}

		thisWidth = maxX - minX
		thisHeight = maxY - minY

		internalPolygon = Polygon(vertices)
		worldPolygon = Polygon(vertices.copyOf())
	}

	override val shape2D get() = poly

	/** The BB positioned in the world */
	val poly: Polygon
		get() {
			updateWorldBB()
			return worldPolygon
		}

	override var width: Float
		get() = thisWidth
		set(value) {
			throw UnsupportedOperationException("Can't modify a polygon's size")
		}

	override var height
		get() = thisHeight
		set(value) {
			throw UnsupportedOperationException("Can't modify a polygon's size")
		}

	override var offsetX
		get() = internalPolygon.x
		set(value) {
			internalPolygon.setPosition(value, internalPolygon.y)
			updateWorldBB()
			wasModified = true
		}

	override var offsetY
		get() = internalPolygon.y
		set(value) {
			internalPolygon.setPosition(internalPolygon.x, value)
			updateWorldBB()
			wasModified = true
		}

	var rotation
		get() = worldPolygon.rotation
		set(value) {
			worldPolygon.rotation = value
			updateWorldBB()
			wasModified = true
		}

	/** world coordinates, when applying [offsetX] to [Entity.x] */
	override val x get() = worldPolygon.x

	/** world coordinates, when applying [offsetY] to [Entity.y] */
	override val y get() = worldPolygon.y

	private fun updateWorldBB() {
		worldPolygon.setPosition(entity.x + internalPolygon.x, entity.y + internalPolygon.y)
		/*
		for (index in 0..worldPolygon.vertices.size step 2) {
			worldPolygon.vertices[index] = internalPolygon.vertices[index] + entity.x
			worldPolygon.vertices[index + 1] = internalPolygon.vertices[index + 1] + entity.y
		}*/
	}

	override fun save(): JsonValue {
		val json = Engine.json
		return JsonSavedObject().also {
			it.addChildValue("name", name)
//            it.addChildValue("internalRect", json.toJson(internalRect))
//            it.addChildValue("internalRectInWorld", json.toJson(internalRectInWorld))
		}
	}

	override fun load(jsonValue: JsonValue, saveVersion: String) {
		val json = Engine.json
//        internalRect.set(libgdxJson.fromJson(Rectangle::class.java, json.getString("internalRect")))
//        internalRectInWorld.set(libgdxJson.fromJson(Rectangle::class.java, json.getString("internalRectInWorld")))
	}
}
