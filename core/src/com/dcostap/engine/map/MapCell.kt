package com.dcostap.engine.map

import com.badlogic.gdx.utils.JsonValue
import com.dcostap.engine.map.entities.CollidingEntity
import com.dcostap.engine.map.entities.Entity
import com.dcostap.engine.map.map_loading.CustomProperties
import com.dcostap.engine.utils.*
import ktx.collections.*

/**
 * Created by Darius on 14/09/2017.
 */
open class MapCell(val xCell: Int, val yCell: Int, val layer: CellLayer) {
	val index get() = layer.cellIndexFromXY(xCell, yCell)
	val map get() = layer.map

	val xUnits get() = xPixels.pixelsToUnits
	val yUnits get() = yPixels.pixelsToUnits

	val xPixels get() = xCell * layer.cellSizePixels
	val yPixels get() = yCell * layer.cellSizePixels

	val cellSizePixels get() = layer.cellSizePixels

	/** Use to directly mark the cell itself as solid */
	var markedAsSolid = false
		set(value) {
			val orig = field
			field = value

			if (value != orig) {
				layer.floodFill(this)
				notify { it.changedIsSolid(this) }
			}
		}

	/**
	 * Terrain system: each cell may have a terrain id, or -1 for no terrain. Using terrains, MapCell may delete all Tiles and re-insert
	 * new ones any time, according to the terrain's behavior depending on the 8 neighbors.
	 *
	 * Note that when using terrains you got no control over newly created Tiles, and therefore its depth.
	 * To change the depth of all Tiles you need to modify [terrainDepth], keep this in mind when loading a map from json */
	var terrainId: Int = -1
		set(value) {
			val old = field
			field = value
			if (value != -1) {
				updateTerrain()
				updateNeighborTerrains()
			}

			if (old != field) notify { it.changedTerrain(this) }
		}

	/** @see terrainId */
	var terrainDepth: Int = 0
		set(value) {
			field = value
			updateTerrain()
		}

	var customProperties: CustomProperties? = null

	/** if the terrain is a [TerrainTileSet8bit], the final mapping value (represents a tileset sprite)
	 * for this cell's tile is stored here. -1 otherwise */
	var terrainTilesetMapping: Int = -1
		private set

	private fun notify(f: (ChangeListener) -> Unit) {
		f(map)
		staticEntitiesAbove.forEachWithoutIterator { if (it is ChangeListener) f(it) }
	}

	private fun updateTerrain() {
		val oldMapping = terrainTilesetMapping

		tiles = null
		val neighbors = getNeighborTerrainIds()
		terrainTilesetMapping = -1

		val terrain = getTerrain()
		tiles = terrain.getTiles(
				this, neighbors[0], neighbors[1], neighbors[2], neighbors[3], neighbors[4], neighbors[5], neighbors[6], neighbors[7]
		)

		if (terrain is TerrainTileSet8bit)
			terrainTilesetMapping = terrain.getMappingValue(
					neighbors[0], neighbors[1], neighbors[2], neighbors[3], neighbors[4], neighbors[5], neighbors[6], neighbors[7]
			)

		if (terrainTilesetMapping != oldMapping) notify { it.changedTerrainTilesetMapping(this) }

		tiles?.forEach { it.depth = terrainDepth }
	}

	private fun getNeighborTerrainIds(): IntArray {
		val array = IntArray(8)
		var i = -1
		for (y in 1 downTo -1) {
			for (x in -1..1) {
				if (x == 0 && y == 0) {
					continue
				}

				i++

				val xx: Int = this.xCell + x
				val yy: Int = this.yCell + y

				if (layer.isCellPosInsideMap(xx.toFloat(), yy.toFloat())) {
					val current = layer.cells[layer.cellIndexFromXY(xx, yy)]
					array[i] = current.terrainId
				} else {
					array[i] = -1
				}
			}
		}

		if (array[0] == terrainId && (array[1] != terrainId || array[3] != terrainId)) array[0] = -1
		if (array[2] == terrainId && (array[1] != terrainId || array[4] != terrainId)) array[2] = -1
		if (array[5] == terrainId && (array[3] != terrainId || array[6] != terrainId)) array[5] = -1
		if (array[7] == terrainId && (array[6] != terrainId || array[4] != terrainId)) array[7] = -1
		return array
	}

	private fun updateNeighborTerrains() {
		for (y in 1 downTo -1) {
			for (x in -1..1) {
				if (x == 0 && y == 0) {
					continue
				}

				val xx: Int = this.xCell + x
				val yy: Int = this.yCell + y

				if (layer.isCellPosInsideMap(xx.toFloat(), yy.toFloat())) {
					val current = layer.cells[layer.cellIndexFromXY(xx, yy)]
					if (current.terrainId != -1)
						current.updateTerrain()
				}
			}
		}
	}

	private val tmpCellArray = GdxArray<MapCell>()
	fun getNeighbors(): GdxArray<MapCell> {
		tmpCellArray.clear()
		fun check(x: Int, y: Int) {
			val cell = layer.getMapCellFromCellCoords(xCell + x, yCell + y)
			if (cell != null) tmpCellArray.add(cell)
		}

		check(1, 0); check(1, 1); check(0, 1)
		check(-1, 0); check(-1, -1); check(0, -1)
		check(1, -1); check(-1, 1)

		return tmpCellArray
	}

	// private since I see no use for this outside
	private fun isTerrainIdValidHere(terrainId: Int): Boolean {
		val neighbors = getNeighborTerrainIds()
		for (terrain in map.screen.assets.terrains[terrainId]) {
			if (!terrain.isValid(neighbors[0], neighbors[1], neighbors[2], neighbors[3], neighbors[4], neighbors[5], neighbors[6], neighbors[7])) {
				return false
			}
		}
		return true
	}

	private fun getTerrain(): Terrain {
		val neighbors = getNeighborTerrainIds()
		val terrains = map.screen.assets.terrains[terrainId]
				?: throw RuntimeException("No terrain registered for id $terrainId\nTerrains: ${map.screen.assets.terrains}")
		if (terrains.size == 0) throw RuntimeException("No terrains for id $terrainId")

		// Try to pick the terrain perfect for the neighbor situation
		for (terrain in terrains) {
			if (terrain.isValid(neighbors[0], neighbors[1], neighbors[2], neighbors[3], neighbors[4], neighbors[5], neighbors[6], neighbors[7])) {
				return terrain
			}
		}

//		logWarning("No terrain perfect for dealing with neighbors of cell: ${toString()}; cell has terrainId: $terrainId")

		// no perfect terrain, just pick first one available
		return terrains.first()
	}

	private var hasSolid = false

	/** Whether any static solid Entity occupies this cell *or* the cell is directly set as solid.
	 * May be used to speed up collision detection.
	 * @see CollidingEntity */
	val isSolid
		get() = hasSolid || markedAsSolid

	var staticEntitiesAbove = GdxArray<Entity>()
	var node = PathfindingNode()
	var debugFlashingRect: FlashingThing? = null

	fun changedStaticEntities() {
		notify { it.changedStaticEntitiesAbove(this) }
	}

	var tiles: Array<Tile>? = null

	internal fun updateHasSolid() {
		val orig = hasSolid
		hasSolid = false
		for (ent in staticEntitiesAbove) {
			if (ent.isSolid) {
				hasSolid = true
				break
			}
		}

		if (orig != hasSolid) {
			layer.floodFill(this)
			notify { it.changedIsSolid(this) }
		}
	}

	val middleX: Float get() = xUnits + (layer.cellSizePixels.pixelsToUnits / 2f)
	val middleY: Float get() = yUnits + (layer.cellSizePixels.pixelsToUnits / 2f)

	/** Different numbers means you can't path-find from one cell to another. -1 means flood fill wasn't performed, so the optimization won't be used */
	var pathfindingFloodIndex = -1L

	/** @see CellLayer.floodRegionSize */
	var floodRegion = 0

	inner class PathfindingNode {
		var g: Float = 0f
		var f: Float = 0f
		var cameFrom: MapCell? = null

		init {
			reset()
		}

		// after performing a pathfinding algorithm, reset these values
		fun reset() {
			g = -1f
			f = -1f
			cameFrom = null
		}
	}

	fun save(): JsonValue {
		val json = JsonSavedObject()
		json.addChildValue("x", xCell)
		json.addChildValue("y", yCell)

		json.addChildValue("markedAsSolid", markedAsSolid)
		json.addChildValue("hasSolid", hasSolid)
		// todo save tile depth
//        json.addChildValue("customProperties", jsonlibgdx.toJson(properties))

		json.addChildValue("tiles", JsonSavedObject().also {
			//			for (tile in tiles) {
//				it.addChild("tile", tile.save())
//			}
		})

		return json
	}

	companion object {
		@JvmStatic
		fun load(json: JsonValue, cellLayer: CellLayer): MapCell {
			val mapCell = MapCell(json.getInt("x"), json.getInt("y"), cellLayer)
			mapCell.markedAsSolid = json.getBoolean("markedAsSolid")
			mapCell.hasSolid = json.getBoolean("hasSolid")

//			mapCell.tiles.clear()
//			json.get("tiles").ifNotNull {
//				for (tile in it) {
//					when (tile.getString("class")) { // todo: check this works
//						"SprTile" -> mapCell.tiles.add(SprTile.load(tile, mapCell))
//					}
//				}
//			}

			return mapCell
		}
	}

	/** Notifies Static Entities above the cell that implement the interface */
	interface ChangeListener {
		fun changedTerrain(cell: MapCell)

		/** The value of [terrainTilesetMapping] changed, this means the sprite associated with the [TerrainTileSet8bit]
		 * changed to another sprite, or maybe the Terrain is no longer a [TerrainTileSet8bit],
		 * in which case the variable [terrainTilesetMapping] is now -1 */
		fun changedTerrainTilesetMapping(cell: MapCell)

		fun changedIsSolid(cell: MapCell)
		fun changedStaticEntitiesAbove(cell: MapCell)
	}
}
