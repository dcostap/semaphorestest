package com.dcostap.engine.map

/** Created by Darius on 23-Aug-19.
 *
 * Each terrain may be [isValid] or not depending on all neighbors of a cell.
 *
 * Keep in mind there can be multiple Terrain for each id, check Assets.
 * MapCells will first try to pick the first terrain of the array for the id that is valid.
 * Having a MapCell with an invalid terrain situation will not throw errors, in this case MapCells will default to the first available terrain.
 *
 * This can be useful if all your [TerrainTileSet8bit] that have transitions to grass, don't wanna define extra transitions for all other terrains.
 * When non-grass terrains collide, this will result in false [isValid] and MapCells will default to the first
 * Terrain, which is this one transitioning to grass.
 * As a result, each one will transition to grass in the border whenever these non-grass terrains collide.
 */
abstract class Terrain(val terrainId: Int) {
	abstract fun isValid(topLeft: Int, top: Int, topRight: Int, left: Int, right: Int,
						 bottomLeft: Int, bottom: Int, bottomRight: Int): Boolean

	abstract fun getTiles(cell: MapCell, topLeft: Int, top: Int, topRight: Int, left: Int,
						  right: Int, bottomLeft: Int, bottom: Int, bottomRight: Int): Array<Tile>?
}

class TerrainSingle(terrainId: Int, val textureName: String) : Terrain(terrainId) {
	override fun isValid(topLeft: Int, top: Int, topRight: Int, left: Int, right: Int, bottomLeft: Int, bottom: Int, bottomRight: Int): Boolean {
		return true
	}

	override fun getTiles(cell: MapCell, topLeft: Int, top: Int, topRight: Int, left: Int,
						  right: Int, bottomLeft: Int, bottom: Int, bottomRight: Int): Array<Tile>? {
		return arrayOf(SprTile(cell, textureName))
	}
}

class TerrainInvisible(terrainId: Int) : Terrain(terrainId) {
	override fun isValid(topLeft: Int, top: Int, topRight: Int, left: Int, right: Int, bottomLeft: Int, bottom: Int, bottomRight: Int): Boolean {
		return true
	}

	override fun getTiles(cell: MapCell, topLeft: Int, top: Int, topRight: Int, left: Int,
						  right: Int, bottomLeft: Int, bottom: Int, bottomRight: Int): Array<Tile>? {
		return null
	}
}

class TerrainTileSet8bit(terrainId: Int, val collidingTerrainId: Int, val textureGroupName: String) : Terrain(terrainId) {
	fun isNeighborValid(terrainId: Int): Boolean {
		return terrainId == this.terrainId || terrainId == collidingTerrainId || terrainId == -1
	}

	override fun isValid(topLeft: Int, top: Int, topRight: Int, left: Int, right: Int,
						 bottomLeft: Int, bottom: Int, bottomRight: Int): Boolean {
		return isNeighborValid(topLeft) && isNeighborValid(top) && isNeighborValid(topRight) &&
				isNeighborValid(bottomLeft) && isNeighborValid(bottom) && isNeighborValid(bottomRight) &&
				isNeighborValid(right) && isNeighborValid(left)
	}

	fun getMappingValue(topLeft: Int, top: Int, topRight: Int, left: Int, right: Int, bottomLeft: Int, bottom: Int, bottomRight: Int): Int {
		return tilesetMapping8bit(topLeft == terrainId,
				top == terrainId, topRight == terrainId, left == terrainId, right == terrainId,
				bottomLeft == terrainId, bottom == terrainId, bottomRight == terrainId)
	}

	override fun getTiles(cell: MapCell, topLeft: Int, top: Int, topRight: Int, left: Int, right: Int, bottomLeft: Int, bottom: Int, bottomRight: Int): Array<Tile>? {
		return arrayOf(SprTile(cell,
				textureGroupName + "_n" + getMappingValue(topLeft, top, topRight, left, right, bottomLeft, bottom, bottomRight)))
	}

	companion object {
		fun tilesetMapping8bit(byteValue: Int): Int {
			return when (byteValue) {
				in 0 until 2 -> 27
				in 2 until 8 -> 19
				in 8 until 10 -> 26
				in 10 until 11 -> 13
				in 11 until 16 -> 18
				in 16 until 18 -> 24
				in 18 until 22 -> 12
				in 22 until 24 -> 16
				in 24 until 26 -> 25
				in 26 until 27 -> 56
				in 27 until 30 -> 60
				in 30 until 31 -> 61
				in 31 until 64 -> 17
				in 64 until 66 -> 3
				in 66 until 72 -> 11
				in 72 until 74 -> 5
				in 74 until 75 -> 57
				in 75 until 80 -> 59
				in 80 until 82 -> 4
				in 82 until 86 -> 48
				in 86 until 88 -> 58
				in 88 until 90 -> 49
				in 90 until 91 -> 34
				in 91 until 94 -> 45
				in 94 until 95 -> 44
				in 95 until 104 -> 22
				in 104 until 106 -> 2
				in 106 until 107 -> 51
				in 107 until 120 -> 10
				in 120 until 122 -> 52
				in 122 until 123 -> 37
				in 123 until 126 -> 30
				in 126 until 127 -> 55
				in 127 until 208 -> 32
				in 208 until 210 -> 0
				in 210 until 214 -> 50
				in 214 until 216 -> 8
				in 216 until 218 -> 53
				in 218 until 219 -> 36
				in 219 until 222 -> 47
				in 222 until 223 -> 23
				in 223 until 248 -> 33
				in 248 until 250 -> 1
				in 250 until 251 -> 31
				in 251 until 254 -> 40
				in 254 until 255 -> 41
				else -> 9
			}
		}

		fun tilesetMapping8bit(topLeft: Boolean, top: Boolean, topRight: Boolean, left: Boolean, right: Boolean,
							   bottomLeft: Boolean, bottom: Boolean, bottomRight: Boolean): Int {
			return tilesetMapping8bit(
					(if (topLeft) 1 else 0) +
							(if (top) 2 else 0) +
							(if (topRight) 4 else 0) +
							(if (left) 8 else 0) +
							(if (right) 16 else 0) +
							(if (bottomLeft) 32 else 0) +
							(if (bottom) 64 else 0) +
							(if (bottomRight) 128 else 0)
			)
		}
	}
}