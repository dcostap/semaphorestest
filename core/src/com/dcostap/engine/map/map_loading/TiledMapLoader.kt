package com.dcostap.engine.map.map_loading

import com.badlogic.gdx.math.Vector2
import com.dcostap.engine.map.EntityMap
import com.dcostap.engine.map.MapCell
import com.dcostap.engine.map.entities.Entity

/**
 * Links strings with Entity creation
 */
interface TiledMapLoader {
	/**
	 * Loads Entity associated with a Tiled's TileObject (Object with image). Identified by its tile image name
	 *
	 * Return Entity if you wanna let [JsonMapInfo] modify some of its properties according to the map file loaded (position & depth)
	 *
	 * @param imageName    The name of the tile used by the Tile Object
	 */
	fun loadEntityFromTiledTileObject(imageName: String, objectName: String, position: Vector2, widthPixels: Int, heightPixels: Int,
									  depth: Int, mirrorX: Boolean, mirrorY: Boolean,
									  map: EntityMap, objectProps: CustomProperties, rotation: Float): Entity?

	/**
	 * Load Entity associated with a Tiled's Object (not a TileObject). Identified by its name
	 */
	fun loadEntityFromObjectName(objectName: String, position: Vector2, widthPixels: Int, heightPixels: Int,
								 depth: Int,
								 map: EntityMap, objectProps: CustomProperties): Entity?

	/**
	 * When loading a cell may have a tile (image in the cell).
	 *
	 * @return false to let the MapLoader do the default cell loading (insert the image in the cell searching by its name,
	 * or other behaviors depending on [JsonMapInfo.tileLoadBehavior]);
	 * @param imageName    The name of the tile inserted in the cell
	 */
	fun loadTileFromImageName(imageName: String, mapCell: MapCell, map: EntityMap, depth: Int): Boolean
}
