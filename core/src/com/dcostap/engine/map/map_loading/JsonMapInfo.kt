package com.dcostap.engine.map.map_loading

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonValue
import com.dcostap.Engine
import com.dcostap.engine.debug.logWarning
import com.dcostap.engine.map.CellLayer
import com.dcostap.engine.map.EntityMap
import com.dcostap.engine.map.SprTile
import com.dcostap.engine.map.entities.Entity
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.ifNotNull
import ktx.collections.*
import java.io.File
import kotlin.math.max
import kotlin.math.min

/**
 * Handles the loading of objects and tiles inside a map created with Tiled map editor, exported to .json files. Stores all
 * the info of a map's .json file. Can be reused with any number of [EntityMap]. To load into a map use [EntityMap.loadFromJson]
 *
 * Uses:
 *  * .json file of the map itself
 *  * .json file of each tileSet the map uses
 *  * .json file of each object template the map uses
 *
 *
 * **Loads:**
 *  * TileLayers: By default a [EntityMap] only creates one default [CellLayer], and all info from TileLayers loaded here
 *  is put into that [CellLayer] (check [EntityMap.loadFromJson]).
 *
 *  Loading may be customized inside a [TiledMapLoader]. For example, you may use a special solid texture
 *  as tile in the editor then in the TileLoader used set the cells as solid and ignore the texture. This allows for easier
 *  marking of solid cells than using snapped solid objects in the map editor.
 *
 *  * ObjectLayers: creates Entities for each Object. Can be Tiled's tile objects or shaped objects.
 * Tile objects load Entities based on its tile image name; shaped objects load Entities based on each object name.
 * See [TiledMapLoader].
 *
 * **Takes into account:**
 *  * Layer offset & visible properties: they are inherited from parent groups. In short, it will look just like in Tiled.
 *  Keep into account that when offsetting tile layers, tiles outside the map are silently ignored. Invisible layers will be ignored if specified.
 *  * mirrorX & mirrorY of TileObjects
 *
 * The map itself, the entities and the tiles all can have custom properties which are specified in Tiled
 *
 * Special custom properties:
 * - depth: can be in a ObjectLayer, TileLayer, and in a Object. Will modify the render order of tiles and entities.
 * Keep in mind Tiles may not be loaded ([TiledMapLoader] overwrites it, or a custom [TileLoadBehavior] is specified). In that case
 * depth can't be set for Tiles here since no Tiles will be created here.
 *
 * Object's depth property will overwrite the ObjectLayer's depth property if both exist.
 * By default (if no custom depth property specified) the depth is changed by a fixed amount per each layer loaded.
 * (see [layerDepthDecrease] and [layerDepthStarting])
 *
 * **Choosing what to load:**
 * * This is an important matter for persistence reasons. When loading from json everything will start in the same state, but...
 * * ... you can choose what layers will be loaded
 * * ... you can choose which entities to load or ignore with the [TiledMapLoader]
 * * Mix this with the [EntityMap.saveEntities] and other related serialization methods to control how your maps are stored and restored
 * * You can choose to load everything from json the first time. After that, retrieve from json only non-changing entities;
 * changing entities will be saved & loaded with [EntityMap.loadEntities].
 * * You can choose to load from json only the first time, then use the [EntityMap] serialization methods to keep the info.
 *
 * Notes:
 *  * While working on Tiled, base files may be different than .json (.tmx for example). You must then use "export"
 * option and export them as .json each time you edit. You may use .json base file for convenience. (When creating tileSets or
 * templates, save them as .json when first prompted)
 *  * Size of objects, depth, mirroring, etc in Tiled may or may not be ignored when loading them into Entities
 *  * Note that when using object templates, the size change in one instance of that template won't be saved, so in the game
 * the loaded size is the template's object size. To actually load that object's size, detach it from the template
 */
class JsonMapInfo(val mapName: String, val mapFolder: String, val objectTemplatesJsonFolder: String, val tileSetsJsonLocation: String) {
	private var mapData = Data()
	private var tiledMapLoader: TiledMapLoader? = null

	class JsonMapLayer(val name: String, val type: Type, val customProperties: CustomProperties) {
		enum class Type { TileLayer, ObjectLayer, GroupLayer }
	}

	private var allowLayer: (JsonMapLayer) -> Boolean = { true }
	private var loadInvisibleLayers = false

	private val loadingOffsetPixels = Vector2(0f, 0f)

	val mapWidth get() = mapData.jsonMapFile.get("width").asInt()
	val mapHeight get() = mapData.jsonMapFile.get("height").asInt()
	val mapCellSize: Int get() {
		val height = mapData.jsonMapFile.getInt("tileheight")
		val width = mapData.jsonMapFile.getInt("tilewidth")
		if (height != width)
			throw RuntimeException("Map $mapFolder/$mapName has different cell width & height. They must be equal")

		return max(height, width)
	}

	private var layer: CellLayer? = null

	enum class TileLoadBehavior {
		/** All tile layers will be completely ignored */
		IGNORE_TILES,
		/** This one will trigger [TiledMapLoader] loadTile method then return */
		LOAD_TILES_BUT_NEVER_ADD_THEM_TO_CELLS,
		APPEND_NEW_TILE_TO_EXISTING_ONES,
		/** Note that this setting will avoid all cells from having more than one Tile */
		REPLACE_EXISTING_TILES
	}

	private var tileLoadBehavior: TileLoadBehavior? = null

	/** @see EntityMap.appendFromJson */
	fun loadMap(map: EntityMap, tiledMapLoader: TiledMapLoader? = null,
				offsetXPixels: Float = 0f, offsetYPixels: Float = 0f,
				loadMapProperties: Boolean = true,
				loadInvisibleLayers: Boolean = false,
				tileLoadBehavior: TileLoadBehavior,
				cellLayer: CellLayer,
				allowLayer: (JsonMapLayer) -> Boolean = { true }) {
		this.tiledMapLoader = tiledMapLoader
		this.loadInvisibleLayers = loadInvisibleLayers
		this.allowLayer = allowLayer
		this.tileLoadBehavior = tileLoadBehavior

		loadingOffsetPixels.set(offsetXPixels, offsetYPixels)

		if (loadMapProperties)
			map.customProperties.loadFromTiledJson(mapData.jsonMapFile)

		if (cellLayer.cellSizePixels != mapCellSize)
			logWarning("Loading map json file with cell size of $mapCellSize into a map with " +
					"a destination cellLayer of cell size ${cellLayer.cellSizePixels}")

		this.layer = cellLayer
		loadLayers(mapData.jsonMapFile, map)
		this.layer = null

		map.removeAndAddEntities()
	}

	fun preLoadTemplatesAndTilesets() {
		loadLayers(mapData.jsonMapFile, null, true)
	}

	/** default depth ordering of layers starts with this value*/
	var layerDepthStarting = 0

	/** default decrease of depth for each layer processed */
	var layerDepthDecrease = 10

	/**
	 * (Note: internal yOffset used when loading is inverted)
	 *
	 * Loads the info inside all tile layers found in the .json file.
	 *
	 * Layers might be disabled, and therefore not loaded. This happens if the layer is inside a Tiled folder
	 * (called group in Tiled) with a name of format "group:name", then the layer belongs to that group.
	 * Layers can belong to more than one group.
	 *
	 * If any of those groups is disabled on map's properties (boolean with name "group:name" is false) the layer is ignored
	 */
	private fun loadLayers(jsonMapFile: JsonValue, map: EntityMap?, onlyPreload: Boolean = false) {
		// iterate through all tile layers of the map
		val layers = jsonMapFile.get("layers")

		val layer: JsonValue? = layers.child() // get first one

		fun loadLayerGroup(firstLayer: JsonValue?, offsetX: Float, offsetY: Float, depth: Int) {
			var thisDepth = depth
			var thisLayer = firstLayer

			while (thisLayer != null) {
				val thisProperties = CustomProperties().also { it.loadFromTiledJson(thisLayer!!) }
				fun isLayerAllowed(type: JsonMapLayer.Type): Boolean {
					return allowLayer(JsonMapLayer(thisLayer!!.getString("name"), type, thisProperties))
				}

				if (loadInvisibleLayers || thisLayer.getBoolean("visible", true)) {
					if (thisLayer.getString("type") == "tilelayer") {
						if (isLayerAllowed(JsonMapLayer.Type.TileLayer))
							loadTileLayer(jsonMapFile, thisLayer, map, onlyPreload, thisDepth, offsetX, offsetY)
					} else if (thisLayer.getString("type") == "objectgroup") {
						if (isLayerAllowed(JsonMapLayer.Type.ObjectLayer))
							loadObjectLayer(jsonMapFile, thisLayer, map, onlyPreload, thisDepth, offsetX, offsetY)
					} else if (thisLayer.getString("type") == "group") {
						if (isLayerAllowed(JsonMapLayer.Type.GroupLayer))
							loadLayerGroup(thisLayer.get("layers").child,
									offsetX + thisLayer.getInt("offsetx", 0),
									offsetY + thisLayer.getInt("offsety", 0),
									thisProperties.ints.get("depth", thisDepth))
					}
				}

				thisLayer = thisLayer.next()
				thisDepth -= layerDepthDecrease
			}
		}

		// we don't use Utils unitsToPixel conversion cause we don't want rounding
		loadLayerGroup(layer, loadingOffsetPixels.x, -loadingOffsetPixels.y, layerDepthStarting)
	}

	/**
	 * Loops through all cells inside the tile layer.
	 */
	private fun loadTileLayer(jsonMapFile: JsonValue, jsonLayerInfo: JsonValue, map: EntityMap?,
							  onlyPreload: Boolean = false, depth: Int,
							  parentOffsetX: Float = 0f, parentOffsetY: Float = 0f) {
		if (tileLoadBehavior == TileLoadBehavior.IGNORE_TILES) return
		val width = jsonMapFile.get("width").asInt()
		val height = jsonMapFile.get("height").asInt()

		val cellPosition = GridPoint2()

		val layerProperties = CustomProperties().also { it.loadFromTiledJson(jsonLayerInfo) }

		// if layer contains "depth" property
		val currentLayerDepth = layerProperties.ints.get("depth", depth)

		if (jsonLayerInfo.has("encoding")) {
			throw java.lang.RuntimeException("Tiled map file: $mapFolder/$mapName has encoding activated on its tile layers. \n" +
					"Change it to CSV under map settings")
		}

		// loop through all the cells of the layer
		val cells = jsonLayerInfo.get("data").asLongArray()
		for (i in cells.indices) {
			// GID of the cell = global identifier that links the cell with a texture inside a tileset
			val GID = cells[i]

			// GID = 0 means cell has no texture
			if (GID == 0L) continue

			val imageName = getTileImageNameFromGIDInsideTileSet(GID, jsonMapFile, null)

			val layer = layer
			if (!onlyPreload && map != null && layer != null) {
				cellPosition.set(i % width, height - i / width - 1)

//                val mapTileSize = Math.max(jsonMapFile.getInt("tileheight"), jsonMapFile.getInt("tilewidth"))

				cellPosition.x += MathUtils.ceil((parentOffsetX + jsonLayerInfo.getInt("offsetx", 0)) / mapCellSize)
				cellPosition.y -= MathUtils.ceil((parentOffsetY + jsonLayerInfo.getInt("offsety", 0)) / mapCellSize)

				// tiles outside map are silently ignored
				if (!layer.isCellPosInsideMap(cellPosition.x, cellPosition.y)) continue
				val mapCell = layer.cells[layer.cellIndexFromXY(cellPosition.x, cellPosition.y)]

//                mapCell.properties = Json().fromJson(CustomProperties::class.java, jsonData.getString("customProperties"))
				var doNormalLoading = true

				tiledMapLoader.ifNotNull {
					// custom loading for that tile?
					if (it.loadTileFromImageName(imageName, mapCell, map, currentLayerDepth)) doNormalLoading = false
				}

				if (doNormalLoading && tileLoadBehavior != TileLoadBehavior.LOAD_TILES_BUT_NEVER_ADD_THEM_TO_CELLS) {
					val newTile = SprTile(mapCell, imageName).also { it.depth = currentLayerDepth }
					val oldTiles = mapCell.tiles
					if (oldTiles == null || tileLoadBehavior == TileLoadBehavior.REPLACE_EXISTING_TILES) {
						mapCell.tiles = arrayOf(newTile)
					} else if (tileLoadBehavior == TileLoadBehavior.APPEND_NEW_TILE_TO_EXISTING_ONES) {
						mapCell.tiles = arrayOf(*oldTiles, newTile)
					}
				}
			}
		}
	}

	private fun loadObjectLayer(jsonMapFile: JsonValue, jsonLayerInfo: JsonValue, map: EntityMap?, onlyPreload: Boolean = false,
								depth: Int, parentOffsetX: Float = 0f, parentOffsetY: Float = 0f) {
		val layerProperties = CustomProperties().also { it.loadFromTiledJson(jsonLayerInfo) }

		// if layer contains "depth" property
		val currentLayerDepth = layerProperties.ints.get("depth", depth)

		// loop through all objects
		val objects = jsonLayerInfo.get("objects")
		var objectInfo: JsonValue? = objects.child
		while (objectInfo != null) {
			val position = Vector2(objectInfo.getInt("x").toFloat(), objectInfo.getInt("y").toFloat())

			// apply the offset
			position.x += jsonLayerInfo.getInt("offsetx", 0) + parentOffsetX
			position.y += jsonLayerInfo.getInt("offsety", 0) + parentOffsetY

			// Tiled saves object's coords as pixel units with origin on top-left, so translate it to game coords
			position.y = (jsonMapFile.getInt("height") * mapCellSize) - position.y

			val isTileObject: Boolean

			// if it's a template, the actual info will be located in that template file
			var objectActualInfo: JsonValue = objectInfo

			// used to store the template info root when the object is a template object, to be used in methods
			// to retrieve tileset info - it's in the root of the template file (and objectActualInfo is in a child of the root)
			var templateInfoRootJson: JsonValue? = null

			// find if it's a tile object -> needs to find if it has a "gid" attribute in the .json
			// if it's a template object, the attribute is in the template file, so parse that file
			if (objectInfo.get("template") == null) {
				isTileObject = objectActualInfo.get("gid") != null
			} else {
				val path = getTemplatePathFromObject(objectInfo)
				objectActualInfo = mapData.loadedTemplates.get(path)
				templateInfoRootJson = objectActualInfo
				objectActualInfo = objectActualInfo.get("object")

				isTileObject = objectActualInfo.get("gid") != null
			}

			val widthPixels = objectActualInfo.getInt("width")
			val heightPixels = objectActualInfo.getInt("height")

			if (onlyPreload) {
				if (isTileObject) {
					getTileImageNameFromGIDInsideTileSet(objectInfo.getLong("gid"), jsonMapFile, templateInfoRootJson)
				}
			} else if (map != null && tiledMapLoader != null) {
				val entity: Entity?
				val props = CustomProperties().also { it.loadFromTiledJson(objectInfo!!) }

				// if specified, get depth from the custom property of the Entity; otherwise get it from the layer
				val entDepth = if (props.ints.containsKey("depth")) {
					props.ints.get("depth")
				} else currentLayerDepth
				// is tile object?
				if (isTileObject) {
					position.scl(1f / Engine.PPU)
					entity = loadTileObject(jsonMapFile, map, position, widthPixels, heightPixels, objectActualInfo, templateInfoRootJson, props, entDepth)
				} else {
					// objects in Tiled have their origin on their top-left corner, fix that. (the origin is "correct" in tileObjects)
					position.y -= heightPixels.toFloat()

					position.scl(1f / Engine.PPU)
					entity = loadObject(map, position, widthPixels, heightPixels, objectActualInfo, props, entDepth)
				}

				entity.ifNotNull { entity ->
					entity.customProperties = props
					entity.position.set(position)
					entity.depth = entDepth
				}
			}

			objectInfo = objectInfo.next
		}
	}

	private fun getTemplatePathFromObject(objectInfo: JsonValue): String {
		var templateFile = objectInfo.getString("template")
		val tileset = File(templateFile)
		templateFile = tileset.name // using File's getName() path modifiers are removed

		// parse .json template file
		val path = objectTemplatesJsonFolder + File.separator + templateFile
		// if it's the first time this template is accessed...
		if (!mapData.loadedTemplates.containsKey(path)) {
			val value = mapData.jsonReader.parse(Gdx.files.internal(path))
			mapData.loadedTemplates.put(path, value)
		}
		return path
	}

	private fun loadTileObject(jsonMapFile: JsonValue, map: EntityMap, position: Vector2, widthPixels: Int, heightPixels: Int,
							   objectInfo: JsonValue, templateInfoRootJson: JsonValue?, objectProps: CustomProperties, depth: Int): Entity? {

		var GID = objectInfo.getLong("gid")
		var mirrorX = false
		var mirrorY = false

		// clunky-ass way Tiled stores mirrorX / Y
		if (GID >= 2147483648 + 1073741824) {
			mirrorX = true
			mirrorY = true
			GID -= 2147483648
			GID -= 1073741824
		} else if (GID >= 2147483648) {
			mirrorX = true
			GID -= 2147483648
		} else if (GID >= 1073741824) {
			mirrorY = true
			GID -= 1073741824
		}
		if (GID == 0L) throw RuntimeException("Entity with name: ${objectInfo.getString("name")} " +
				"in map named $mapName has GID with value 0 which means no Tile is associated with it, " +
				"but Entity is a Tiled Object")

		val tileImageName = getTileImageNameFromGIDInsideTileSet(GID, jsonMapFile, templateInfoRootJson)
		val objectName = objectInfo.getString("name")
		var rotation = objectInfo.getFloat("rotation")
		// mirror rotation on Y
		rotation -= min(Utils.angleDiffSigned(rotation, 0f), Utils.angleDiffSigned(rotation, 180f)) * 2
		if (rotation > 360) rotation -= 360
		if (rotation < 0) rotation += 360
		val entity = tiledMapLoader!!.loadEntityFromTiledTileObject(tileImageName, objectName, position,
				widthPixels, heightPixels, depth, mirrorX, mirrorY, map, objectProps, rotation)
		entity.ifNotNull { map.addEntity(it) }
		return entity
	}

	/**
	 * @param objectInfo Object's name as configured in Tiled and saved in .json. If saved as a template object, the
	 * name will be in the template file
	 */
	private fun loadObject(map: EntityMap, position: Vector2, widthPixels: Int, heightPixels: Int,
						   objectInfo: JsonValue, objectProps: CustomProperties, depth: Int): Entity? {
		val objectName = objectInfo.getString("name")
		val entity = tiledMapLoader!!.loadEntityFromObjectName(objectName, position, widthPixels, heightPixels,
				depth, map, objectProps)
		entity.ifNotNull { map.addEntity(it) }
		return entity
	}

	/**
	 * Returns the image name of the tile with the GID provided.
	 * Looks in the folder where tileSets are for the tileSet where that GID belongs
	 *
	 * @param objectTemplateInfoRootJson The .json info of the template file, if it's a template object.
	 * If not an object (tile), pass null
	 * @return Name of the image, without extension and path modifiers
	 */
	private fun getTileImageNameFromGIDInsideTileSet(GID: Long, jsonMapFile: JsonValue, objectTemplateInfoRootJson: JsonValue?): String {
		val jsonTileSet: JsonValue? = findTileset(GID, jsonMapFile, objectTemplateInfoRootJson)
		val tilesetFile = tilesetFileName(GID, jsonTileSet)

		// get the local tileset image ID from the Global ID (GID) of the cell
		val localTileID = GID - jsonTileSet!!.getInt("firstgid")

		return mapData.loadedTileSets.get(tilesetFile).getImageNameFromTileId(localTileID)
	}

	private fun findTileset(GID: Long, jsonMapFile: JsonValue, objectTemplateInfoRootJson: JsonValue?): JsonValue? {
		var jsonTileSet: JsonValue? = null
		// find the tileset: if it's a object from a template, the tileset is in the template file
		// if not, the tileset is in one of the tileset array in the jsonMapFile
		if (objectTemplateInfoRootJson?.get("tileset") == null) {
			val jsonTileSetsInfo = jsonMapFile.get("tilesets")

			// iterate through all the tileSets of the map
			// using the "firstGID" property of each tileset, find the tileset that belongs to the GID provided
			var entry: JsonValue? = jsonTileSetsInfo.child
			while (entry != null) {
				if (GID >= entry.getInt("firstgid")) {
					jsonTileSet = entry
				}
				entry = entry.next
			}
		} else {
			// template
			jsonTileSet = objectTemplateInfoRootJson.get("tileset")
		}

		return jsonTileSet
	}

	private fun tilesetFileName(GID: Long, jsonTileSet: JsonValue?): String {
		// get the name of the tileset file, without extension
		var tilesetFile = jsonTileSet!!.getString("source")
		val tileset = File(tilesetFile)
		val fileName = tileset.name // using File's getName() path modifiers are removed
		tilesetFile = Utils.removeExtensionFromFilename(fileName) // remove extension

		// if it's the first time this tileset is accessed, create a JsonTileSet
		if (!mapData.loadedTileSets.containsKey(tilesetFile)) {
			val tilesetLoader = JsonTileSet(mapData.buildTilesetFileName(tilesetFile), mapData.jsonReader)
			mapData.loadedTileSets.put(tilesetFile, tilesetLoader)
		}

		return tilesetFile
	}

	/**
	 * Created by Darius on 19/05/2018.
	 *
	 * Loads and stores the info of a map JSON file.
	 */
	inner class Data() {
		//todo: tilesets / templates info should be shared globally. Right now with many JsonMapInfo they will have copies of the same info
		val jsonReader: JsonReader = JsonReader()
		val mapFile = Gdx.files.internal(mapFolder + File.separator + mapName + ".json")

		init {
			if (!mapFile.exists()) {
				throw RuntimeException("Map file: ${mapFile.path()} doesn't exist")
			}
		}

		val jsonMapFile = jsonReader.parse(mapFile)

		/**
		 * TilesetLoaders are stored here, one for each tileset.
		 * They are only created when a tileset is first needed while reading the map
		 */
		val loadedTileSets: GdxMap<String, JsonTileSet> = GdxMap()

		/** @see [loadedTileSets] */
		val loadedTemplates = GdxMap<String, JsonValue>()

		fun buildTilesetFileName(tileset_name: String): String {
			return tileSetsJsonLocation + File.separator + tileset_name + ".json"
		}
	}
}
