package com.dcostap.engine.map

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.*
import com.badlogic.gdx.utils.Pool
import com.dcostap.Engine
import com.dcostap.engine.debug.log
import com.dcostap.engine.map.entities.*
import com.dcostap.engine.utils.*
import ktx.collections.*

/**
 * Created by Darius on 20/10/2017.
 *
 * Holds entities in cells, each one with configurable size with [cellSize]
 * Cells occupy the entire provided map's size, creating as many cells as necessary to fill it
 *
 * Holds dynamic and static entities as separate groups
 * - Dynamic entities _that moved_ are removed & added on each frame. Before that, method resetDynamicEntities() must be called
 * - Static entities are added only when created, until they are removed
 * - Note: Dynamic entities can move, Static entities can't!
 *
 * All coordinates not related to arrays of cells are in game units
 */
@Suppress("LibGDXUnsafeIterator")
class CollisionTree(val cellSize: Int, mapSizeX: Int, mapSizeY: Int, private val entityMap: EntityMap) {
	private val dynamicEntsThatChanged = GdxArray<Entity>()

	private val dummyTreeCellSet = GdxArray<CollisionTreeCell>()
	private val dummyEntitySet = GdxSet<Entity>()
	private val dummyEntityArray = GdxArray<Entity>()
	private val dummyTreeCellArray = GdxArray<CollisionTreeCell>()

	private val dynamicBBsPosition = GdxMap<BoundingBox, GdxArray<CollisionTreeCell>>()

	private val rectangleOrigin = GridPoint2()
	private val rectangleEnd = GridPoint2()

	private val collisionTreePool = CollisionTreePool()

	private var cellNumber = 0
	private val sizeX: Int = MathUtils.ceil(mapSizeX / cellSize.toFloat())
	private val sizeY: Int = MathUtils.ceil(mapSizeY / cellSize.toFloat())

	private val treeCells: GdxArray<GdxArray<CollisionTreeCell>>
	private val outsideCell = CollisionTreeCell(GridPoint2(-1, -1), true)

	init {
		treeCells = GdxArray(sizeX)
		for (x in 0 until sizeX) {
			val yArray = GdxArray<CollisionTreeCell>(sizeY)
			for (y in 0 until sizeY) {
				yArray.add(CollisionTreeCell(GridPoint2(x, y), false))
				cellNumber++
			}
			treeCells.add(yArray)
		}
	}

	private fun isTreeCellPositionInsideTree(x: Int, y: Int): Boolean {
		return (x >= 0 && y >= 0 && x < sizeX && y < sizeY)
	}

	private val tmpRect = Rectangle()

	/**
	 * @return A list of entities approximately ordered from closest to farthest to the edges of the bounding box
	 * It's an estimation, so that first entity in the list could be farther from the third
	 * But assures that in large search areas last entities are farthest, so if you loop through the array you start checking
	 * the (possibly) closest ones. Also being allowed to specify the maximum distance, the array is limited in size
	 *
	 * @param maxApproxDistance keep in mind this is a rough estimation to quickly discard far away Entities
	 */
	@JvmOverloads
	fun getClosestApproxEnts(boundingBox: BoundingBox, self: Entity? = null, maxApproxDistance: Float, includeDynamicEnts: Boolean = true,
							 boundingBoxName: String = "default", filter: (Entity) -> Boolean = { true }): GdxArray<Entity> {
		return when (boundingBox) {
			is RectBoundingBox, is PolyBoundingBox -> {
				if (boundingBox is PolyBoundingBox)
					tmpRect.set(boundingBox.poly.boundingRectangle)
				else if (boundingBox is RectBoundingBox) {
					tmpRect.set(boundingBox.rect)
					tmpRect.x -= maxApproxDistance
					tmpRect.width += maxApproxDistance * 2f
					tmpRect.y -= maxApproxDistance
					tmpRect.height += maxApproxDistance * 2f
				}

				getPossibleCollidingEnts(tmpRect, self, includeDynamicEnts, boundingBoxName, filter)
			}
			is CircleBoundingBox -> {
				val circle = boundingBox.circle
				tmpRect.set(circle.x - circle.radius, circle.y - circle.radius, circle.radius * 2, circle.radius * 2)
				tmpRect.x -= maxApproxDistance
				tmpRect.width += maxApproxDistance * 2f
				tmpRect.y -= maxApproxDistance
				tmpRect.height += maxApproxDistance * 2f

				getPossibleCollidingEnts(tmpRect, self, includeDynamicEnts, boundingBoxName, filter)
			}
			else -> {
				dummyEntityArray.clear()
				dummyEntityArray
			}
		}
	}

	private val tmpCircle = Circle()

	@JvmOverloads
	fun getClosestApproxEnts(x: Float, y: Float, self: Entity? = null, maxApproxDistance: Float, includeDynamicEnts: Boolean = true,
							 boundingBoxName: String = "default", filter: (Entity) -> Boolean = { true }): GdxArray<Entity> {
		tmpCircle.set(x, y, maxApproxDistance)
		return getPossibleCollidingEnts(tmpCircle, self, includeDynamicEnts, boundingBoxName, filter)
	}

	/** @return List of Entities; starts with entities closest to [boundingBox] origin */
	fun getClosestApproxEntsSorted(boundingBox: BoundingBox, self: Entity?, maxApproxDistance: Float, includeDynamicEnts: Boolean = true,
								   boundingBoxName: String = "default", filter: (Entity) -> Boolean = { true }): GdxArray<Entity> {
		val list = getClosestApproxEnts(boundingBox, self, maxApproxDistance, includeDynamicEnts, boundingBoxName, filter)
		list.sortBy { it.position.distanceBetween(boundingBox.x, boundingBox.y) }
		return list
	}

	/** @return List of Entities; starts with entities closest to [x] [y] */
	fun getClosestApproxEntsSorted(x: Float, y: Float, self: Entity?, maxApproxDistance: Float, includeDynamicEnts: Boolean = true,
								   boundingBoxName: String = "default", filter: (Entity) -> Boolean = { true }): GdxArray<Entity> {
		val list = getClosestApproxEnts(x, y, self, maxApproxDistance, includeDynamicEnts, boundingBoxName, filter)
		list.sortBy { it.position.distanceBetween(x, y) }
		return list
	}

	/** @return List of Entities; starts with entities closest to [x] [y]; provides the distance to the other Entity's origin
	 *
	 * @param forEach if it returns false, exits loop */
	fun forEachClosestApproxEntsSorted(x: Float, y: Float, self: Entity, maxApproxDistance: Float, includeDynamicEnts: Boolean = true,
									   boundingBoxName: String = "default", filter: (Entity) -> Boolean = { true },
									   forEach: (ent: Entity, distance: Float) -> Boolean) {
		val list = getClosestApproxEnts(x, y, self, maxApproxDistance, includeDynamicEnts, boundingBoxName, filter)
		list.sortBy { it.position.distanceBetween(self.position) }
		list.forEach { val dist = Utils.distanceBetween(x, y, it.x, it.y); if (!forEach(it, dist)) return }
	}

//
//	@JvmOverloads
//	fun getClosestEntsSortedByDistance(boundingBox: BoundingBox, self: Entity? = null, maxApproxDistance: Int, includeDynamicEnts: Boolean = true,
//									   boundingBoxName: String = "default", filter: (Entity) -> Boolean = { true }): GdxSet<Entity> {
//		val ents = getClosestEnts(boundingBox, self, maxApproxDistance, includeDynamicEnts, boundingBoxName, filter)
//		dummyEntityArray.clear()
//		dummyEntityArray.addAll(ents)
//		dummyEntityArray.sort { o1, o2 -> Utils.getPointDistance(o1.position) }
//	}

	/** Searches possible colliding Entities with bounding boxes with the name. Ignores entities without a bounding box with that name.
	 * Don't keep references to the returned Array, as it is reused in this object
	 *
	 * Includes Entities outside of the map (if the input rectangle is outside of the map) */
	fun getPossibleCollidingEnts(shape2D: Shape2D?, self: Entity? = null, includeDynamicEnts: Boolean = true, otherBoundingBox: String = "default",
								 filter: (Entity) -> Boolean = defaultFilter): GdxArray<Entity> {
		dummyEntityArray.clear()
		shape2D ?: return dummyEntityArray

		if (includeDynamicEnts) updateDynamicEntitiesThatChanged()
		dummyEntityArray.addAll(getEntsFromTreeCellsOccupiedBy(shape2D, self, otherBoundingBox, filter, !includeDynamicEnts, false))

		return dummyEntityArray
	}

	private val dummyRectangle = Rectangle()
	private val defaultFilter: (Entity) -> Boolean = { true }

	/** Not really exact, point is replicated with a rectangle of size 1x1 pixels; point being the origin */
	@JvmOverloads
	fun getPossibleCollidingEnts(x: Float, y: Float, self: Entity? = null, includeDynamicEnts: Boolean = true, otherBoundingBox: String = "default",
								 filter: (Entity) -> Boolean = defaultFilter): GdxArray<Entity> {
		dummyRectangle.set(x, y, 1f / Engine.PPU, 1f / Engine.PPU)
		return getPossibleCollidingEnts(dummyRectangle, self, includeDynamicEnts, otherBoundingBox, filter)
	}

	fun getPossibleCollidingEnts(boundingBox: BoundingBox?, self: Entity? = null, includeDynamicEnts: Boolean = true, otherBoundingBox: String = "default",
								 filter: (Entity) -> Boolean = defaultFilter): GdxArray<Entity> {
		return getPossibleCollidingEnts(boundingBox?.shape2D, self, includeDynamicEnts, otherBoundingBox, filter)
	}

	@JvmOverloads
	fun getCollidingEnts(boundingBox: BoundingBox?, self: Entity? = null, includeDynamicEnts: Boolean = true, otherBoundingBox: String = "default",
						 filter: (Entity) -> Boolean = defaultFilter): GdxArray<Entity> {
		val newFilter: (Entity) -> Boolean = {
			boundingBox != null && it.getBoundingBoxOrNull(otherBoundingBox)?.collidesWith(boundingBox) == true && filter(it)
		}

		return getPossibleCollidingEnts(boundingBox, self, includeDynamicEnts, otherBoundingBox, newFilter)
	}

	@JvmOverloads
	fun getCollidingEnts(shape2D: Shape2D, self: Entity? = null, includeDynamicEnts: Boolean = true, otherBoundingBox: String = "default",
						 filter: (Entity) -> Boolean = defaultFilter): GdxArray<Entity> {
		val newFilter: (Entity) -> Boolean = {
			it.getBoundingBoxOrNull(otherBoundingBox)?.collidesWith(shape2D) == true && filter(it)
		}

		return getPossibleCollidingEnts(shape2D, self, includeDynamicEnts, otherBoundingBox, newFilter)
	}

	@JvmOverloads
	fun getCollidingEnts(x: Float, y: Float, self: Entity? = null, includeDynamicEnts: Boolean = true, otherBoundingBox: String = "default",
						 filter: (Entity) -> Boolean = defaultFilter): GdxArray<Entity> {
		val newFilter: (Entity) -> Boolean = {
			it.getBoundingBoxOrNull(otherBoundingBox)?.collidesWith(x, y) == true && filter(it)
		}

		return getPossibleCollidingEnts(x, y, self, includeDynamicEnts, otherBoundingBox, newFilter)
	}

	@JvmOverloads
	fun getCollidingEnts(point: Vector2, self: Entity? = null, includeDynamicEnts: Boolean = true, otherBoundingBox: String = "default",
						 filter: (Entity) -> Boolean = defaultFilter): GdxArray<Entity> {
		return getCollidingEnts(point.x, point.y, self, includeDynamicEnts, otherBoundingBox, filter)
	}

	/** Searches Entities by specific bounding box with a name
	 * @param boundingBoxName The name of the bounding boxes to search for. Entities without a BB with this name are ignored */
	private fun getEntsFromTreeCellsOccupiedBy(shape2D: Shape2D, self: Entity? = null, boundingBoxName: String = "default",
											   filter: (Entity) -> Boolean = defaultFilter,
											   excludeDynamicEnts: Boolean, excludeStaticEntities: Boolean): GdxSet<Entity> {
		dummyEntitySet.clear()
		for (treeCell in when (shape2D) {
			is Rectangle -> getTreeCellsOverlappedByRectangle(shape2D)
			is Circle -> getTreeCellsOverlappedByCircle(shape2D)
			is Polygon -> getTreeCellsOverlappedByPolygon(shape2D)
			else -> {
				dummyTreeCellSet.clear()
				dummyTreeCellSet
			}
		}) {
			for (bb in treeCell.boundingBoxes) {
				if (bb.entity === self) continue
				if (!filter(bb.entity)) continue
				if (bb.name != boundingBoxName) continue
				if ((excludeDynamicEnts && !bb.isStatic) || (excludeStaticEntities && bb.isStatic)) continue

				dummyEntitySet.add(bb.entity)

				// try to ignore camera checks for debug purposes
				if (Engine.DEBUG_COLLISION_TREE_UPDATES && !(shape2D is Rectangle && shape2D.area() > 1000.pixelsToUnits)) {
					bb.entity.debugFlashingThing.flashColor(Color.RED, 0.3f)
				}
			}
		}
		return dummyEntitySet
	}

	fun addDynamicEntityThatChanged(entity: Entity) {
		dynamicEntsThatChanged.add(entity)
	}

	fun removeDynamicEntityThatChanged(entity: Entity) {
		dynamicEntsThatChanged.removeValue(entity, true) // rarer than adding, so remove performance is ok
	}

	fun updateDynamicEntitiesThatChanged() {
		for (entity in dynamicEntsThatChanged) {
			if (entity.isDisposed || entity.map !== entityMap) continue
			removeEntity(entity, true)
			addEntity(entity, true)
			entity.hasMoved = false
		}

		dynamicEntsThatChanged.clear()
	}

	fun removeEntity(ent: Entity, isDynamic: Boolean) {
		addOrRemoveEntity(ent, isDynamic, true)
	}

	fun addEntity(ent: Entity, isDynamic: Boolean) {
		addOrRemoveEntity(ent, isDynamic, false)
	}

	private fun addOrRemoveEntity(ent: Entity, isDynamic: Boolean, remove: Boolean) {
		dummyTreeCellArray.clear()

		if (isDynamic && remove) {
			removeDynamicEnt(ent)
			return
		}

		// find out which cells the Entity occupies
		for (boundingBox in ent.boundingBoxes.values()) {
			val cells =
					when (boundingBox) {
						is RectBoundingBox -> getTreeCellsOverlappedByRectangle(boundingBox.rect)
						is CircleBoundingBox -> getTreeCellsOverlappedByCircle(boundingBox.circle)
						is PolyBoundingBox -> getTreeCellsOverlappedByPolygon(boundingBox.poly)
						else -> {
							dummyTreeCellSet.clear()
							dummyTreeCellSet
						}
					}

			for (treeCell in cells) {
				if (remove) {
					treeCell.boundingBoxes.removeValue(boundingBox, true)
				} else {
					treeCell.boundingBoxes.add(boundingBox)
				}
			}

			// only happens on add
			if (isDynamic) {
				addDynamicEntBB(cells, boundingBox)
			}
		}
	}

	private fun removeDynamicEnt(ent: Entity) {
		for (boundingBox in ent.boundingBoxes.values()) {
			dynamicBBsPosition.get(boundingBox).ifNotNull {
				for (cell in it) {
					cell.boundingBoxes.removeValue(boundingBox, true)
				}

				collisionTreePool.free(it)
				dynamicBBsPosition.remove(boundingBox)
			}
		}
	}

	private fun addDynamicEntBB(cells: GdxArray<CollisionTreeCell>, boundingBox: BoundingBox) {
		val newArray = collisionTreePool.obtain()
		newArray.clear()
		newArray.addAll(cells)
		dynamicBBsPosition.put(boundingBox, newArray)
	}

	/**
	 * Don't keep references to the returned Array nor its contents, as they are reused in this object
	 *
	 * @param rectangle Positioned and sized in game map units
	 */
	private fun getTreeCellsOverlappedByRectangle(rectangle: Rectangle): GdxArray<CollisionTreeCell> {
		val returnedCells = dummyTreeCellSet
		returnedCells.clear()

		if (rectangle.width < 0 || rectangle.height < 0)
			log("Collision checking with a Rectangle with negative height or width. This will yield wrong results.\n" +
					"Use Rectangle's extension function 'fixNegatives()'")

		rectangleOrigin.set(rectangle.x.toInt(), rectangle.y.toInt())
		rectangleEnd.set(((rectangle.x + rectangle.width).toInt()), ((rectangle.y + rectangle.height).toInt()))

		val cell1 = getTreeCellCoordsFromMapCellCoords(rectangleOrigin)
		val cell2 = getTreeCellCoordsFromMapCellCoords(rectangleEnd)

		for (x in cell1.x..cell2.x) {
			for (y in cell1.y..cell2.y) {
				returnedCells.add(getTreeCellFromTreeCellCoords(x, y))
			}
		}

		return returnedCells
	}

	private fun getTreeCellsOverlappedByPolygon(poly: Polygon): GdxArray<CollisionTreeCell> {
		val returnedCells = dummyTreeCellSet
		returnedCells.clear()

		val rectangle = poly.boundingRectangle
		rectangleOrigin.set(rectangle.x.toInt(), rectangle.y.toInt())
		rectangleEnd.set(((rectangle.x + rectangle.width).toInt()), ((rectangle.y + rectangle.height).toInt()))

		val cell1 = getTreeCellCoordsFromMapCellCoords(rectangleOrigin)
		val cell2 = getTreeCellCoordsFromMapCellCoords(rectangleEnd)

		for (x in cell1.x..cell2.x) {
			for (y in cell1.y..cell2.y) {
				returnedCells.add(getTreeCellFromTreeCellCoords(x, y))
			}
		}

		return returnedCells
	}

	private fun getTreeCellsOverlappedByCircle(circle: Circle): GdxArray<CollisionTreeCell> {
		val returnedCells = dummyTreeCellSet
		returnedCells.clear()

		if (circle.radius < 0)
			log("Collision checking with a Circle with negative height or width. This will yield wrong results.\n")

		rectangleOrigin.set((circle.x - circle.radius).toInt(), (circle.y - circle.radius).toInt())
		rectangleEnd.set(((circle.x + circle.radius).toInt()), ((circle.y + circle.radius).toInt()))

		val cell1 = getTreeCellCoordsFromMapCellCoords(rectangleOrigin)
		val cell2 = getTreeCellCoordsFromMapCellCoords(rectangleEnd)

		for (x in cell1.x..cell2.x) {
			for (y in cell1.y..cell2.y) {
				returnedCells.add(getTreeCellFromTreeCellCoords(x, y))
			}
		}

		return returnedCells
	}

	/**
	 * Modifies input vector
	 */
	private fun getTreeCellCoordsFromMapCellCoords(mapCellCoords: GridPoint2): GridPoint2 {
		// outside coords are all transformed to -1, -1 or sizeX, sizeY (first outside coordinate on all sides)
		// this avoids unnecessary loops if looping over all range of coords
		return mapCellCoords.set(Math.min(Math.max(-1f, (mapCellCoords.x / cellSize.toFloat())), sizeX.toFloat()).toInt(),
				Math.min(Math.max(-1f, (mapCellCoords.y / cellSize.toFloat())), sizeY.toFloat()).toInt())
	}

	private fun getTreeCellFromTreeCellCoords(treeCellX: Int, treeCellY: Int): CollisionTreeCell {
		if (isTreeCellPositionInsideTree(treeCellX, treeCellY)) {
			return treeCells[treeCellX][treeCellY]
		} else {
			return outsideCell
		}
	}

	fun addStaticEntity(ent: Entity) {
		addOrRemoveEntity(ent, false, false)
	}

	fun removeStaticEntity(ent: Entity) {
		addOrRemoveEntity(ent, false, true)
	}

	fun debugDrawCellBounds(gd: GameDrawer) {
		gd.color = Color.RED
		gd.alpha = 0.3f

		for (x in 0 until sizeX) {
			for (y in 0 until sizeY) {
				gd.drawRectangle((x * cellSize).pixelsToUnits, (y * cellSize).pixelsToUnits,
						cellSize.pixelsToUnits, cellSize.pixelsToUnits,
						false, Engine.DEBUG_LINE_THICKNESS * 1.1f)
			}
		}

		gd.resetColor()
		gd.resetAlpha()
	}

	class CollisionTreeCell(val position: GridPoint2, val isOutside: Boolean) {
		val boundingBoxes = GdxArray<BoundingBox>()
	}

	private class CollisionTreePool : Pool<GdxArray<CollisionTreeCell>>(10) {
		override fun newObject(): GdxArray<CollisionTreeCell> {
			return GdxArray<CollisionTreeCell>()
		}
	}
}
