package com.dcostap.engine.utils.finite_state_machine

import com.dcostap.engine.utils.actions.ActionsUpdater
import java.lang.RuntimeException

/** Created by Darius on 24-Oct-18. */
abstract class State {
	val stateActions = ActionsUpdater()

	abstract fun onEnter()
	fun update(delta: Float) {
		stateActions.update(delta)
		onUpdate(delta)
	}
	abstract fun onUpdate(delta: Float)
	abstract fun updateStateChanges(delta: Float)
	abstract fun onExit()
	override fun toString(): String {
		return this.javaClass.simpleName
	}
}
