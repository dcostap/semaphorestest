package com.dcostap.engine.utils.finite_state_machine

import com.dcostap.engine.utils.actions.ActionsUpdater

/** Created by Darius on 19-Apr-20.
 *
 * Each State lives in a Frame. Every frame can have subStates and a superState that overrides any other execution inside the Frame.
 * (each one of these is another Frame)
 *
 * Using superStates lets you create another embedded [AIStateMachine] inside any frame, so you can layer these recursively.
 *
 * A Frame may live inside another Frame, or inside the root of [AIStateMachine]
 *
 * Call [exit] whenever state finishes and frame will take care of being deleted.
 *
 * A State may be pure or impure ([isPure])
 * - Pure States can't access information outside of their own frame. They must do some kind of work then exit().
 * They may provide information to the exterior via a lambda passed in a constructor.
 * Pure States are very reusable and can be used as subStates.
 *
 * - Impure States can access its parent Frame. These States can do some work, then exit() on its own or exit by changing parent's superState.
 * Impure States can't be added as subStates.
 * Therefore, if you are an Impure State, you are always located in parent's superState.
 *
 * Impure SubStates are useful when you wanna simplify a chain of States: First one State executes, and when it finishes this state directly
 * tells the parent what State to change to next, instead of (if it was a pure State) having to communicate to the parent what the result was
 * and let the parent take the decision. Mostly useful in nested Frames where the parent is a Frame encapsulating other group of States.
 *
 * Impure States are not reusable in different contexts, or shouldn't be reused to avoid confusion over side-effects.
 * Root States can never be Impure (parent frame is null, you are inside [AIStateMachine] directly)
 * */
abstract class AIState {
	private var _frame: AIStateMachine.StateFrame? = null
	val frame get() = _frame!!

	abstract val isPure: Boolean

	private var _parentFrame: AIStateMachine.StateFrame? = null
	open val parentFrame: AIStateMachine.StateFrame
		get() {
			if (isPure) throw RuntimeException("A pure state can't access parentFrame.")
			return _parentFrame ?: throw RuntimeException("State $this has no parent frame.")
		}

	open fun initFrame(frame: AIStateMachine.StateFrame, parentFrame: AIStateMachine.StateFrame?, shouldBePure: Boolean) {
		this._frame = frame
		this._parentFrame = parentFrame
		if (shouldBePure && !isPure) throw RuntimeException("Impure States not allowed where this one was put")
	}

//	private val fsm get() = _fsm ?: throw RuntimeException("fsm variable was still not assigned at this moment.")

	val stateActions = ActionsUpdater()

	abstract fun onEnter()
	fun update(delta: Float) {
		stateActions.update(delta)
		onUpdate(delta)
	}

	/** Note this one only runs if superState is null */
	abstract fun onUpdate(delta: Float)

	/** This one always runs, even if a superState is up or the entire frame is sleeping. Use it for global interruptions */
	open fun onForcedUpdate(delta: Float) {

	}

	abstract fun onExit()
	override fun toString(): String {
		return this.javaClass.simpleName
	}

	fun exit() {
		frame.exit()
	}
}

//abstract class PureAIState : AIState() {
//	override fun initFrame(frame: AIStateMachine.StateFrame, parentFrame: AIStateMachine.StateFrame?, shouldBePure: Boolean) {
//		this._frame = frame
//		this._parentFrame = parentFrame
//		this.shouldBePure = shouldBePure
//	}
//}

abstract class AIStateGlobal {
	lateinit var fsm: AIStateMachine
		private set

	fun init(fsm: AIStateMachine) {
		this.fsm = fsm
	}

	val stateActions = ActionsUpdater()

	//todo: sleeping in the root StateMachine stops everything. Special method in GlobalState that executes even when sleeping?
	// so you can react to interruptions even when sleeping
	fun update(delta: Float) {
		stateActions.update(delta)
		onUpdate(delta)
	}

	abstract fun onUpdate(delta: Float)
}