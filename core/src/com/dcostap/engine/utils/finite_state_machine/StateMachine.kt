package com.dcostap.engine.utils.finite_state_machine

import com.dcostap.engine.utils.ifNotNull
import ktx.collections.*

/**
 * Created by Darius on 31/08/2017.
 *
 * For AI, check [AIStateMachine]
 *
 * Custom implementation of a Finite State Machine
 */
class StateMachine<T : State> @JvmOverloads constructor(currentState: T? = null) {
	var currentState: T? = null
		set(value) {
			field?.onExit()
			field = value
			field?.onEnter()
		}

	init {
		this.currentState = currentState
	}

	/** Pauses state machine execution */
	var sleep = 0f

	var isPaused = false

	private var allowStateChanges = true

	fun update(delta: Float) {
		if (isPaused) return

		if (sleep > 0f) {
			sleep = Math.max(0f, sleep - delta)
			return
		}

		currentState.ifNotNull {
			allowStateChanges = false
			it.update(delta)
			allowStateChanges = true
			it.updateStateChanges(delta)
		}
	}

	override fun toString(): String {
		return "StateMachine: $currentState\n"
	}
}
