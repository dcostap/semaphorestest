package com.dcostap.engine.utils.finite_state_machine

import com.dcostap.engine.utils.Updatable
import com.dcostap.engine.utils.actions.ActionsUpdater
import com.dcostap.engine.utils.ifNotNull
import com.dcostap.engine.utils.ifNull
import ktx.collections.*
import kotlin.RuntimeException

abstract class EnumState<T : Enum<T>>(val enum: T) {
	internal val stateActions = ActionsUpdater()

	abstract fun enter()
	abstract fun update(delta: Float)
	abstract fun updateStateChanges(delta: Float)
	abstract fun exit(newState: T): Boolean
}

open class DSLEnumState<T : Enum<T>>(enum: T) : EnumState<T>(enum) {
	private var enterFunc: (EnumState<T>) -> Unit = {}
	fun enter(f: (EnumState<T>) -> Unit) {
		this.enterFunc = f
	}

	private class Transition<T : Enum<T>>(val f: (T) -> Unit)

	private var globalTransition: Transition<T>? = null
	private var commonTransition: Transition<T>? = null
	private var transitions = GdxMap<T, Transition<T>>()


	final override fun enter() {
		enterFunc(this)
	}

	private var update: (self: EnumState<T>, delta: Float) -> Unit = { _, _ -> }
	private var stateChangesUpdate: (self: EnumState<T>, delta: Float) -> Unit = { _, _ -> }
	fun update(f: (self: EnumState<T>, delta: Float) -> Unit) {
		this.update = f
	}

	/** Runs after [update]. Use this to run any code that may trigger state changes. Separating this code may avoid errors,
	 * where you transition to another state but current state still will need to finish executing. */
	fun updateStateChanges(f: (self: EnumState<T>, delta: Float) -> Unit) {
		this.stateChangesUpdate = f
	}

	override fun update(delta: Float) {
		update(this, delta)
	}

	override fun updateStateChanges(delta: Float) {
		stateChangesUpdate(this, delta)
	}

	/** Will only activate whenever the new state isn't explicitly defined with [exitTo]. Will allow any transition without errors */
	fun exitToAnyOther(f: (T) -> Unit = {}) {
		globalTransition = Transition(f)
	}

	/** Will activate with any transition, along with explicit transitions defined with [exitTo]. Doesn't let the transition pass without errors */
	fun onExit(f: (T) -> Unit) {
		commonTransition = Transition(f)
	}

	fun exitTo(newState: T, f: (T) -> Unit = {}) {
		transitions[newState] = Transition(f)
	}

	fun exitTo(vararg newStates: T, f: (T) -> Unit = {}) {
		for (s in newStates) transitions[s] = Transition(f)
	}

	fun canChangeTo(newState: T) = transitions.get(newState, null) != null || globalTransition != null

	override fun exit(newState: T): Boolean {
		if (canChangeTo(newState)) {
			commonTransition?.f?.invoke(newState)

			transitions.get(newState, null).ifNotNull {
				it.f(this.enum)
				return true
			}.ifNull {
				globalTransition?.ifNotNull {
					it.f(this.enum)
					return true
				}
			}
		}

		return false
	}
}

/** Created by Darius on 13-Mar-20. */
class EnumStateMachine<T : Enum<T>> private constructor() : Updatable {
	var currentState: T?
		set(value) {
			if (updatingState) throw RuntimeException("Tried to change state while current state is executing. " +
					"Put any code that might trigger a state change in EnumState.updateStateChanges() instead of update()")
			if (value == null) actualCurrentState = null
			else {
				if (states.isEmpty || !states.containsKey(value)) {
					val ktState = DSLEnumState(value)
					dslStates[value].invoke(ktState)
					actualCurrentState = ktState
				} else {
					actualCurrentState = states[value].invoke()
				}
			}
		}
		get() {
			return actualCurrentState?.enum
		}

	private var updatingState = false

	fun tryToChangeState(newState: T): Boolean {
		try {
			currentState = newState
			return true
		} catch (e: Exception) {
			return false
		}
	}

	fun canChangeTo(newState: T) = (actualCurrentState as DSLEnumState).canChangeTo(newState)

	private var actualCurrentState: EnumState<T>? = null
		set(value) {
			value ?: return
			if (field?.exit(value.enum) == false) throw RuntimeException("Transition from ${field!!.enum.name} to ${value.enum.name} was not defined")
			field = value
			beforeNewStateEnter()
			field?.enter()
		}

	private var beforeNewStateEnter: () -> Unit = {}

	/** Allows you to do something before the new state's enter() */
	fun changeTo(newState: T, beforeEnter: () -> Unit) {
		beforeNewStateEnter = beforeEnter
		currentState = newState
		beforeNewStateEnter = {}
	}

	private val dslStates = GdxMap<T, DSLEnumState<T>.() -> Unit>()
	private val states = GdxMap<T, () -> EnumState<T>>()

	fun DSL_STATE(stateEnum: T, init: DSLEnumState<T>.() -> Unit) {
		dslStates.put(stateEnum, init)
	}

	fun STATE(create: () -> EnumState<T>) {
		states.put(create().enum, create)
	}

	private var globalUpdate: (delta: Float) -> Unit = {}

	fun GLOBAL_STATE(update: (delta: Float) -> Unit) {
		globalUpdate = update
	}

	override fun update(delta: Float) {
		globalUpdate(delta)

		updatingState = true
		actualCurrentState?.update(delta)
		updatingState = false
		val old = actualCurrentState
		actualCurrentState?.stateActions?.update(delta)
		if (old === actualCurrentState)
			actualCurrentState?.updateStateChanges(delta)
	}

	companion object {
		fun <T : Enum<T>> create(initialState: T?, init: EnumStateMachine<T>.() -> Unit): EnumStateMachine<T> {
			val sm = EnumStateMachine<T>()
			sm.init()
			sm.currentState = initialState
			return sm
		}
	}
}