package com.dcostap.engine.utils.finite_state_machine

import com.dcostap.engine.utils.forEachWithoutIterator
import com.dcostap.engine.utils.ifNotNull
import com.dcostap.engine.utils.ifNull
import ktx.collections.*

/**
 * Created by Darius on 31/08/2017.
 *
 * Custom implementation of a Finite State Machine
 *
 * GlobalState contains what must be run on the background no matter what's the current state. Normally manages interruptions and the default AI
 * behavior when no state is set.
 *
 * One interruption State may override current State execution.
 *
 * Interruptions are state changes happening outside of the currently executing State.
 * They may be managed in different ways. On simple scenarios, every state may be able to exit at any time without problems.
 *
 * For simple interruptions, there is a dedicated [interruptionStateFrame] that, once finished,
 * will let the previous [currentStateFrame] continue executing as normal. (They just temporarily overwrite execution like superStates in [AIState])
 *
 * At any rate, the way you handle interruptions is up to the design and must be managed in the [AIStateGlobal].
 *
 * @see AIState
 */
class AIStateMachine(val globalState: AIStateGlobal) {
	var currentState: AIState?
		set(value) {
			currentStateFrame = if (value == null) null else StateFrame(value, true, null)
		}
		get() = currentStateFrame?.state

	private var currentStateFrame: StateFrame? = null
		private set(value) {
			field?.exit()
			field = value
			field?.enter()
		}

	private var interruptionStateFrame: StateFrame? = null
		private set(value) {
			field?.exit()
			field = value
			field?.enter()
		}

	/** List of all currently executing states, going down the StateFrame hierarchy.
	 * Includes subStates.
	 * Doesn't include GlobalState, which is always executing */
	fun currentlyExecutingStates(): GdxArray<AIState> {
		if (interruptionStateFrame != null) return interruptionStateFrame!!.executingStates
		if (currentStateFrame != null) return currentStateFrame!!.executingStates
		return gdxArrayOf()
	}

	var interruptionState: AIState?
		set(value) {
			interruptionStateFrame = if (value == null) null else StateFrame(value, true, null)
		}
		get() = interruptionStateFrame?.state

	fun init() {
		globalState.init(this)
	}

	private var firstUpdate = true

	class StateFrame(val state: AIState, private val shouldBePure: Boolean, private val parent: StateFrame?) {
		val executingStates: GdxArray<AIState>
			get() {
				if (superStateFrame != null) return superStateFrame!!.executingStates
				return GdxArray<AIState>().also {
					_subStateFrames.forEach { frame -> it.addAll(frame.executingStates) }
					it.add(state)
				}
			}

		var sleep = 0f

		private val _subStateFrames = GdxArray<StateFrame>()
		private val tmpArray = GdxArray<StateFrame>()
		val subStateFrames: GdxArray<StateFrame>
			get() {
				tmpArray.clear()
				tmpArray.addAll(_subStateFrames)
				return tmpArray
			}

		fun addSubState(state: AIState) {
			_subStateFrames.add(StateFrame(state, true, this).also { it.enter() })
		}

		var superState: AIState?
			set(value) {
				superStateFrame = if (value == null) null else StateFrame(value, false, this)
			}
			get() {
				check(!normalUpdate) { "superState is always null inside onUpdate() " }
				return superStateFrame?.state
			}

		var superStateFrame: StateFrame? = null
			private set(value) {
				field?.exit()
				field = value
				field?.enter()
			}

		var isFinished = false

		fun enter() {
			state.initFrame(this, parent, shouldBePure)
			state.onEnter()
		}

		fun exit() {
			superStateFrame?.exit()
			_subStateFrames.forEachWithoutIterator { it.exit() }
			state.onExit()
			isFinished = true
		}

		private var normalUpdate = false

		fun update(delta: Float) {
			if (sleep > 0f) {
				sleep = Math.max(0f, sleep - delta)
				state.onForcedUpdate(delta)
				return
			}

			if (superStateFrame?.isFinished == true) superStateFrame = null
			superStateFrame.ifNotNull {
				it.update(delta)
			}.ifNull {
				_subStateFrames.removeAll { it.isFinished }
				_subStateFrames.forEachWithoutIterator { it.update(delta) }
				normalUpdate = true
				state.update(delta)
				normalUpdate = false
			}

			state.onForcedUpdate(delta)
		}

		/** Exits all superStates / subStates */
		fun reset() {
			superState = null
			subStateFrames.forEachWithoutIterator { it.exit() }
			subStateFrames.clear()
		}

		override fun toString(): String {
			return "->$state      " + (if (_subStateFrames.isNotEmpty()) _subStateFrames.joinToString("\t", "\nsub: ") else "") +
					(if (superStateFrame == null) "" else "\nsuper: $superStateFrame") +
					(if (sleep > 0f) " [SLEEPING: $sleep] " else "")
		}
	}

	/** Pauses state machine execution */
	var sleep = 0f

	var isPaused = false

	fun update(delta: Float) {
		if (firstUpdate) {
			firstUpdate = false
			init()
		}
		if (isPaused) return

		if (sleep > 0f) {
			sleep = Math.max(0f, sleep - delta)
			return
		}

		if (interruptionStateFrame?.isFinished == true) interruptionStateFrame = null
		interruptionStateFrame.ifNotNull {
			it.update(delta)
		}.ifNull {
			if (currentStateFrame?.isFinished == true) {
				currentState = null
			}
			currentStateFrame?.update(delta)
		}

		globalState.update(delta)
	}

	override fun toString(): String {
		return "AI fsm ${currentStateFrame
				?: "(no state)"}\n" + (if (interruptionStateFrame != null) "interrupted by: $interruptionStateFrame" else "") +
				(if (sleep > 0f) " \n[SLEEPING: $sleep] " else "")
	}
}
