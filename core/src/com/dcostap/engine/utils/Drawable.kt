package com.dcostap.engine.utils

/**
 * Created by Darius on 15/11/2017
 */
interface Drawable {
	fun draw(gd: GameDrawer, delta: Float)
}

interface DrawableWithDepth : Drawable {
	var drawDepth: Int
}