package com.dcostap.engine.utils.ui

import com.badlogic.gdx.scenes.scene2d.Group

/** Created by Darius on 09-Jun-20. */
class ExtGroup : Group() {
	var updateFunc: (delta: Float) -> Unit = {}

	override fun act(delta: Float) {
		super.act(delta)

		updateFunc(delta)
	}
}