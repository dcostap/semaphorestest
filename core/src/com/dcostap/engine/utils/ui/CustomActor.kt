package com.dcostap.engine.utils.ui

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.dcostap.engine.Assets
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.actions.ActionsUpdater
import ktx.actors.alpha

/** Created by Darius on 14-Apr-20. */
abstract class CustomActor(val stageDrawer: GameDrawer) : Actor() {
	constructor(stage: Stage, assets: Assets) : this(GameDrawer(stage.batch, assets, stage.viewport))

	val darenActions = ActionsUpdater()

	override fun act(delta: Float) {
		super.act(delta)
		darenActions.update(delta)
		update(delta)
	}

	override fun draw(batch: Batch?, parentAlpha: Float) {
		super.draw(batch, parentAlpha)
		stageDrawer.alpha = alpha * parentAlpha
		draw(stageDrawer, parentAlpha)
		stageDrawer.reset()
	}

	abstract fun update(delta: Float)
	abstract fun draw(gd: GameDrawer, parentAlpha: Float)
}