package com.dcostap.engine.utils.ui

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.dcostap.engine.Assets
import com.dcostap.engine.utils.GameDrawer

/**
 * Created by Darius on 06/04/2018.
 */
open class DrawingWidget(stage: Stage, assets: Assets, var drawingFunction: (self: DrawingWidget) -> Unit = {}) : Table() {
	val gd = GameDrawer(stage.batch, assets, stage.viewport).also { it.customPPM = 1; it.useCustomPPM = true }

	override fun drawBackground(batch: Batch?, parentAlpha: Float, x: Float, y: Float) {
		super.drawBackground(batch, parentAlpha, x, y)
		drawingFunction(this)
		draw(batch, parentAlpha, x, y)
	}

	open fun draw(batch: Batch?, parentAlpha: Float, x: Float, y: Float) {

	}
}

abstract class DrawingTable(stage: Stage, assets: Assets) : Table() {
	val gd = GameDrawer(stage.batch, assets, stage.viewport).also { it.customPPM = 1; it.useCustomPPM = true }
}
