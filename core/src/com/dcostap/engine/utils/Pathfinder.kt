package com.dcostap.engine.utils

import com.badlogic.gdx.graphics.Color
import com.dcostap.Engine
import com.dcostap.engine.debug.log
import com.dcostap.engine.debug.logWarning
import com.dcostap.engine.map.MapCell
import com.dcostap.engine.utils.Pathfinder.OnFailure.*
import ktx.collections.*
import kotlin.math.abs

class Pathfinder {
	private val tmpArray1 = GdxArray<MapCell>()
	private val tmpArray2 = GdxArray<MapCell>()
	private val tmpArray3 = GdxArray<MapCell>()
	private val tmpArray4 = GdxArray<MapCell>()

	private val returnedArray = GdxArray<MapCell>()

	private var currentValidator: (MapCell) -> Boolean = { true }

	enum class Result {
		SUCCESS, FAILURE
	}

	private var allowDiagonals = false

	enum class OnFailure {
		RETURN_NULL,
		/** Will return a very rough approximate path, result of running the search where invalid cells
		 * are transitable but instead have a bigger weight. Path will be sliced after first invalid cell is found, so
		 * if there are multiple invalid cells in the path the result will be incomplete. */
		APPROXIMATE_CHEAP_PATH,
		/** Will do a spiral loop around the end cell, trying to pathfind to each one found valid. May be more expensive to run since
		 * it may hit a lot of consecutive invalid cells that surround the original endCell. You can chose max. number on [findPath] */
		CLOSEST_PATH
	}

	private var doingRedoOfPathAfterFailure = false

	private var ignoreInvalidCells = false

//	fun findPath(startX: Float, startY: Float, goalX: Float, goalY: Float,
//				 cellLayer: CellLayer,
//				 allowDiagonals: Boolean = false,
//				 returnedArray: GdxArray<Vector2>,
//				 isCellValid: (MapCell) -> Boolean = { true },
//				 areSolidCellsInvalid: Boolean = true,
//				 getCellWeight: (MapCell) -> Float = { 0f },
//				 onFailure: OnFailure = RETURN_NULL,
//				 maximumTries: Int = 1500,
//				 onFailureClosestPathMaxFailures: Int = 5)
//			: Pair<Result, GdxArray<Vector2>?> {
//		val result = findPath(cellLayer.getMapCellFromUnits(startX, startY)!!, cellLayer.getMapCellFromUnits(goalX, goalY),
//				allowDiagonals, this.returnedArray, isCellValid, areSolidCellsInvalid, getCellWeight, onFailure,
//				maximumTries, onFailureClosestPathMaxFailures)
//		returnedArray.clear()
//
//		result.second.ifNotNull {
//			it.forEachIndexed { index, cell ->
//				if (index == it.size - 1) // last?
//					returnedArray.add(Vector2(goalX, goalY))
//				else
//					returnedArray.add(Vector2(cell.middleX, cell.middleY))
//			}
//		}
//	}

	/**
	 * @param isCellValid Not necessary to filter solid cells here, use [areSolidCellsInvalid] for that
	 * @param areSolidCellsInvalid If solid cells are invalid, flood fill optimization can be used
	 * @param getCellWeight Additional weight added to each cell, more weight = less desirable
	 * @param onFailure Behavior whenever the full path to the [goalCell] isn't found
	 * @param maximumTries Maximum number of cells checked before exiting the pathfinding
	 * @param onFailureClosestPathMaxFailures Limits [OnFailure.CLOSEST_PATH] algorithm
	 * */
	fun findPath(start: MapCell?, goalCell: MapCell?,
				 allowDiagonals: Boolean = false,
				 returnedArray: GdxArray<MapCell> = this.returnedArray,
				 isCellValid: (MapCell) -> Boolean = { true },
				 areSolidCellsInvalid: Boolean = true,
				 getCellWeight: (MapCell) -> Float = { 0f },
				 onFailure: OnFailure = RETURN_NULL,
				 maximumTries: Int = 1500,
				 onFailureClosestPathMaxFailures: Int = 5)
			: Pair<Result, GdxArray<MapCell>?> {
		if (start == null || goalCell == null) return Pair(Result.FAILURE, null)

		this.allowDiagonals = allowDiagonals
		this.currentValidator = { isCellValid(it) && !(it.isSolid && areSolidCellsInvalid) }
		if (!currentValidator(start)) {
			TODO("What do when start is invalid?")
		}

		val affectedMapCells = tmpArray1 // to reset them after
		this.tmpArray1.clear()
		this.tmpArray2.clear()
		this.tmpArray3.clear()
		this.tmpArray4.clear()
		returnedArray.clear()

		fun redoPathWithClosestValidGoalCell(): Pair<Result, GdxArray<MapCell>?> {
			doingRedoOfPathAfterFailure = true

			clearMapCells(affectedMapCells)

			var finalResult: Pair<Result, GdxArray<MapCell>?>? = null
			var failures = 0
			Utils.spiralLoop { x, y ->
				if (x != 0 || y != 0) {
					start.layer.getMapCellFromCellCoords(x + goalCell.xCell, y + goalCell.yCell).ifNotNull {
						if (Engine.DEBUG && Engine.DEBUG_PATHFINDING)
							Engine.debugUI.drawCircleInWorld(it.middleX, it.middleY, 1f, start.map.worldViewport, Color.RED, disappearTime = 1f)
						if (currentValidator(it)) {
							val result = findPath(start, it,
									allowDiagonals, returnedArray, isCellValid, areSolidCellsInvalid,
									getCellWeight, RETURN_NULL, maximumTries, onFailureClosestPathMaxFailures)
							if (result.first == Result.SUCCESS)
								finalResult = result
							else
								failures++
						}
					}
				}

				finalResult != null || failures >= onFailureClosestPathMaxFailures
			}

			doingRedoOfPathAfterFailure = false

			return finalResult ?: Pair(Result.FAILURE, null)
		}

		if (!ignoreInvalidCells && !currentValidator(goalCell)) {
			if (onFailure == APPROXIMATE_CHEAP_PATH) {
				ignoreInvalidCells = true
				val result = findPath(start, goalCell, allowDiagonals, returnedArray,
						isCellValid, areSolidCellsInvalid, getCellWeight,
						onFailure, maximumTries, onFailureClosestPathMaxFailures)
				ignoreInvalidCells = false
				return result
			} else if (onFailure == CLOSEST_PATH && !doingRedoOfPathAfterFailure)
				return redoPathWithClosestValidGoalCell()
			else
				return Pair(Result.FAILURE, null)
		}

		if (start.map !== goalCell.map) {
			throw RuntimeException("Error: Pathfinding with cells from different maps")
		}

		if (start.layer !== goalCell.layer) {
			throw RuntimeException("Error: Pathfinding with cells from different cellLayers")
		}

		// todo: right now the flood fill optimization is not compatible with diagonals
		if (!ignoreInvalidCells && !allowDiagonals
				&& start.layer.doFloodFillForSolidCellsPathfinding
				&& areSolidCellsInvalid
				&& start.floodRegion == goalCell.floodRegion
				&& start.pathfindingFloodIndex != goalCell.pathfindingFloodIndex
				&& start.pathfindingFloodIndex != -1L && goalCell.pathfindingFloodIndex != -1L) {
			if (onFailure == APPROXIMATE_CHEAP_PATH) {
				ignoreInvalidCells = true
				val result = findPath(start, goalCell, allowDiagonals, returnedArray,
						isCellValid, areSolidCellsInvalid, getCellWeight,
						onFailure, maximumTries, onFailureClosestPathMaxFailures)
				ignoreInvalidCells = false
				return result
			} else if (onFailure == CLOSEST_PATH && !doingRedoOfPathAfterFailure)
				return redoPathWithClosestValidGoalCell()
			else
				return Pair(Result.FAILURE, null)
		}

		val openList = tmpArray2 // the list containing to-be inspected MapCells
		val closedList = tmpArray3 // the list containing inspected MapCells

		openList.add(start)
		affectedMapCells.add(start)

		// we initialize start here, other MapMapCells are initialized in MapCell code,
		// when first interacted with in getCellNeighbors
		start.node.g = 0f
		start.node.f = start.node.g + getHeuristic(start, goalCell)
		start.node.cameFrom = null

		fun constructPath(): GdxArray<MapCell> {
			var mapCell: MapCell? = goalCell

			// going through the parents (cameFrom) from the end mapCell, you get the final path
			var path = returnedArray

			while (mapCell != null) {
				path.add(mapCell)
				mapCell = mapCell.node.cameFrom

				// skip first node
				if (mapCell != null && mapCell.node.cameFrom == null) break
			}

			if (ignoreInvalidCells) {
				val oldPath = tmpArray4
				oldPath.clear()
				oldPath.addAll(path)
				path.clear()

				var startValid = currentValidator(start)

				oldPath.forEachReversed { cell ->
					// skip the rest of the path when you hit the first invalid cell
					// this might happen when ignoring invalid cells, and allows you to construct the closest path even if you can't reach the goal
					if (!currentValidator(cell)) {
						if (startValid) return@forEachReversed
					} else startValid = true
					path.add(cell)
				}

				path.reverse()
			}

			clearMapCells(affectedMapCells)

			if (Engine.DEBUG_PATHFINDING) {
				if (path.size >= 1)
					Engine.debugUI.drawLineInWorld(start.middleX, start.middleY, path.last().middleX, path.last().middleY,
							path.last().map.worldViewport, Engine.DEBUG_LINE_THICKNESS, true, disappearTime = 1f)

				var previous: MapCell? = null
				path.forEach {
					previous.ifNotNull { previous ->
						Engine.debugUI.drawLineInWorld(it.middleX, it.middleY, previous.middleX, previous.middleY,
								it.map.worldViewport, Engine.DEBUG_LINE_THICKNESS, true, disappearTime = 1f)
					}
					previous = it
				}
			}

			return path
		}

		// get the MapCell with the lowest f value
		var count = 0
		while (openList.size != 0 && count < maximumTries) {
			count++

			var lowestF = java.lang.Float.POSITIVE_INFINITY
			var current: MapCell? = null
			for (n in openList) {
				if (n.node.f < lowestF) {
					lowestF = n.node.f
					current = n
				}
			}

			check(current != null) { "Pathfinding fatal error: current is null" }

			// if goalCell is found
			if (current === goalCell) {
				return Pair(if (ignoreInvalidCells) Result.FAILURE else Result.SUCCESS, constructPath())
			}

			openList.removeValue(current, true)
			closedList.add(current)

			// get all neighbor MapCells of current MapCell
			// this is done with the method of the class MapCell
			// and it's there where invalid MapCells get ignored (solids / walls through diagonals...)
			val neigh = getCellNeighbors(current, goalCell, ignoreInvalidCells)

			// if getCellNeighbors found the goal, return the path without checking for other neighbors
			if (neigh.size == 1 && neigh.get(0) === goalCell) {
				if (count >= Engine.PATHFINDING_SEARCH_COUNT_WARNING_THRESHOLD)
					log("Pathfinding finished with a search of $count cells")

				for (n in neigh) {
					affectedMapCells.add(n)
				}

				neigh.get(0).node.cameFrom = current
				return Pair(if (ignoreInvalidCells) Result.FAILURE else Result.SUCCESS, constructPath())
			}

			for (neighbor in neigh) {
				affectedMapCells.add(neighbor)

				// if it's inside a closed list, ignore the MapCell
				if (!closedList.contains(neighbor, true)) {

					// if it wasn't in an open list already, add it now
					if (!openList.contains(neighbor, true)) {
						openList.add(neighbor)
					}

					// get the g value of the neighbor MapCell from the current
					// you either do this with a MapCell that is in the open list (so that it already has a value)
					// in that case you compare the values and if this path is better, update that MapCell with the
					// new value and new parent (cameFrom)
					// if you do this with a MapCell that wasn't in the open list, you do the same, but since the MapCell
					// has the default g value (infinite), the new g is always updated as better
					var g2: Float =
							if (neighbor.xCell != current.xCell && neighbor.yCell != current.yCell) {
								current.node.g + 14.14f // diagonal cost
							} else {
								current.node.g + 10 // not diagonal
							}

					// when ignoring invalid cells, invalid cells have extra cost
					if (ignoreInvalidCells && !currentValidator(current)) g2 *= 2f

					g2 += getCellWeight(current)

					if (g2 < neighbor.node.g) { // path is better
						neighbor.node.cameFrom = current
						neighbor.node.g = g2
						neighbor.node.f = neighbor.node.g + getHeuristic(neighbor, goalCell)
					}
				}
			}
		}

		clearMapCells(affectedMapCells)
		logWarning("SEARCH OF PATH FAILED! count is $count")
		if (ignoreInvalidCells) { // only happens inside recursive call
			return Pair(Result.FAILURE, null)
		}

		// pathfinding failed, try again ignoring invalid cells to get the closest path before hitting invalid cells
		if (onFailure == APPROXIMATE_CHEAP_PATH) {
			ignoreInvalidCells = true
			val result = findPath(start, goalCell)
			ignoreInvalidCells = false
			return result
		} else if (onFailure == CLOSEST_PATH && !doingRedoOfPathAfterFailure)
			return redoPathWithClosestValidGoalCell()
		else
			return Pair(Result.FAILURE, null)
	}


	private val tmpNeighArray = GdxArray<MapCell>()

	private fun getCellNeighbors(cell: MapCell, goalCell: MapCell?, ignoreInvalidCells: Boolean): GdxArray<MapCell> {
		tmpNeighArray.clear()
		val neighbors = tmpNeighArray

		for (x in -1..1) {
			for (y in -1..1) {
				if (x == 0 && y == 0)
					continue

				val xx: Int = cell.xCell + x
				val yy: Int = cell.yCell + y

				cell.layer.getMapCellFromCellCoords(xx, yy).ifNotNull { current ->

					//                    // is current the goal node? return a list with only that node
//                    if (current === goalCell) {
//                        val l = Array<MapCell>()
//                        l.add(current)
//                        return l
//                    }

					if (currentValidator(current) || ignoreInvalidCells) {
						var valid = true

						// find diagonal neighbors and discard invalid ones
						if (xx != cell.xCell && yy != cell.yCell) {
							if (!allowDiagonals) {
								valid = false
							} else {
								fun checkCell(xAdd: Int, yAdd: Int) {
									val cell = cell.layer.getMapCellFromCellCoords(xx + xAdd, yy + yAdd)
									if (cell == null || !currentValidator(cell)) valid = false
								}
								// check if the diagonal block is not surrounded by 1 or 2 solids
								if (xx < cell.xCell && yy > cell.yCell) {
									checkCell(1, 0)
									checkCell(0, -1)
								} else if (xx > cell.xCell && yy > cell.yCell) {
									checkCell(-1, 0)
									checkCell(0, -1)
								} else if (xx < cell.xCell && yy < cell.yCell) {
									checkCell(1, 0)
									checkCell(0, 1)
								} else if (xx > cell.xCell && yy < cell.yCell) {
									checkCell(-1, 0)
									checkCell(0, 1)
								}
							}
						}

						if (valid) {
							neighbors.add(current)

							// put the values really high if they were not initialized yet
							if (current.node.f == -1f) {
								current.node.f = Float.POSITIVE_INFINITY
								current.node.g = Float.POSITIVE_INFINITY
							}
						}
					}
				}
			}
		}

		return neighbors
	}

	private fun getHeuristic(start: MapCell, finish: MapCell): Float {
		// more expensive than manhattan
		// but this takes into account diagonal movement
		val xDistance = abs(start.xCell - finish.xCell).toFloat()
		val yDistance = abs(start.yCell - finish.yCell).toFloat()
		return if (xDistance > yDistance) {
			14 * yDistance + 10 * (xDistance - yDistance)
		} else {
			14 * xDistance + 10 * (yDistance - xDistance)
		}
	}

	private fun clearMapCells(cells: GdxArray<MapCell>) {
		for (mapCell in cells) {
			// flash affected cells
			if (Engine.DEBUG_PATHFINDING) {
				mapCell.debugFlashingRect.ifNull {
					mapCell.debugFlashingRect = FlashingThing(Color(
							map(mapCell.node.g, 0f, 100f, 0f, 1f),
							map(mapCell.node.g, 0f, 100f, 1f, 0f),
							0f, 1f
					), 0.5f)
				}
			}

			mapCell.node.reset()
		}
	}

	// returns 1 if not surrounded by solids / firstCollisionEntity is there
	// returns 0 if surrounded by solids but goal is walkable
	// returns -1 if surrounded and goal isn't walkable
//    private fun checkIfSurroundedBySolids(firstCollisionEntity: Entity?, startCell: MapCell, finishCell: MapCell): Int {
//        for (x in -1..1) {
//            for (y in -1..1) {
//                if (x == 0 && y == 0)
//                    continue
//
//                if (firstCollisionEntity!!.map.isInsideMap((startCell.x + x).toFloat(), (startCell.y + y).toFloat())) {
//                    val checkingCell = firstCollisionEntity.map.mapCells[startCell.x + x][startCell.y + y]
//                    if (checkingCell.isWalkable || checkingCell.tiledEntity === firstCollisionEntity) {
//                        return 1
//                    }
//                }
//            }
//        }
//
//        return if (finishCell.isWalkable) {
//            0
//        } else {
//            -1
//        }
//
//    }
}
