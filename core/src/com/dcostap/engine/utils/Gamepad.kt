package com.dcostap.engine.utils

import com.badlogic.gdx.controllers.Controller
import com.badlogic.gdx.controllers.ControllerAdapter
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Disposable
import com.dcostap.engine.debug.log
import com.dcostap.engine.debug.logWarning
import ktx.collections.*
import org.libsdl.SDL
import uk.co.electronstudio.sdl2gdx.RumbleController
import uk.co.electronstudio.sdl2gdx.SDL2ControllerManager
import kotlin.math.absoluteValue

/** Created by Darius on 12-Apr-20. */
class Gamepad(var deadZone: Float = 0.15f) : ControllerAdapter(), Disposable {
	private val sdl2 = SDL2ControllerManager()
	val slots = GdxMap<Int, Slot>()

	class Slot(var controllerIndex: Int, var controller: Controller?) {
		val buttons = GdxMap<Button, ButtonState>()
		val isControllerDisconnected get() = controller == null
		val rightStick = Vector2()
		val leftStick = Vector2()
		var r2 = 0f
		var l2 = 0f

		init {
			for (buttonEnum in Button.values()) {
				buttons.put(buttonEnum, ButtonState())
			}
		}
	}

	private var lastControllerIndex = -1

	fun getSlotForController(controller: Controller?) =
			slots.values().firstOrNull { it.controller == controller }
					?: throw RuntimeException("No slot for this controller! $controller")

	class ButtonState {
		var isJustPressed = false
		var isJustReleased = false
		var isPressed = false
	}

	init {
		sdl2.addListenerAndRunForConnectedControllers(this)
	}

	var onAnyInputDetected: () -> Unit = {}
	private var wasInputDetected = false

	fun update() {
		if (wasInputDetected) onAnyInputDetected()
		wasInputDetected = false
		slots.values().forEach {
			if (!it.isControllerDisconnected) {
				for (buttonEnum in Button.values()) {
					val button = it.buttons[buttonEnum]
					button.apply {
						isJustReleased = false
						isJustPressed = false
					}

					val pressed = it.controller!!.getButton(buttonEnum.code)
					if (pressed) {
						button.isJustPressed = !button.isPressed
						button.isPressed = true
						wasInputDetected = true
					} else {
						button.isJustReleased = button.isPressed
						button.isPressed = false
					}
				}

				it.r2 = it.controller!!.getAxis(SDL.SDL_CONTROLLER_AXIS_TRIGGERRIGHT)
//						.map(0f, SDL.SDL_JOYSTICK_AXIS_MAX(), 0f, 1f)
				it.l2 = it.controller!!.getAxis(SDL.SDL_CONTROLLER_AXIS_TRIGGERLEFT)
//						.map(0f, SDL.SDL_JOYSTICK_AXIS_MAX(), 0f, 1f)

				it.rightStick.x = it.controller!!.getAxis(SDL.SDL_CONTROLLER_AXIS_RIGHTX)
//						.map(SDL.SDL_JOYSTICK_AXIS_MIN(), SDL.SDL_JOYSTICK_AXIS_MAX(), -1f, 1f)
				it.rightStick.y = it.controller!!.getAxis(SDL.SDL_CONTROLLER_AXIS_RIGHTY) * -1f
//						.map(SDL.SDL_JOYSTICK_AXIS_MIN(), SDL.SDL_JOYSTICK_AXIS_MAX(), -1f, 1f)
				it.leftStick.x = it.controller!!.getAxis(SDL.SDL_CONTROLLER_AXIS_LEFTX)
//						.map(SDL.SDL_JOYSTICK_AXIS_MIN(), SDL.SDL_JOYSTICK_AXIS_MAX(), -1f, 1f)
				it.leftStick.y = it.controller!!.getAxis(SDL.SDL_CONTROLLER_AXIS_LEFTY) * -1f
//						.map(SDL.SDL_JOYSTICK_AXIS_MIN(), SDL.SDL_JOYSTICK_AXIS_MAX(), -1f, 1f)

				if (it.r2 > 0f || it.l2 > 0f || it.rightStick.len() >= deadZone || it.leftStick.len() >= deadZone)
					wasInputDetected = true
			}
		}
	}

	fun vibrate(leftMag: Float, rightMag: Float, duration_ms: Int, controllerSlot: Int = 0) {
		slots.get(controllerSlot, null)?.controller?.let {
			(it as RumbleController).rumble(leftMag, rightMag, duration_ms)
		}
	}

	private val fallbackOffButton = ButtonState()

	fun getButton(button: Button, controllerSlot: Int = 0) = getButtonOrNull(button, controllerSlot) ?: fallbackOffButton

	fun getButtonOrNull(button: Button, controllerSlot: Int = 0): ButtonState?
			= slots.get(controllerSlot, null)?.buttons?.get(button)

	private val tmpVec = Vector2()
	private val emptyVector = Vector2()

	fun getRightJoystickOrNull(controllerSlot: Int = 0, precisionDecimals: Int = 4): Vector2? {
		val result = getRightJoystickRaw(controllerSlot, precisionDecimals)
		result ?: return result
		if (result.len() < deadZone) result.set(0f, 0f)
		return result
	}

	fun getRightJoystick(controllerSlot: Int = 0, precisionDecimals: Int = 4): Vector2 {
		emptyVector.setZero()
		return getRightJoystickOrNull(controllerSlot, precisionDecimals) ?: emptyVector
	}

	/** Without deadzone adjustment */
	fun getRightJoystickRaw(controllerSlot: Int = 0, precisionDecimals: Int = 4): Vector2? {
		if (!slots.containsKey(controllerSlot) || slots[controllerSlot].isControllerDisconnected) return null

		tmpVec.set(Utils.roundFloat(slots[controllerSlot].rightStick.x, precisionDecimals),
				Utils.roundFloat(slots[controllerSlot].rightStick.y, precisionDecimals))
		return tmpVec
	}

	fun getLeftJoystickOrNull(controllerSlot: Int = 0, precisionDecimals: Int = 4): Vector2? {
		val result = getLeftJoystickRaw(controllerSlot, precisionDecimals)
		result ?: return result
		if (result.len() < deadZone) result.set(0f, 0f)
		return result
	}

	fun getLeftJoystick(controllerSlot: Int = 0, precisionDecimals: Int = 4): Vector2 {
		emptyVector.setZero()
		return getLeftJoystickOrNull(controllerSlot, precisionDecimals) ?: emptyVector
	}

	/** Without deadzone adjustment */
	fun getLeftJoystickRaw(controllerSlot: Int = 0, precisionDecimals: Int = 4): Vector2? {
		if (!slots.containsKey(controllerSlot) || slots[controllerSlot].isControllerDisconnected) return null

		tmpVec.set(
				Utils.roundFloat(slots[controllerSlot].leftStick.x, precisionDecimals),
				Utils.roundFloat(slots[controllerSlot].leftStick.y, precisionDecimals))
		return tmpVec
	}

	fun getR2(controllerSlot: Int = 0): Float? {
		if (!slots.containsKey(controllerSlot) || slots[controllerSlot].isControllerDisconnected) return null
		return slots[controllerSlot].r2
	}

	fun getL2(controllerSlot: Int = 0): Float? {
		if (!slots.containsKey(controllerSlot) || slots[controllerSlot].isControllerDisconnected) return null
		return slots[controllerSlot].l2
	}

	enum class Button(val code: Int) {
		A(SDL.SDL_CONTROLLER_BUTTON_A),
		B(SDL.SDL_CONTROLLER_BUTTON_B),
		X(SDL.SDL_CONTROLLER_BUTTON_X),
		Y(SDL.SDL_CONTROLLER_BUTTON_Y),
		SELECT(SDL.SDL_CONTROLLER_BUTTON_BACK),
		START(SDL.SDL_CONTROLLER_BUTTON_START),
		L1(SDL.SDL_CONTROLLER_BUTTON_LEFTSHOULDER),
		R1(SDL.SDL_CONTROLLER_BUTTON_RIGHTSHOULDER),
		LEFT_STICK(SDL.SDL_CONTROLLER_BUTTON_LEFTSTICK),
		RIGHT_STICK(SDL.SDL_CONTROLLER_BUTTON_RIGHTSTICK),
		DPAD_UP(SDL.SDL_CONTROLLER_BUTTON_DPAD_UP),
		DPAD_DOWN(SDL.SDL_CONTROLLER_BUTTON_DPAD_DOWN),
		DPAD_LEFT(SDL.SDL_CONTROLLER_BUTTON_DPAD_LEFT),
		DPAD_RIGHT(SDL.SDL_CONTROLLER_BUTTON_DPAD_RIGHT),
	}

	override fun connected(controller: Controller?) {
		slots.values().firstOrNull { it.isControllerDisconnected }.ifNotNull {
			log("Controller ${it.controllerIndex} reconnected")
			it.controller = controller
		}.ifNull {
			lastControllerIndex++
			slots.put(lastControllerIndex, Slot(lastControllerIndex, controller))
		}
	}

	fun isAnyControllerConnected() = slots.any { !it.value.isControllerDisconnected }

	override fun disconnected(controller: Controller?) {
		controller ?: return

		slots.values().firstOrNull { it.controller == controller }.ifNotNull {
			it.controller = null
			logWarning("Controller ${it.controllerIndex} disconnected")
		}.ifNull {
			logWarning("Disconnected controller $controller that wasn't registered in Gamepad.kt")
		}
	}

	override fun dispose() {
		sdl2.close()
	}
}
