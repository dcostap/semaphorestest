package com.dcostap.engine.utils

import com.badlogic.gdx.graphics.Color
import com.dcostap.engine.Assets

/** Created by Darius on 26-Aug-18. */
interface Light : Drawable

/** @param width if -1f, it will use the sprite's original width
 * @param height see [width] */
class BasicLight(var sprName: String, var x: Float = 0f, var y: Float = 0f,
				 var width: Float = -1f, var height: Float = -1f, var color: Color = Color.WHITE,
				 val updateFunc: (self: BasicLight, delta: Float) -> Unit = { self, delta -> }) : Light {
	/** Allows for random change of size and position. Width and height of light will be the same */
	var isDynamic = false

	var dynamicNewChangeTimer = 0.3f

	var dynamicMaxSizeChange = 0.2f
	var dynamicSizeChangePerSecond = 3f

	private val timer = Timer().also {
		it.elapsed = Float.MAX_VALUE
	}

	private var randomChangeSizeGoal = 0f

	var dynamicMaxPositionChange = 0.5f
	var dynamicPositionChangePerSecond = 3f

	private var randomXChange = 0f
	private var randomYChange = 0f

	private var xAdd = 0f
	private var yAdd = 0f

	private var sizeChange = 0f

	private fun update(delta: Float) {
		if (!isDynamic) return

		timer.timeLimit = dynamicNewChangeTimer

		// calculate new random changes
		if (timer.tick(delta)) {
			randomChangeSizeGoal = randomFloat(0f, dynamicMaxSizeChange) * randomSign()

			randomXChange = randomFloat(0f, dynamicMaxPositionChange) * randomSign()
			randomYChange = randomFloat(0f, dynamicMaxPositionChange) * randomSign()
		}

		var sizeChange = Math.max(width, height)

		if (sizeChange < randomChangeSizeGoal) {
			sizeChange += dynamicSizeChangePerSecond * delta
		}
		if (sizeChange > randomChangeSizeGoal) {
			sizeChange -= dynamicSizeChangePerSecond * delta
		}

		// position changing
		if (xAdd < randomXChange) {
			xAdd += dynamicPositionChangePerSecond * delta
		}
		if (xAdd > randomXChange) {
			xAdd -= dynamicPositionChangePerSecond * delta
		}
		if (yAdd < randomYChange) {
			yAdd += dynamicPositionChangePerSecond * delta
		}
		if (yAdd > randomYChange) {
			yAdd -= dynamicPositionChangePerSecond * delta
		}
	}

	override fun draw(gd: GameDrawer, delta: Float) {
		updateFunc(this, delta)
		update(delta)

		gd.color = color
		gd.drawCentered(sprName, x + xAdd, y + yAdd,
				customWidth = (if (width == -1f) gd.assets.getRegion(sprName).regionWidth.toFloat() else width) + sizeChange,
				customHeight = (if (height == -1f) gd.assets.getRegion(sprName).regionHeight.toFloat() else height) + sizeChange)
		gd.resetColorAndAlpha()
	}
}