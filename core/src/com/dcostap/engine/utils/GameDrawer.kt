package com.dcostap.engine.utils

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.NinePatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.*
import com.badlogic.gdx.scenes.scene2d.utils.Drawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.Engine
import com.dcostap.engine.Assets
import space.earlygrey.shapedrawer.ShapeDrawer


/**
 * Created by Darius on 14/09/2017.
 *
 * Helper class to draw stuff in a specific viewport. By default it adapts textures drawn to the global [Engine.PPU].
 *
 * Use it with a **scaling viewport** with arbitrary units (so 1 unit = PPM pixels):
 * * Automatically scales all textures to the PPM, so when drawing 1 pixel isn't 1 unit
 *
 * Note: you shouldn't draw fonts in scaling viewports, do it in another viewport
 *
 * Use it with a **ScreenViewport** that adjusts to different resolutions using DensityFactor
 * (so higher resolution != smaller images, and so physical size of drawn things stays the same):
 *
 * * Use drawScaled to draw images that scale to comply with DensityFactor, based on an initial size
 * * Draw normally with a font, if that font is generated in Assets with size based on DensityFactor
 *
 * If you use **Scene2d**, adapt to density factor this way:
 *
 * * use stage's actor (ExtRegionImage) to draw images, will work the same way as using drawScaled
 * * with 9patches (button / pane / widget graphics), create them with a base size multiplied by Engine.getDensityFactor
 * * use fonts the same way as before :D
 *
 * * If you don't want it to scale textures based on the PPM (scaling viewport with no arbitrary units),
 * change the global [Engine.PPU] to 1
 */
class GameDrawer(var batch: Batch, val assets: Assets, val viewport: Viewport) {
	var alpha = 1f
		set(value) {
			field = value
		}

	/** won't modify alpha */
	var color = Color(1f, 1f, 1f, 1f)
		set(value) {
			setColor(value.r, value.g, value.b)
		}

	/** won't modify alpha */
	fun setColor(r: Float, g: Float, b: Float) {
		color.r = r
		color.g = g
		color.b = b
	}

	fun setColor255(r: Int, g: Int, b: Int) {
		color.r = r / 255f
		color.g = g / 255f
		color.b = b / 255f
	}

	/** modifies alpha too */
	fun setColorAndAlpha(r: Float, g: Float, b: Float, a: Float) {
		color.r = r
		color.g = g
		color.b = b
		alpha = a
	}

	fun resetColor() {
		this.color.set(1f, 1f, 1f, 1f)
	}

	fun resetAlpha() {
		this.alpha = 1f
	}

	fun resetColorAndAlpha() {
		this.resetAlpha()
		this.resetColor()
	}

	private fun updateDrawingColorAndAlpha(color: Color) {
		batch.setColor(color.r, color.g, color.b, alpha)
	}

	private fun updateDrawingColorAndAlphaShapeDrawer(color: Color) {
		shapeDrawer!!.setColor(color.r, color.g, color.b, alpha)
	}


	private var dummyVector2 = Vector2()

	val pixelTexture get() = Engine.whitePixel
	val pixelCircle get() = Engine.whiteCircle
	var drawingOffset = Vector2(0f, 0f)

	fun setDrawingOffsetPixels(x: Float, y: Float) {
		drawingOffset.set(pixelsToUnits(x), pixelsToUnits(y))
	}

	fun setOriginPixels(x: Float, y: Float) {
		origin.set(pixelsToUnits(x), pixelsToUnits(y))
	}

	fun setDrawingOffsetPixelsXY(xy: Float) {
		drawingOffset.set(pixelsToUnits(xy), pixelsToUnits(xy))
	}

	fun resetDrawingOffset() {
		drawingOffset.set(0f, 0f)
	}

	/** Local scaleX, used when no scaleX is specified on the draw methods */
	var scaleX = 1f

	/** Local scaleY, used when no scaleY is specified on the draw methods */
	var scaleY = 1f

	fun setScaleXY(value: Float) {
		scaleX = value
		scaleY = value
	}

	val origin = Vector2()

	/** Local rotation, used when no rotation is specified on the draw methods */
	var rotation = 0f

	/** Resets local scale and rotation to the default values */
	fun resetModifiers() {
		scaleX = 1f; scaleY = 1f
		rotation = 0f
		origin.set(0f, 0f)
		drawingOffset.setZero()
	}

	fun reset() {
		resetColorAndAlpha()
		resetModifiers()
	}

	private fun pixelSizeScreen() = (1f / Engine.PPU) / ((viewport.screenWidth / viewport.worldWidth) / (Engine.PPU)) * (viewport.camera as OrthographicCamera).zoom

	private fun getFinalDrawingX(x: Float): Float {
		val pix = pixelSizeScreen()
		return MathUtils.round((x + drawingOffset.x) / pix) * pix
	}

	private fun getFinalDrawingY(y: Float): Float {
		val pix = pixelSizeScreen()
		return MathUtils.round((y + drawingOffset.y) / pix) * pix
	}

	var useCustomPPM = false
	var customPPM = 16

	/** the actual PPM value used by this GameDrawer. Will be [Engine.PPU] if [useCustomPPM] is false */
	val usedPPM get() = if (useCustomPPM) customPPM else Engine.PPU

	private fun pixelsToUnits(pixels: Number): Float = pixels.toFloat() / usedPPM
	private fun unitsToPixels(units: Number): Float = units.toFloat() * usedPPM

	private fun pixelsToUnits(pixels: Int): Float = pixels / usedPPM.toFloat()

	/** returns the amount the texture needs to be scaled to be drawn according to the Pixels Per Meter (PPM) constant **/
	fun getUnitWidth(textureRegion: TextureRegion) = pixelsToUnits(textureRegion.regionWidth)

	fun getUnitHeight(textureRegion: TextureRegion) = pixelsToUnits(textureRegion.regionHeight)

	fun getUnitWidth(texture: Texture) = pixelsToUnits(texture.width)
	fun getUnitHeight(texture: Texture) = pixelsToUnits(texture.height)

	/**
	 * @param rotation in degrees.
	 *
	 * @param mirrorX flips the textureRegion, alternative to negative [scaleX].
	 * @param mirrorY flips the textureRegion, alternative to negative [scaleY].
	 *
	 * @param displaceX displaces the texture by its entire width. If 1 to the right, if -1 to the left, 0 disables it.
	 * Won't reset any previous displacement.
	 *
	 * @param displaceY see [displaceX]
	 *
	 * @param centerOriginOnXAxis sets the origin to the center of the texture.
	 * Note this only affects transformations (Scaling & rotation happen around the origin).
	 *
	 * @param centerOriginOnYAxis see [centerOriginOnXAxis]
	 *
	 * @param centerOnXAxis offsets the drawing by half of its width so the origin of the position X is in the middle of the texture, thus
	 * the texture is being drawn centered. Note that if true the previous displacement is temporarily reset
	 * @param centerOnYAxis see [centerOnXAxis]
	 */
	@JvmOverloads
	fun draw(textureRegion: TextureRegion, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
			 rotation: Float = this.rotation,
			 originX: Float = this.origin.x, originY: Float = this.origin.y,
			 mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
			 customWidth: Float = -1f, customHeight: Float = -1f,
			 centerOnXAxis: Boolean = false, centerOnYAxis: Boolean = false,
			 centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false,
			 srcX: Int = -1, srcY: Int = -1, srcWidth: Int = -1, srcHeight: Int = -1, color: Color = this.color) {
		updateDrawingColorAndAlpha(color)
		textureRegion.flip(mirrorX, mirrorY)
		val width = if (customWidth != -1f) customWidth else getUnitWidth(textureRegion)
		val height = if (customHeight != -1f) customHeight else getUnitHeight(textureRegion)

		val previousOffset = dummyVector2
		previousOffset.set(drawingOffset)

//        if (centerOnXAxis || centerOnYAxis) resetDrawingOffset()

		if (displaceX != 0 || displaceY != 0) {
			if (displaceX != 0) drawingOffset.x += (width * displaceX)
			if (displaceY != 0) drawingOffset.y += (height * displaceY)
		}

		if (centerOnXAxis) drawingOffset.x += -width / 2f
		if (centerOnYAxis) drawingOffset.y += -height / 2f

		val thisX = getFinalDrawingX(x)
		val thisY = getFinalDrawingY(y)

//        worldViewport.ifNotNull { Engine.debugUI.drawDebugTextInWorldPosition("x: $thisX; y: $thisY", x, y, it) }

		if (srcX <= 0 && srcY <= 0 && srcHeight <= 0 && srcWidth <= 0) {
			batch.draw(textureRegion, thisX, thisY, if (!centerOriginOnXAxis) originX else (width / 2f),
					if (!centerOriginOnYAxis) originY else (height / 2f), width, height, scaleX, scaleY, rotation)
		} else {
			batch.draw(textureRegion.texture, thisX, thisY, if (!centerOriginOnXAxis) originX else (width / 2f),
					if (!centerOriginOnYAxis) originY else (height / 2f), width, height, scaleX, scaleY, rotation,
					textureRegion.regionX + srcX, textureRegion.regionY + srcY,
					Math.min(srcWidth, textureRegion.regionWidth),
					Math.min(srcHeight, textureRegion.regionHeight), false, false)
		}

		drawingOffset.set(previousOffset)
		textureRegion.flip(mirrorX, mirrorY)
	}

	@JvmOverloads
	fun draw(ninePatch: NinePatch, x: Float, y: Float, width: Float, height: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
			 rotation: Float = this.rotation, originX: Float = this.origin.x, originY: Float = this.origin.y,
			 centerOnXAxis: Boolean = false, centerOnYAxis: Boolean = false,
			 centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false,
			 color: Color = this.color, alpha: Float = this.alpha) {
		val oldAlpha = this.alpha
		this.alpha = alpha
		updateDrawingColorAndAlpha(color)

		val previousOffset = dummyVector2
		previousOffset.set(drawingOffset)

		if (centerOnXAxis || centerOnYAxis) resetDrawingOffset()

		if (centerOnXAxis) drawingOffset.x += -width / 2f
		if (centerOnYAxis) drawingOffset.y += -height / 2f

		val thisX = getFinalDrawingX(x)
		val thisY = getFinalDrawingY(y)

		ninePatch.draw(batch, thisX, thisY, if (!centerOriginOnXAxis) originX else (width / 2f),
				if (!centerOriginOnYAxis) originY else (height / 2f), width, height, scaleX, scaleY, rotation)

		drawingOffset.set(previousOffset)
		this.alpha = oldAlpha
	}

	@JvmOverloads
	fun draw(texture: Texture, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
			 rotation: Float = this.rotation,
			 originX: Float = this.origin.x, originY: Float = this.origin.y,
			 mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
			 customWidth: Float = -1f, customHeight: Float = -1f,
			 centerOnXAxis: Boolean = false, centerOnYAxis: Boolean = false,
			 centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false,
			 srcX: Int = -1, srcY: Int = -1, srcWidth: Int = -1, srcHeight: Int = -1,
			 color: Color = this.color, alpha: Float = this.alpha) {
		val oldAlpha = this.alpha
		this.alpha = alpha
		updateDrawingColorAndAlpha(color)
		val width = if (customWidth != -1f) customWidth else getUnitWidth(texture)
		val height = if (customHeight != -1f) customHeight else getUnitHeight(texture)

		val previousOffset = dummyVector2
		previousOffset.set(drawingOffset)

		if (centerOnXAxis || centerOnYAxis) resetDrawingOffset()

		if (displaceX != 0 || displaceY != 0) {
			if (displaceX != 0) drawingOffset.x += (width * displaceX)
			if (displaceY != 0) drawingOffset.y += (height * displaceY)
		}

		if (centerOnXAxis) drawingOffset.x += -width / 2f
		if (centerOnYAxis) drawingOffset.y += -height / 2f

		val thisX = getFinalDrawingX(x)
		val thisY = getFinalDrawingY(y)

		if (srcX <= 0 && srcY <= 0 && srcHeight <= 0 && srcWidth <= 0) {
			batch.draw(texture, thisX, thisY, if (!centerOriginOnXAxis) originX else (width / 2f),
					if (!centerOriginOnYAxis) originY else (height / 2f), width, height, scaleX, scaleY, rotation,
					0, 0, texture.width, texture.height, mirrorX, mirrorY)
		} else {
			batch.draw(texture, thisX, thisY, if (!centerOriginOnXAxis) originX else (width / 2f),
					if (!centerOriginOnYAxis) originY else (height / 2f), width, height, scaleX, scaleY, rotation,
					srcX, srcY,
					Math.min(srcWidth, texture.width),
					Math.min(srcHeight, texture.height), false, false)
		}

		drawingOffset.set(previousOffset)
		this.alpha = oldAlpha
	}

	@JvmOverloads
	fun draw(textureName: String, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
			 rotation: Float = this.rotation,
			 originX: Float = this.origin.x, originY: Float = this.origin.y,
			 mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
			 customWidth: Float = -1f, customHeight: Float = -1f,
			 centerOnXAxis: Boolean = false, centerOnYAxis: Boolean = false,
			 centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false,
			 srcX: Int = -1, srcY: Int = -1, srcWidth: Int = -1, srcHeight: Int = -1, color: Color = this.color) {
		this.draw(assets.getRegion(textureName), x, y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY,
				displaceX, displaceY, customWidth, customHeight, centerOnXAxis, centerOnYAxis,
				centerOriginOnXAxis, centerOriginOnYAxis, srcX, srcY, srcWidth, srcHeight, color)
	}

	@JvmOverloads
	fun draw(textureRegion: TextureRegion, position: Vector2, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
			 rotation: Float = this.rotation,
			 originX: Float = this.origin.x, originY: Float = this.origin.y,
			 mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
			 customWidth: Float = -1f, customHeight: Float = -1f,
			 centerOnXAxis: Boolean = false, centerOnYAxis: Boolean = false,
			 centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false, color: Color = this.color) {
		this.draw(textureRegion, position.x, position.y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY,
				displaceX, displaceY, customWidth, customHeight, centerOnXAxis, centerOnYAxis, centerOriginOnXAxis, centerOriginOnYAxis, color = color)
	}

	@JvmOverloads
	fun draw(textureName: String, position: Vector2, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
			 rotation: Float = this.rotation,
			 originX: Float = this.origin.x, originY: Float = this.origin.y,
			 mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
			 customWidth: Float = -1f, customHeight: Float = -1f,
			 centerOnXAxis: Boolean = false, centerOnYAxis: Boolean = false,
			 centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false, color: Color = this.color) {
		this.draw(assets.getRegion(textureName), position.x, position.y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY,
				displaceX, displaceY, customWidth, customHeight, centerOnXAxis, centerOnYAxis, centerOriginOnXAxis, centerOriginOnYAxis, color = color)
	}

	@JvmOverloads
	fun drawCentered(textureName: String, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
					 rotation: Float = this.rotation,
					 originX: Float = this.origin.x, originY: Float = this.origin.y,
					 mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
					 customWidth: Float = -1f, customHeight: Float = -1f,
					 centerOnXAxis: Boolean = true, centerOnYAxis: Boolean = true, color: Color = this.color) {
		this.draw(assets.getRegion(textureName), x, y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY, displaceX, displaceY, customWidth, customHeight,
				centerOnXAxis, centerOnYAxis, color = color)
	}

	@JvmOverloads
	fun drawCentered(textureRegion: TextureRegion, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
					 rotation: Float = this.rotation,
					 originX: Float = this.origin.x, originY: Float = this.origin.y,
					 mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
					 customWidth: Float = -1f, customHeight: Float = -1f,
					 centerOnXAxis: Boolean = true, centerOnYAxis: Boolean = true, color: Color = this.color) {
		this.draw(textureRegion, x, y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY, displaceX, displaceY, customWidth, customHeight,
				centerOnXAxis, centerOnYAxis, color = color)
	}

	@JvmOverloads
	fun drawWithOriginOnCenter(textureRegion: TextureRegion, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
							   rotation: Float = this.rotation, originX: Float = this.origin.x, originY: Float = this.origin.y,
							   mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
							   customWidth: Float = -1f, customHeight: Float = -1f,
							   centerOriginOnXAxis: Boolean = true, centerOriginOnYAxis: Boolean = true, color: Color = this.color) {
		this.draw(textureRegion, x, y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY, displaceX, displaceY, customWidth, customHeight,
				centerOriginOnXAxis = centerOriginOnXAxis, centerOriginOnYAxis = centerOriginOnYAxis, color = color)
	}

	@JvmOverloads
	fun drawWithOriginOnCenter(textureRegion: TextureRegion, position: Vector2, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
							   rotation: Float = this.rotation, originX: Float = this.origin.x, originY: Float = this.origin.y,
							   mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
							   customWidth: Float = -1f, customHeight: Float = -1f,
							   centerOriginOnXAxis: Boolean = true, centerOriginOnYAxis: Boolean = true, color: Color = this.color) {
		this.draw(textureRegion, position.x, position.y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY, displaceX, displaceY, customWidth, customHeight,
				centerOriginOnXAxis = centerOriginOnXAxis, centerOriginOnYAxis = centerOriginOnYAxis, color = color)
	}

	@JvmOverloads
	fun drawWithOriginOnCenter(textureName: String, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
							   rotation: Float = this.rotation, originX: Float = this.origin.x, originY: Float = this.origin.y,
							   mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
							   customWidth: Float = -1f, customHeight: Float = -1f,
							   centerOriginOnXAxis: Boolean = true, centerOriginOnYAxis: Boolean = true, color: Color = this.color) {
		this.draw(assets.getRegion(textureName), x, y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY, displaceX, displaceY, customWidth, customHeight,
				centerOriginOnXAxis = centerOriginOnXAxis, centerOriginOnYAxis = centerOriginOnYAxis, color = color)
	}

	@JvmOverloads
	fun drawCenteredWithOriginOnCenter(textureRegion: TextureRegion, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
									   rotation: Float = this.rotation, originX: Float = this.origin.x, originY: Float = this.origin.y,
									   mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
									   customWidth: Float = -1f, customHeight: Float = -1f,
									   centerOnXAxis: Boolean = true, centerOnYAxis: Boolean = true,
									   centerOriginOnXAxis: Boolean = true, centerOriginOnYAxis: Boolean = true, color: Color = this.color) {
		this.draw(textureRegion, x, y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY, displaceX, displaceY, customWidth, customHeight,
				centerOnXAxis, centerOnYAxis, centerOriginOnXAxis, centerOriginOnYAxis, color = color)
	}

	@JvmOverloads
	fun drawCenteredWithOriginOnCenter(textureName: String, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
									   rotation: Float = this.rotation, originX: Float = this.origin.x, originY: Float = this.origin.y,
									   mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
									   customWidth: Float = -1f, customHeight: Float = -1f,
									   centerOnXAxis: Boolean = true, centerOnYAxis: Boolean = true,
									   centerOriginOnXAxis: Boolean = true, centerOriginOnYAxis: Boolean = true, color: Color = this.color) {
		this.draw(assets.getRegion(textureName), x, y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY, displaceX, displaceY, customWidth, customHeight,
				centerOnXAxis, centerOnYAxis, centerOriginOnXAxis, centerOriginOnYAxis, color = color)
	}

	private val tmpCharRegion = TextureRegion()
	@JvmOverloads
	fun drawChar(char: Char, font: BitmapFont, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
				 rotation: Float = this.rotation,
				 originX: Float = this.origin.x, originY: Float = this.origin.y,
				 mirrorX: Boolean = false, mirrorY: Boolean = false, displaceX: Int = 0, displaceY: Int = 0,
				 customWidth: Float = -1f, customHeight: Float = -1f,
				 centerOnXAxis: Boolean = false, centerOnYAxis: Boolean = false,
				 centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false,
				 srcX: Int = -1, srcY: Int = -1, srcWidth: Int = -1, srcHeight: Int = -1, color: Color = this.color) {
		val glyph = font.data.getGlyph(char)
		tmpCharRegion.texture = font.regions[glyph.page].texture
		tmpCharRegion.setRegion(glyph.u, glyph.v, glyph.u2, glyph.v2)
		tmpCharRegion.flip(false, true)
		this.draw(tmpCharRegion, x, y, scaleX, scaleY, rotation, originX, originY, mirrorX, mirrorY,
				displaceX, displaceY, customWidth, customHeight, centerOnXAxis, centerOnYAxis,
				centerOriginOnXAxis, centerOriginOnYAxis, srcX, srcY, srcWidth, srcHeight, color = color)
	}

	/**
	 * Draws an image scaled to the nearest non-decimal scale factor, based on the densityFactor / resolutionFactor of the app
	 * This means that the images will -almost- always have the same physical size on all devices
	 *
	 * Use it with ScreenViewport - since with a scaling viewport the above can't be achieved
	 * @param useDensityFactor if false will use resolution factor
	 */
	fun drawScaled(textureRegion: TextureRegion, x: Float, y: Float, baseSize: Float, useDensityFactor: Boolean,
				   resolutionFactorBaseWidth: Int, color: Color = this.color) {
		val thisX = getFinalDrawingX(x)
		val thisY = getFinalDrawingY(y)

		val scaleFactor = getImageScaleFactor(baseSize, textureRegion.regionWidth.toFloat(),
				textureRegion.regionHeight.toFloat(), useDensityFactor, resolutionFactorBaseWidth)

		updateDrawingColorAndAlpha(color)
		batch.draw(textureRegion, thisX, thisY, (textureRegion.regionWidth * scaleFactor).toFloat(),
				(textureRegion.regionHeight * scaleFactor).toFloat())
	}

	private val fontColor = Color()

	@JvmOverloads
	fun drawText(text: Any?, x: Float, y: Float, font: BitmapFont, color: Color = this.color,
				 scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
				 hAlign: Int = Align.left, targetWidth: Float = 0f, wrap: Boolean = false) {
		val endScaleX = (1f / usedPPM) * scaleX
		val endScaleY = (1f / usedPPM) * scaleY
		if (endScaleX == 0f || endScaleY == 0f) return

		val oldColor = font.color
		fontColor.set(color)
		fontColor.a = alpha

		font.color = fontColor

		val oldScaleX = font.getScaleX()
		val oldScaleY = font.getScaleY()
		val usedIntegers = font.usesIntegerPositions()
		font.setUseIntegerPositions(false)
		font.getData().setScale(endScaleX, endScaleY)

		font.draw(batch, text.toString(), x + drawingOffset.x, y + drawingOffset.y, targetWidth, hAlign, wrap)

		font.getData().setScale(oldScaleX, oldScaleY)
		font.setUseIntegerPositions(usedIntegers)
		font.color = oldColor
	}

	/**
	 * Draws a Rectangle using "pixel" image on atlas
	 *
	 * @param x         bottom-left corner x
	 * @param y         bottom-left corner y
	 * @param thickness size of the borders if the rectangle is not filled
	 * @param fill      whether the rectangle drawn will be filled with the color
	 */
	@JvmOverloads
	fun drawRectangle(x: Float, y: Float, width: Float, height: Float, fill: Boolean, thickness: Float = Engine.DEBUG_LINE_THICKNESS,
					  originX: Float = this.origin.x, originY: Float = this.origin.y,
					  scaleX: Float = this.scaleX, scaleY: Float = this.scaleY, rotation: Float = this.rotation,
					  color: Color = this.color) {
		updateDrawingColorAndAlpha(color)
		val thisX = getFinalDrawingX(x)
		val thisY = getFinalDrawingY(y)

		if (!fill) {
			batch.draw(pixelTexture, thisX + thickness, thisY, originX, originY, width - thickness * 2, thickness, scaleX, scaleY, rotation)
			batch.draw(pixelTexture, thisX, thisY, originX, originY, thickness, height, scaleX, scaleY, rotation)
			batch.draw(pixelTexture, thisX + thickness, thisY + height - thickness, originX, originY, width - thickness * 2, thickness, scaleX, scaleY, rotation)
			batch.draw(pixelTexture, thisX + width - thickness, thisY, originX, originY, thickness, height, scaleX, scaleY, rotation)
		} else {
			batch.draw(pixelTexture, thisX, thisY, originX, originY, width, height, scaleX, scaleY, rotation)
		}
	}

	private var shapeDrawer: ShapeDrawer? = null
		get() {
			if (field == null) field = ShapeDrawer(batch, pixelTexture)
			field!!.setPixelSize(pixelSizeScreen())
			return field
		}

	@JvmOverloads
	fun drawCircle(x: Float, y: Float, radius: Float, fill: Boolean = true, lineThickness: Float = Engine.DEBUG_LINE_THICKNESS,
				   scale: Float = 1f, color: Color = this.color) {
		updateDrawingColorAndAlphaShapeDrawer(color)
		val thisX = getFinalDrawingX(x)
		val thisY = getFinalDrawingY(y)

//        batch.draw(pixelCircle, thisX, thisY, originX, originY, size, size, scaleX, scaleY, rotation)

		if (!fill)
			shapeDrawer!!.circle(thisX, thisY, radius * scale, lineThickness * scale)
		else
			shapeDrawer!!.filledCircle(thisX, thisY, radius * scale)
	}

	@JvmOverloads
	fun drawArc(x: Float, y: Float, radius: Float, startAngle: Float, angleAmount: Float,
				fill: Boolean = true, lineThickness: Float = Engine.DEBUG_LINE_THICKNESS,
				scale: Float = 1f, color: Color = this.color) {
		updateDrawingColorAndAlphaShapeDrawer(color)
		val thisX = getFinalDrawingX(x)
		val thisY = getFinalDrawingY(y)

		if (!fill)
			shapeDrawer!!.arc(thisX, thisY, radius * scale,
					MathUtils.degreesToRadians * startAngle,
					MathUtils.degreesToRadians * angleAmount, lineThickness * scale)
		else
			shapeDrawer!!.sector(thisX, thisY, radius * scale,
					MathUtils.degreesToRadians * startAngle,
					MathUtils.degreesToRadians * angleAmount)
	}

	@JvmOverloads
	fun drawEllipse(x: Float, y: Float, radiusX: Float, radiusY: Float,
					fill: Boolean = true, lineThickness: Float = Engine.DEBUG_LINE_THICKNESS,
					rotation: Float = 0f, scale: Float = 1f, color: Color = this.color) {
		updateDrawingColorAndAlphaShapeDrawer(color)
		val thisX = getFinalDrawingX(x)
		val thisY = getFinalDrawingY(y)

//        batch.draw(pixelCircle, thisX, thisY, originX, originY, size, size, scaleX, scaleY, rotation)

		if (!fill)
			shapeDrawer!!.ellipse(thisX, thisY, radiusX * scale, radiusY * scale, rotation, lineThickness * scale)
		else
			shapeDrawer!!.filledEllipse(thisX, thisY, radiusX * scale, radiusY * scale, rotation)
	}

	@JvmOverloads
	fun drawPolygon(polygon: Polygon, fill: Boolean = true, lineThickness: Float = Engine.DEBUG_LINE_THICKNESS,
					color: Color = this.color) {
		updateDrawingColorAndAlphaShapeDrawer(color)

		var even = true
		val verts = polygon.transformedVertices
		verts.forEachIndexed { index, fl ->
			verts[index] =
					if (even) fl + drawingOffset.x
					else fl + drawingOffset.y
			even = !even
		}

		if (!fill)
			shapeDrawer!!.polygon(polygon, lineThickness)
		else
			shapeDrawer!!.filledPolygon(polygon)

		even = true
		verts.forEachIndexed { index, fl ->
			verts[index] =
					if (even) fl - drawingOffset.x
					else fl - drawingOffset.y
			even = !even
		}
	}

	@JvmOverloads
	fun drawCircle(circle: Circle, fill: Boolean = true, lineThickness: Float = Engine.DEBUG_LINE_THICKNESS,
				   scale: Float = 1f, color: Color = this.color) {
		this.drawCircle(circle.x, circle.y, circle.radius, fill, lineThickness, scale, color)
	}

	fun drawRectangle(rectangle: Rectangle, fill: Boolean, thickness: Float = Engine.DEBUG_LINE_THICKNESS,
					  originX: Float = this.origin.x, originY: Float = this.origin.y,
					  scaleX: Float = this.scaleX, scaleY: Float = this.scaleY, rotation: Float = this.rotation,
					  color: Color = this.color) {
		this.drawRectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height, fill, thickness,
				originX, originY, scaleX, scaleY, rotation, color)
	}

	fun drawShape(shape2D: Shape2D, thickness: Float = Engine.DEBUG_LINE_THICKNESS, fill: Boolean = false,
				  originX: Float = this.origin.x, originY: Float = this.origin.y,
				  scaleX: Float = this.scaleX, scaleY: Float = this.scaleY, rotation: Float = this.rotation, color: Color = this.color) {
		if (shape2D is Rectangle)
			this.drawRectangle(shape2D, fill, thickness, originX, originY, scaleX, scaleY, rotation, color)
		else if (shape2D is Circle)
			this.drawCircle(shape2D, fill, thickness, Math.max(scaleX, scaleY), color)
		else if (shape2D is Polygon) {
			drawPolygon(shape2D, fill, thickness, color)
		}
	}

	fun drawCross(x: Float, y: Float, axisSize: Float, thickness: Float = Engine.DEBUG_LINE_THICKNESS, color: Color = this.color) {
		this.drawLine(x - axisSize, y, x + axisSize, y, thickness, color = color)
		this.drawLine(x, y - axisSize, x, y + axisSize, thickness, color = color)
	}

	/**
	 * Draws a line using "pixel" image on atlas. Lines drawn together will not be correctly joined.
	 *
	 * @param x1        start x
	 * @param y1        start y
	 * @param x2        end x
	 * @param y2        end y
	 * @param thickness size of the line
	 */
	@JvmOverloads
	fun drawLine(x1: Float, y1: Float, x2: Float, y2: Float, thickness: Float = Engine.DEBUG_LINE_THICKNESS, isArrow: Boolean = false,
				 arrowSize: Float = Engine.DEBUG_LINE_THICKNESS * 10, color: Color = this.color) {
		updateDrawingColorAndAlpha(color)
		val thisX1 = getFinalDrawingX(x1)
		val thisX2 = getFinalDrawingX(x2)
		val thisY1 = getFinalDrawingY(y1)
		val thisY2 = getFinalDrawingY(y2)

		val dx = thisX2 - thisX1
		val dy = thisY2 - thisY1
		val dist = Math.sqrt((dx * dx + dy * dy).toDouble()).toFloat()
		val deg = Math.toDegrees(Math.atan2(dy.toDouble(), dx.toDouble())).toFloat()
		batch.draw(pixelTexture, thisX1, thisY1, 0f, thickness / 2f, dist, thickness, 1f, 1f, deg)

		if (isArrow) {
			val angle = Math.toRadians(Utils.angleBetween(x1, y1, x2, y2).toDouble())
			val angleDiff = -40
			val angle1 = angle + angleDiff
			val angle2 = angle - angleDiff
			drawLine(x2, y2, (x2 + (Math.cos(angle1) * arrowSize)).toFloat(), (y2 + (Math.sin(angle1) * arrowSize)).toFloat(), thickness, isArrow = false)
			drawLine(x2, y2, (x2 + (Math.cos(angle2) * arrowSize)).toFloat(), (y2 + (Math.sin(angle2) * arrowSize)).toFloat(), thickness, isArrow = false)
		}
	}

	fun drawLine(start: Vector2, end: Vector2, thickness: Float = Engine.DEBUG_LINE_THICKNESS,
				 isArrow: Boolean = false, arrowSize: Float = Engine.DEBUG_LINE_THICKNESS * 10, color: Color = this.color) {
		this.drawLine(start.x, start.y, end.x, end.y, thickness, isArrow, arrowSize, color)
	}

	fun drawLineFromAngle(x1: Float, y1: Float, distance: Float, angleDegrees: Float, thickness: Float = Engine.DEBUG_LINE_THICKNESS,
						  isArrow: Boolean = false, arrowSize: Float = Engine.DEBUG_LINE_THICKNESS * 10, color: Color = this.color) {
		this.drawLine(x1, y1, ((x1 + distance * Math.cos(Math.toRadians(angleDegrees.toDouble()))).toFloat()),
				((y1 + distance * Math.sin(Math.toRadians(angleDegrees.toDouble()))).toFloat()), thickness, isArrow, arrowSize, color)
	}

	companion object {
		/**
		 * Gets the nearest non-decimal scale factor for the image, based on the densityFactor / resolutionFactor of the app
		 * @param useDensityFactor if false will use resolution factor
		 */
		fun getImageScaleFactor(size: Float, imageWidthPixels: Float, imageHeightPixels: Float,
								useDensityFactor: Boolean = true, resolutionFactorBaseWidth: Int): Int {
			val pixels: Float =
					if (useDensityFactor)
						1f // Engine.densityFactor * size
					else
						Engine.getResolutionFactor(resolutionFactorBaseWidth.toFloat())

			return Math.round(pixels / Math.max(imageHeightPixels, imageWidthPixels))
		}
	}
}