package com.dcostap.engine.utils.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.Engine
import com.dcostap.engine.debug.DebugCommand
import com.dcostap.engine.debug.TextDebugCommand
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.actions.ActionsUpdater
import com.dcostap.engine.utils.input.InputController
import com.dcostap.engine.utils.ui.ExtTable

/**
 * Convenience behavior already coded: base variables with Engine, a Camera, a Viewport, InputController and GameDrawer
 *
 * Extend this class instead of ScreenAdapter if you need all that functionality
 */
abstract class BaseScreen(val engine: Engine) : Screen, Drawable, Updatable {
	val camera get() = worldViewport.camera as OrthographicCamera

	var worldViewport: Viewport
		protected set

	var debugDrawMousePosition = false

	val assets get() = engine.assets

	val gd: GameDrawer

	val worldInput: InputController

	interface ResizeListener {
		fun resize(width: Int, height: Int)
	}

	val resizeNotifier = Notifier<ResizeListener>()

	var inputMulti: InputMultiplexer

	/** If value is not -1, [Engine.PPU] will be temporarily changed while this screen is loaded */
	open fun PPU(): Int = -1

	val camUtils = CameraUtils(this)

	init {
		worldViewport = createViewport()
		gd = GameDrawer(engine.batch, engine.assets, worldViewport)

		worldInput = InputController(worldViewport)
		inputMulti = InputMultiplexer(Engine.debugUI.stage, worldInput)

		Gdx.input.inputProcessor = inputMulti

		engine.debugCommands.clear()
		engine.debugCommands += TextDebugCommand("mouse") { debugDrawMousePosition = !debugDrawMousePosition }
		engine.debugCommands += TextDebugCommand("cam", description = "control camera with numpad") {
			camUtils.debugMoveCamera = !camUtils.debugMoveCamera
		}

		engine.debugCommands += addDebugCommands()
	}

	open fun addDebugCommands(): Array<DebugCommand> {
		return arrayOf()
	}

	override fun hide() {

	}

	override fun show() {

	}

	/** used by TiledMap */
	lateinit var lightFbo: FrameBuffer
		private set

	lateinit var gameFbo: FrameBuffer
		private set

	open var worldInputEnabled: Boolean = true
		set(value) {
			if (value != field) {
				if (value) {
					inputMulti.clear()
					inputMulti.addProcessor(Engine.debugUI.stage)
					inputMulti.addProcessor(worldInput)
				} else
					inputMulti.removeProcessor(1)
			}
			field = value
		}

	val actions = ActionsUpdater()

	/** Override to change created viewport */
	abstract fun createViewport(): Viewport

	private var firstUpdate = true

	/** Called on the first frame update() method is called. Surprisingly useful */
	open fun firstUpdate(delta: Float) {

	}

	var timePassed = 0f

	override fun update(delta: Float) {
		if (firstUpdate) {
			firstUpdate = false
			firstUpdate(delta)
		}

		worldInput.update(delta)

		actions.update(delta)

		camUtils.update(delta)

		timePassed += delta
	}

	override fun draw(gd: GameDrawer, delta: Float) {
		camUtils.beforeDrawing(delta)

		// update camera & viewport
		worldViewport.apply()



		// start the batch
		engine.batch.projectionMatrix = camera.combined

		gameFbo.use {
			Utils.clearScreen(Color.BLACK)
			engine.batch.use {
				it.setColor(1f, 1f, 1f, 1f)
				drawWorld(gd, delta)
			}
		}

		drawFboContents()

		camUtils.afterDrawing(delta)
	}

	private fun drawFboContents() {
		val oldBatch = gd.batch

		gd.batch = engine.fboBatch

		// had to use a dummy ScreenViewport to allow to get the projection of just the full game screen size
		// there must be a better way of doing this
		dummyFullScreenViewport.apply()

		gd.batch.projectionMatrix = dummyFullScreenViewport.camera.combined
		gd.batch.setColor(1f, 1f, 1f, 1f)
		gd.batch.setBlendFunctionSeparate(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)

		fun drawFbo() {
			val width = worldViewport.screenWidth.float
			val height = worldViewport.screenHeight.float
			Utils.clearScreen(Color.BLACK)
			gd.batch.draw(gameFbo.colorBufferTexture,
					((Gdx.graphics.width - width) / 2f).floorf(),
					((Gdx.graphics.height - height) / 2f).floorf(),
					0f, 0f, width, height,
					1f, 1f, 0f, 0, 0,
					gameFbo.colorBufferTexture.width, gameFbo.colorBufferTexture.height, false, true)
		}

		gd.batch.use {
			if (blurSampleDistance > 0f && blurSampleStrength > 0f) {
				gd.batch.shaderMotionBlur(blurOriginX, blurOriginY, blurSampleDistance, blurSampleStrength) {
					drawFbo()
				}
			} else {
				drawFbo()
			}
		}

		// reset blending function to default
		gd.batch = oldBatch

		worldViewport.apply()
		gd.batch.projectionMatrix = camera.combined
	}

	open var blurSampleStrength = 0f
	open var blurSampleDistance = 0f

	/** 1 = completely to the right, 0 = to the left */
	open var blurOriginX = 0.5f
	open var blurOriginY = 0.5f

	fun setBlurOriginToWorldPosition(worldX: Float, worldY: Float) {
		Utils.projectPosition(worldX, worldY, worldViewport).also {
			blurOriginX = it.x / Gdx.graphics.width
			blurOriginY = it.y / Gdx.graphics.height
		}
	}

	fun debugBlur() {
		Engine.debugUI.addDebugVariableSlider("blur strength", 0f, 1f, 0.025f, { blurSampleStrength }) {
			blurSampleStrength = it
		}

		Engine.debugUI.addDebugVariableSlider("blur distance", 0f, 1f, 0.025f, { blurSampleDistance }) {
			blurSampleDistance = it
		}
	}

	abstract fun drawWorld(gd: GameDrawer, delta: Float)

	override fun render(delta: Float) {
		if (Gdx.app.graphics.width == 0) return // wait for resize

		update(delta)

		// round camera position to nearest screen pixel
		val origX = camera.position.x
		val origY = camera.position.y
		val pixelSize = (1f / Engine.PPU) / ((worldViewport.screenWidth / worldViewport.worldWidth) / (Engine.PPU)) *
				(worldViewport.camera as OrthographicCamera).zoom
		camera.position.x = (MathUtils.round(camera.position.x / pixelSize) * pixelSize)
		camera.position.y = (MathUtils.round(camera.position.y / pixelSize) * pixelSize)

		draw(gd, delta)

		lateDrawing(gd, delta)

		camera.position.set(origX, origY, 0f)
	}

	val debugDrawMouseDragStart = Vector2()

	private fun lateDrawing(gd: GameDrawer, delta: Float) {
		if (debugDrawMousePosition) {
			Engine.debugUI.drawTextInWorld("x: ${worldInput.mouseWorld.x}; y: ${worldInput.mouseWorld.y}",
					worldInput.mouseWorld.x, worldInput.mouseWorld.y + 1f, worldViewport, disappearTime = 0f)

			if (worldInput.isJustPressed()) {
				debugDrawMouseDragStart.set(worldInput.mouseWorld)
			}

			// measuring drag length
			if (worldInput.isPressed()) {
				pool(Vector2::class) { length ->
					length.set(worldInput.mouseWorld)
					length.sub(debugDrawMouseDragStart)

					Engine.debugUI.drawTextInWorld(length.toString(),
							debugDrawMouseDragStart.x, debugDrawMouseDragStart.y - 1f, worldViewport, Color.CYAN, disappearTime = 0f)

					Engine.debugUI.drawLineInWorld(debugDrawMouseDragStart.x, debugDrawMouseDragStart.y,
							worldInput.mouseWorld.x, worldInput.mouseWorld.y,
							worldViewport, isArrow = true, color = Color.CYAN, disappearTime = 0f)
				}
			}
		}
	}

	val dummyFullScreenViewport = ScreenViewport()

	override fun resize(width: Int, height: Int) {
		dummyFullScreenViewport.update(width, height, true)
		worldViewport.update(width, height)
		Engine.debugUI.resize(width, height)
		resizeNotifier.notifyListeners { it.resize(width, height) }

		if (width == 0 || height == 0) return

//		val widthPow2 = Utils.nextPowerOf2(width) //todo power of 2 fbos for compatibility
//		val heightPow2 = Utils.nextPowerOf2(height)

		if (this::lightFbo.isInitialized) {
			if (width == lightFbo.width && height == lightFbo.height) return
			lightFbo.dispose()
		}

		lightFbo = FrameBuffer(Pixmap.Format.RGBA8888, width, height, false)

		if (this::gameFbo.isInitialized) {
			if (width == gameFbo.width && height == gameFbo.height) return
			gameFbo.dispose()
		}

		// for some reason had to disable the A (alpha) component in the game fbo. Enabling it makes semi-transparent textures look darker.
		// luckily alpha isn't needed for this fbo
		gameFbo = FrameBuffer(Pixmap.Format.RGB888, width, height, true)
		gameFbo.colorBufferTexture.setFilter(gameFboFilterMin, gameFboFilterMax)
	}

	open fun extraDebugInfoForDebugStage(): String {
		return ""
	}

	open fun extraUIForDebugStage(): Table? {
		return null
	}

	open val gameFboFilterMin get() = Texture.TextureFilter.Linear
	open val gameFboFilterMax get() = Texture.TextureFilter.Linear

	open fun openDebugWindow(table: ExtTable) {}

	/** when android app regains focus; in desktop when app closes, called before [dispose] */
	override fun pause() {}

	/** only for android, when app loses focus */
	override fun resume() {}

	override fun dispose() {
		Engine.debugUI.stage.clear()
		lightFbo.dispose()
		gameFbo.dispose()
		engine.debugCommands.clear()
	}
}
