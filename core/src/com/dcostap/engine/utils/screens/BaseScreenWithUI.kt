package com.dcostap.engine.utils.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.dcostap.Engine
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.Notifier
import com.dcostap.engine.utils.input.InputController
import com.dcostap.engine.utils.use
import ktx.collections.*

/**
 * Created by Darius on 28/12/2017.
 *
 * Adds base UI functionality: UIController and a Stage
 */
abstract class BaseScreenWithUI(engine: Engine) : BaseScreen(engine) {
	val stage: Stage
	val stageInput: InputController

	init {
		stage = createStage()
		stageInput = InputController(stage.viewport)

		inputMulti = InputMultiplexer(Engine.debugUI.stage, stage, stageInput, worldInput)
		Gdx.input.inputProcessor = inputMulti
	}

	val drawOnUIBeforeStageNotifier = Notifier<DrawOnUIBeforeStageListener>()

	override var worldInputEnabled: Boolean = true
		set(value) {
			if (value != field) {
				if (value) {
					inputMulti.clear()
					inputMulti.addProcessor(Engine.debugUI.stage)

					if (stageInputEnabled) {
						inputMulti.addProcessor(stage)
						inputMulti.addProcessor(stageInput)
					}
					inputMulti.addProcessor(worldInput)
				} else
					inputMulti.removeProcessor(worldInput)
			}
			field = value
		}

	open var stageInputEnabled: Boolean = true
		set(value) {
			if (value != field) {
				if (value) {
					inputMulti.clear()
					inputMulti.addProcessor(Engine.debugUI.stage)
					inputMulti.addProcessor(stage)
					inputMulti.addProcessor(stageInput)

					if (worldInputEnabled)
						inputMulti.addProcessor(worldInput)
				} else {
					inputMulti.removeProcessor(stage)
					inputMulti.removeProcessor(stageInput)
				}
			}
			field = value
		}

	override fun update(delta: Float) {
		super.update(delta)

		stageInput.update(delta)
	}

	private val gameDrawerUI = GameDrawer(stage.batch, engine.assets, stage.viewport).also {
		it.useCustomPPM = true; it.customPPM = 1
	}

	override fun draw(gd: GameDrawer, delta: Float) {
		super.draw(gd, delta)
		drawUI(gameDrawerUI, delta)
	}

	open fun drawUI(gd: GameDrawer, delta: Float) {
		stage.act(delta)
		stage.viewport.apply()

		if (drawOnUIBeforeStageNotifier.listeners.isNotEmpty()) {
			stage.batch.use {
				drawOnUIBeforeStageNotifier.notifyListeners { it.drawOnUIBeforeStage(gd, delta) }
			}
		}

		stage.draw()
	}

	/** Override to change created Stage */
	abstract fun createStage(): Stage

	override fun resize(width: Int, height: Int) {
		super.resize(width, height)
		stage.viewport.update(width, height, true)
	}

	override fun dispose() {
		super.dispose()
		stage.dispose()
	}

	interface DrawOnUIBeforeStageListener {
		fun drawOnUIBeforeStage(gd: GameDrawer, delta: Float)
	}
}
