package com.dcostap.engine.utils.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.dcostap.Engine
import com.dcostap.engine.map.EntityMap
import com.dcostap.engine.utils.*
import kotlin.math.min

open class CameraUtils(val screen: BaseScreen, var getMap: () -> EntityMap? = { null }) : Updatable {
	private val camera
		get() = screen.camera

	var debugMoveCamera = false
	var debugCamMoveSpeed = 70.pixelsToUnits
	var debugCamZoomSpeed = 0.7f

	var cameraMovementWithInterpolation = false

	private var camObjectiveX: Float = 0f
	private var camObjectiveY: Float = 0f

	private val noise = OpenSimplexNoise()

	var screenShakeIntensity = 0f

	var clampCameraToMapBounds = false

	/** If [cameraMovementWithInterpolation] is false it will just move the camera normally */
	fun moveCameraInterpolated(x: Float, y: Float) {
		if (!cameraMovementWithInterpolation) {
			camera.position.set(x, y, camera.position.z)
			return
		}

		if (!debugMoveCamera) {
			camObjectiveX = x
			camObjectiveY = y
		}
	}

	fun clampCamera() {
		if (!clampCameraToMapBounds) return

		getMap().ifNotNull { map ->
			if (camera.leftX() < 0f) camera.setLeftX(0f)
			if (camera.rightX() > map.widthUnits) camera.setRightX(map.widthUnits)
			if (camera.bottomY() < 0f) camera.setBottomY(0f)
			if (camera.topY() > map.heightUnits) camera.setTopY(map.heightUnits)
		}
	}

	override fun update(delta: Float) {
		if (Engine.DEBUG && Engine.debugUI.commandsUIClosed && debugMoveCamera) {
			if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_4)) {
				camera.position.x -= debugCamMoveSpeed * delta * camera.zoom
			}
			if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_6)) {
				camera.position.x += debugCamMoveSpeed * delta * camera.zoom
			}
			if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_5)) {
				camera.position.y -= debugCamMoveSpeed * delta * camera.zoom
			}
			if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_8)) {
				camera.position.y += debugCamMoveSpeed * delta * camera.zoom
			}
			if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_1)) {
				camera.zoom += debugCamZoomSpeed * delta
			}
			if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_3)) {
				camera.zoom -= debugCamZoomSpeed * delta
			}
			if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_0)) {
				camera.zoom = 1f
			}
		} else {
			// interpolate camera to objective position
			if (cameraMovementWithInterpolation) {
				val diff = Utils.distanceBetween(camera.position.x, camera.position.y, camObjectiveX, camObjectiveY)
				val angle = Utils.angleBetween(camera.position.x, camera.position.y, camObjectiveX, camObjectiveY)
				var movement = interpolatedMovementMapping(diff) * delta
				movement = min(diff, movement)
				camera.position.add(Utils.angleMovementX(movement, angle), Utils.angleMovementY(movement, angle), 0f)
			}
		}
	}

	/** Change to configure [cameraMovementWithInterpolation]. Map the distance to the objective camera position to how much it will move
	 * (in units per second) */
	var interpolatedMovementMapping: (distanceToObjective: Float) -> Float = {
		map(it, 0f, 0.5f, 20f, 200f, Easing.pow2)
	}

	private var origCamPosX = 0f
	private var origCamPosY = 0f
	private var origRot = 0f

	fun beforeDrawing(delta: Float) {
		clampCamera()

		origCamPosX = camera.position.x
		origCamPosY = camera.position.y
		origRot = camera.getRotation()

		if (screenShakeIntensity > 0f) {
			noise.advanceDelta(delta * 23) // intensity of shake
			// the less intensity, the less shake; it's not linear
			fun smooth(intensity: Float): Float {
				return map(intensity, 0f, 1f, 0f, 1f, Easing.linear)
			}

			camera.position.x += 3.pixelsToUnits * smooth(screenShakeIntensity) *
					map(noise.eval(0f), 0f, 1f, -1f, 1f)
			camera.position.y += 3.pixelsToUnits * smooth(screenShakeIntensity) *
					map(noise.eval(60f), 0f, 1f, -1f, 1f)
			camera.setRotation(camera.getRotation() + 5 * smooth(screenShakeIntensity) *
					map(noise.eval(120f), 0f, 1f, -1f, 1f))

			clampCamera()
			screenShakeIntensity -= delta * 1.4f
			if (screenShakeIntensity < 0f) {
				screenShakeIntensity = 0f
			}
		}
	}

	fun afterDrawing(delta: Float) {
		camera.position.set(origCamPosX, origCamPosY, 0f)
		camera.setRotation(origRot)
	}
}