package com.dcostap.engine.utils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.dcostap.engine.map.EntityMap
import java.io.File

/** Created by Darius on 24-Aug-19. */
val defaultShader = SpriteBatch.createDefaultShader()

var outlineShader: ShaderProgram = defaultShader
var tintShader: ShaderProgram = defaultShader
var colorShader: ShaderProgram = defaultShader
var waterShader: ShaderProgram = defaultShader
var shadowShader: ShaderProgram = defaultShader
var replaceSkinColor: ShaderProgram = defaultShader
var waterReplaceSkinColorAndTint: ShaderProgram = defaultShader
var replaceSkinColorAndTint: ShaderProgram = defaultShader
var motionBlurShader: ShaderProgram = defaultShader

val tmpColor = Color()

fun loadShaders() {
	outlineShader = Shader("outline", "outline")
	tintShader = Shader("default", "tint")
	colorShader = Shader("default", "color")
	waterShader = Shader("default", "water")
	shadowShader = Shader("default", "shadow")
	replaceSkinColor = Shader("default", "replaceSkinColor")
	waterReplaceSkinColorAndTint = Shader("default", "waterReplaceSkinColorAndTint")
	replaceSkinColorAndTint = Shader("default", "replaceSkinColorAndTint")
	motionBlurShader = Shader("motionBlur", "motionBlur")
}

fun disposeShaders() {
	outlineShader.dispose()
	tintShader.dispose()
	colorShader.dispose()
	waterShader.dispose()
	shadowShader.dispose()
	replaceSkinColor.dispose()
	waterReplaceSkinColorAndTint.dispose()
	motionBlurShader.dispose()
}

/** Using shaders, tints batch to the color specified. Color.BLACK means no tinting. */
inline fun Batch.shaderTint(color: Color, f: () -> Unit) {
	flush()
	shader = tintShader
	tmpColor.set(color)
	tmpColor.a = 0f
	tintShader.setUniformf("u_emissive", tmpColor)
	f()
	flush()
	shader = null
}

/** @param originX 1f = rightmost, 0f = leftmost, 0.5f = center
 * @param originY see [originX]
 * @param blurSampleRadius 0.5 = size of non-blurred area is half of screen; 1 = non-blurred area is a tiny dot in the center
 * @param blurSampleStrength 0 = none; 1 = very blurry */
inline fun Batch.shaderMotionBlur(originX: Float, originY: Float, blurSampleRadius: Float, blurSampleStrength: Float, f: () -> Unit) {
	flush()
	shader = motionBlurShader
	shader.setUniformf("sampleDist", map(blurSampleRadius, 0f, 1f, 0f, 9f, Interpolation.pow2In))
	shader.setUniformf("sampleStrength", map(blurSampleStrength, 0f, 1f, 0f, 0.6f, Interpolation.pow2In))
	shader.setUniformf("originX", originX)
	shader.setUniformf("originY", originY)
	f()
	flush()
	shader = null
}

/** Using shaders, replaces all non transparent pixels with the color specified. */
inline fun Batch.shaderColor(color: Color, f: () -> Unit) {
	flush()
	shader = colorShader
	tmpColor.set(color)
	colorShader.setUniformf("u_emissive", tmpColor)
	f()
	flush()
	shader = null
}


inline fun Batch.shaderOutline(textureRegion: TextureRegion, outlineSize: Float, color: Color, f: () -> Unit) {
	val sprite = textureRegion
	flush()
	ShaderProgram.pedantic = false
	shader = outlineShader
	shader.begin()
	shader.setUniformf("u_viewportInverse", Vector2(1f / sprite.texture.width, 1f / sprite.texture.height))
	shader.setUniformf("u_offset", outlineSize)
	shader.setUniformf("u_step", Math.min(1f, 1f))
	shader.setUniformf("u_color", Vector3(color.r, color.g, color.b))
	if (sprite.isFlipX)
		shader.setUniformf("u_u", Vector2(sprite.u2, sprite.u))
	else
		shader.setUniformf("u_u", Vector2(sprite.u, sprite.u2))
	shader.setUniformf("u_v", Vector2(sprite.v, sprite.v2))
	shader.end()

	f()
	flush()
	shader = null
}

/** Using shaders, replaces all non transparent pixels with the color specified.
 * @param sineSpeedScale Higher means faster wave animation
 * @param waveHeightScale Smaller means less height */
inline fun Batch.shaderWater(map: EntityMap, tintedColor: FloatArray = floatArrayOf(0f, 0f, 0f), alpha: Float = 1f,
							 sineSpeedScale: Float = 2f, waveHeightScale: Float = 0.7f,
							 waveWidthScale: Float = 1f, f: () -> Unit) {
	flush()
	ShaderProgram.pedantic = false
	shader = waterShader
//    shader.setUniformf("pixelAtlasSize", Vector2(1f / textureRegion.texture.width, 1f / textureRegion.texture.height))
	shader.setUniformf("pixelScreenSize", Vector2(1f / Gdx.graphics.width, 1f / Gdx.graphics.height))
	shader.setUniformf("cameraPos", Vector2(1f / map.screen.camera.leftX().unitsToPixels,
			1f / map.screen.camera.bottomY().unitsToPixels))
	shader.setUniformf("tintedColor", Vector3(tintedColor[0], tintedColor[1], tintedColor[2]))
	shader.setUniformf("alpha", alpha)
	shader.setUniformf("time", map.secondsPassed.toFloat());
	shader.setUniformf("timeScale", sineSpeedScale);
	shader.setUniformf("waveHeightScale", waveHeightScale);
//    println(waveHeightScale)
	shader.setUniformf("waveWidthScale", waveWidthScale);
	f()
	flush()
	shader = null
}

inline fun Batch.shaderShadow(map: EntityMap, originY: Float, textureRegion: TextureRegion, f: () -> Unit) {
	flush()
	ShaderProgram.pedantic = false
	shader = shadowShader
//    shader.setUniformf("pixelAtlasSize", Vector2(1f / textureRegion.texture.width, 1f / textureRegion.texture.height))
	shader.setUniformf("pixelScreenSize", Vector2(1f / Gdx.graphics.width, 1f / Gdx.graphics.height))
	shader.setUniformf("cameraPos", Vector2(1f / map.screen.camera.leftX().unitsToPixels,
			1f / map.screen.camera.bottomY().unitsToPixels))
	shader.setUniformf("originY", originY.unitsToPixels)
	if (textureRegion.isFlipX)
		shader.setUniformf("u_u", Vector2(textureRegion.u2, textureRegion.u))
	else
		shader.setUniformf("u_u", Vector2(textureRegion.u, textureRegion.u2))
	shader.setUniformf("v1_v2", Vector2(textureRegion.v, textureRegion.v2))
	f()
	flush()
	shader = null
}

private fun vector(color: Color) = Vector3(color.r, color.g, color.b)
private val outlineColor = vector(newColorFrom255RGB(187, 0, 178))
private val darkerBaseColor = vector(newColorFrom255RGB(255, 68, 247))
private val baseColor = vector(newColorFrom255RGB(255, 145, 250))
private val tmpv1 = Vector3()
private val tmpv2 = Vector3()
private val tmpv3 = Vector3()

/** Using shaders, replaces all non transparent pixels with the color specified.
 * @param sineSpeedScale Higher means faster wave animation
 * @param waveHeightScale Smaller means less height */
fun Batch.shaderWaterReplaceSkinColorAndTint(map: EntityMap, tintedColor: FloatArray = floatArrayOf(0f, 0f, 0f), alpha: Float = 1f,
											 sineSpeedScale: Float = 2f, waveHeightScale: Float = 0.7f,
											 waveWidthScale: Float = 1f,
											 newBaseColor: Color, newDarkerBaseColor: Color, newOutlineColor: Color,
											 tint: Color = Color.CLEAR,
											 f: () -> Unit) {
	flush()
	ShaderProgram.pedantic = false
	shader = waterReplaceSkinColorAndTint
//    shader.setUniformf("pixelAtlasSize", Vector2(1f / textureRegion.texture.width, 1f / textureRegion.texture.height))
	shader.setUniformf("pixelScreenSize", Vector2(1f / Gdx.graphics.width, 1f / Gdx.graphics.height))
	shader.setUniformf("cameraPos", Vector2(1f / map.screen.camera.leftX().unitsToPixels,
			1f / map.screen.camera.bottomY().unitsToPixels))
	shader.setUniformf("tintedColor", Vector3(tintedColor[0], tintedColor[1], tintedColor[2]))
	shader.setUniformf("alpha", alpha)
	shader.setUniformf("time", map.secondsPassed.toFloat());
	shader.setUniformf("timeScale", sineSpeedScale);
	shader.setUniformf("waveHeightScale", waveHeightScale);
//    println(waveHeightScale)
	shader.setUniformf("waveWidthScale", waveWidthScale);

	tmpv1.set(newOutlineColor.r, newOutlineColor.g, newOutlineColor.b)
	tmpv2.set(newDarkerBaseColor.r, newDarkerBaseColor.g, newDarkerBaseColor.b)
	tmpv3.set(newBaseColor.r, newBaseColor.g, newBaseColor.b)

	shader.setUniformf("outlineColor", outlineColor)
	shader.setUniformf("darkerBaseColor", darkerBaseColor)
	shader.setUniformf("baseColor", baseColor)

	shader.setUniformf("newOutlineColor", tmpv1)
	shader.setUniformf("newDarkerBaseColor", tmpv2)
	shader.setUniformf("newBaseColor", tmpv3)

	tmpColor.set(tint)
	tmpColor.a = 0f
	shader.setUniformf("u_emissive", tmpColor)

	f()
	flush()
	shader = null
}

fun Batch.shaderReplaceSkinColor(newBaseColor: Color, newDarkerBaseColor: Color, newOutlineColor: Color, f: () -> Unit) {
	flush()
	ShaderProgram.pedantic = false
	shader = replaceSkinColor

	tmpv1.set(newOutlineColor.r, newOutlineColor.g, newOutlineColor.b)
	tmpv2.set(newDarkerBaseColor.r, newDarkerBaseColor.g, newDarkerBaseColor.b)
	tmpv3.set(newBaseColor.r, newBaseColor.g, newBaseColor.b)

	shader.setUniformf("outlineColor", outlineColor)
	shader.setUniformf("darkerBaseColor", darkerBaseColor)
	shader.setUniformf("baseColor", baseColor)

	shader.setUniformf("newOutlineColor", tmpv1)
	shader.setUniformf("newDarkerBaseColor", tmpv2)
	shader.setUniformf("newBaseColor", tmpv3)
	f()
	flush()
	shader = null
}

fun Batch.shaderReplaceSkinColorAndTint(newBaseColor: Color, newDarkerBaseColor: Color, newOutlineColor: Color, tint: Color, f: () -> Unit) {
	flush()
	ShaderProgram.pedantic = false
	shader = replaceSkinColorAndTint

	tmpv1.set(newOutlineColor.r, newOutlineColor.g, newOutlineColor.b)
	tmpv2.set(newDarkerBaseColor.r, newDarkerBaseColor.g, newDarkerBaseColor.b)
	tmpv3.set(newBaseColor.r, newBaseColor.g, newBaseColor.b)

	shader.setUniformf("outlineColor", outlineColor)
	shader.setUniformf("darkerBaseColor", darkerBaseColor)
	shader.setUniformf("baseColor", baseColor)

	shader.setUniformf("newOutlineColor", tmpv1)
	shader.setUniformf("newDarkerBaseColor", tmpv2)
	shader.setUniformf("newBaseColor", tmpv3)

	tmpColor.set(tint)
	tmpColor.a = 0f
	shader.setUniformf("u_emissive", tmpColor)
	f()
	flush()
	shader = null
}

private class Shader(vert: String = "default", frag: String = "default") : ShaderProgram(
		Gdx.files.internal("shaders" + File.separator + vert + ".vert"),
		Gdx.files.internal("shaders" + File.separator + frag + ".frag")
)