package com.dcostap.engine.debug

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.NinePatch
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Pool
import com.badlogic.gdx.utils.Pools
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.Engine
import com.dcostap.engine.debug.DebugUI.DebugActor
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.actions.Action
import com.dcostap.engine.utils.screens.BaseScreen
import com.dcostap.engine.utils.ui.ExtLabel
import com.dcostap.engine.utils.ui.ExtTable
import com.dcostap.engine.utils.ui.ResizableActorTable
import com.kotcrab.vis.ui.VisUI
import com.kotcrab.vis.ui.widget.*
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel
import com.kotcrab.vis.ui.widget.spinner.SimpleFloatSpinnerModel
import ktx.actors.onChange
import ktx.collections.*
import java.util.*

/**
 * Created by Darius on 28/12/2017.
 *
 * Used for easier debugging. Allows to quickly draw arbitrary stuff or add Actors to a stage which is rendered above everything else.
 * Can be accessed statically via [Engine].
 *
 * Stage uses ScreenViewport. Has utility methods to quickly add [DebugActor] which follow a
 * position in another viewport, but it is actually positioned in this Stage. Utility methods to add those [DebugActor]
 * with built-in animation or quickly add a Label. Has access to a default statically accessed debug Font for the text.
 *
 * For custom Actor adding, a [ResizableActorTable] is a good easy way of adding Actors or Widgets, while being positioned in arbitrary
 * positions - not following Table's layout (like a Window).
 */
class DebugUI(val engine: Engine) : ScreenAdapter() {
	val stage = Stage(ScreenViewport())

	private val debugCommands = ExtTable()
	private val commandHistory = GdxArray<Pair<String, Boolean>>()
	private val debugCommandField = object : TextField("", VisUI.getSkin()) {
		// add action to when Enter key is pressed on textField
		override fun createInputListener(): InputListener {
			return object : TextFieldClickListener() {
				override fun keyDown(event: InputEvent?, keycode: Int): Boolean {
					if (keycode == Engine.DEBUG_COMMAND_WINDOW_KEY) {
						openCloseDebugCommandsWindow()
					}

					return super.keyDown(event, keycode)
				}
			}
		}
	}

	private val debugCommandUpTable = ExtTable()
	private val debugCommandDownTable = ExtTable()
	private var debugScreenInfoTable = ExtTable()

	var commandsUIClosed = true
		private set

	private val debugFont
		get() = Engine.debugFont

	private val visUInormalFont
		get() = VisUI.getSkin().getFont("default-font")
	private val visUIsmallFont
		get() = VisUI.getSkin().getFont("small-font")

	init {
		debugCommands.also {
			it.setFillParent(true)
			it.center()
			it.add(Table().also { it.bottom(); it.add(debugCommandUpTable) }).growY()
			it.row()
			it.add(debugCommandField)
			it.row()
			it.add(Table().also { it.top(); it.add(debugCommandDownTable).top() }).growY()

			debugCommandField.text = ""
		}
	}

	fun reset() {
		stage.clear()
		closeDebugWindow()
		debugCommandJustInteractedWith = false
		if (!commandsUIClosed) openCloseDebugCommandsWindow()

		debugScreenInfoTable = ExtTable()
		debugScreenInfoTable.also {
			it.updateFunction = { _ -> it.isVisible = Engine.DEBUG && Engine.DEBUG_SCREEN_INFO }

			it.setFillParent(true)
			it.right().top()
			it.pad(5f)

			it.add(ExtLabel(font = debugFont, color = Color.WHITE, alignment = Align.right) {
				var string = "fps: ${Gdx.graphics.framesPerSecond}; rc: ${Engine.renderCalls}\n"
				if (Engine.DEBUG_DELTA_SPEED_CONTROL) string += "   debug speed mult: ${engine.debugDeltaSpeedControl}"
				string += "\n"

				(engine.screen as? BaseScreen)?.let {
					string += it.extraDebugInfoForDebugStage()
				}
				string
			})

			it.row()
			(engine.screen as? BaseScreen)?.extraUIForDebugStage()?.let { table ->
				it.add(table)
			}
		}

		stage.addActor(debugScreenInfoTable)
	}

	private var variableSlidersWindow: VisWindow? = null

	/** unfinished, slider won't work correctly if value changes outside of here */
	fun addDebugVariableSlider(name: String, min: Float, max: Float, stepSize: Float, getValue: () -> Float, updateValue: (Float) -> Unit) {
		if (variableSlidersWindow == null) {
			variableSlidersWindow = object : VisWindow("debug window") {
				init {
					titleLabel.setAlignment(Align.center)
					addCloseButton()
					isMovable = true

					background = NinePatchDrawable(NinePatch((VisUI.getSkin().getDrawable("window") as NinePatchDrawable).patch).also {
						//                    it.color.a = 0.73f
					})
				}
			}

			stage.addActor(variableSlidersWindow.also {

			})
		}

		val variableSlidersWindow = variableSlidersWindow!!

		variableSlidersWindow.add(ExtTable().also {
			it.left()
			it.add(Utils.visUI_spinnerWithSlider(name, min, max, getValue = getValue, stepSize = stepSize) { updateValue(it) })
		}).growX()
		variableSlidersWindow.row()

		variableSlidersWindow.pack()
	}

	fun clearDebugVariableSlider() {
		variableSlidersWindow?.remove()
		variableSlidersWindow = null
	}

	private var debugCommandJustInteractedWith = false
	fun openCloseDebugCommandsWindow() {
		if (debugCommandJustInteractedWith) return
		debugCommandJustInteractedWith = true
		Engine.keyboardFocusInDebugUI = !Engine.keyboardFocusInDebugUI

		engine.actions.addOrReplace("debugUI", Action.RunAfter(0.14f) { debugCommandJustInteractedWith = false })

		debugCommands.remove()

		if (!commandsUIClosed) {
			if (engine.runDebugCommand(debugCommandField.text)) {
				commandHistory.add(Pair(debugCommandField.text, true))
				if (commandHistory.size > 7) commandHistory.removeIndex(0)
			} else {
				if (!debugCommandField.text.isBlank()) {
					commandHistory.add(Pair("(!) " + debugCommandField.text, false))

					debugCommandJustInteractedWith = false
					engine.actions.clearTag("debugUI")

					commandsUIClosed = !commandsUIClosed
					openCloseDebugCommandsWindow()
					return
				}
			}
		} else {
			stage.keyboardFocus = debugCommandField
			stage.addActor(debugCommands)
			debugCommandField.text = ""

			val bg = VisUI.getSkin().getDrawable("window-bg")
			debugCommandDownTable.run {
				background = bg
				clear()
				for (h in commandHistory.reversed()) {
					add(ExtLabel(h.first, Engine.debugFont, if (h.second) Color.SKY else Color.CORAL)).padLeftRight(4f)
					row()
				}
			}

			debugCommandUpTable.background = bg
			debugCommandUpTable.bottom()
		}

		commandsUIClosed = !commandsUIClosed
	}

	fun openCloseDebugWindow() {
		if (debugWindow == null)
			openDebugWindow()
		else
			closeDebugWindow()
	}

	private var debugWindow: Window? = null
	private fun openDebugWindow() {
		val oldLabelFont = ExtLabel.defaultFont
		ExtLabel.defaultFont = visUInormalFont
		ExtLabel.defaultColor = Color.WHITE
		val table = ExtTable()
		val window = object : VisWindow("debug window") {
			init {
				Utils.visUI_defaultFont().data.markupEnabled = true
				titleLabel.setAlignment(Align.center)
				addCloseButton()
				debugWindow = this
				isMovable = true

				background = NinePatchDrawable(NinePatch((VisUI.getSkin().getDrawable("window") as NinePatchDrawable).patch).also {
					//                    it.color.a = 0.73f
				})

				row()

				fun Table.separator() {
					this.add(Separator()).padTopBottom(4f).growX().row()
				}

				add(table.also {
					it.top()
					it.defaults().left().top()

					val oldDefault = ExtLabel.defaultFont
					ExtLabel.defaultFont = visUIsmallFont
					it.row().padTop(10f)

					it.separator()

					var coll1 = CollapsibleWidget(ExtTable().also {
						it.defaults().left()
						it.add(Utils.visUI_customCheckBox("DEBUG_MAP_CELLS", { Engine.DEBUG_MAP_CELLS }).also {
							it.onChange {
								Engine.DEBUG_MAP_CELLS = !Engine.DEBUG_MAP_CELLS
							}
						})

						it.row()
						it.add(Utils.visUI_customCheckBox("DEBUG_MAP_CELLS_SOLID_ONLY", { Engine.DEBUG_MAP_CELLS_SOLID_ONLY }).also {
							it.onChange {
								Engine.DEBUG_MAP_CELLS_SOLID_ONLY = !Engine.DEBUG_MAP_CELLS_SOLID_ONLY
							}
						})

						it.row()
						it.add(Utils.visUI_customCheckBox("DEBUG_MAP_CELLS_INFO", { Engine.DEBUG_MAP_CELLS_INFO }).also {
							it.onChange {
								Engine.DEBUG_MAP_CELLS_INFO = !Engine.DEBUG_MAP_CELLS_INFO
							}
						})

						it.row().padTop(10f)
						it.add(Utils.visUI_customCheckBox("DEBUG_CELL_FLOOD_FILL", { Engine.DEBUG_CELL_FLOOD_FILL }).also {
							it.onChange {
								Engine.DEBUG_CELL_FLOOD_FILL = !Engine.DEBUG_CELL_FLOOD_FILL
							}
						})

						it.row()
						it.add(Utils.visUI_customCheckBox("DEBUG_PATHFINDING", { Engine.DEBUG_PATHFINDING }).also {
							it.onChange {
								Engine.DEBUG_PATHFINDING = !Engine.DEBUG_PATHFINDING
							}
						})
					}, false)

					it.add(ExtLabel(font = Utils.visUI_defaultFont(), alignment = Align.center) { "fps: " + Gdx.graphics.framesPerSecond.toString() }).growX()
					it.row()
					it.add(VisCheckBox("[CYAN]Map cells", true).also { it.onChange { coll1.isCollapsed = !coll1.isCollapsed } })
					it.row(); it.add(coll1).growX(); it.row(); it.separator()

					var coll2 = CollapsibleWidget(ExtTable().also {
						it.defaults().left()
						it.add(Table().also {
							it.left()
							it.defaults().left()
							it.add(Table().also {
								it.defaults().left().growX()
								it.add(Utils.visUI_customCheckBox("DEBUG_ENTITIES_BB", { Engine.DEBUG_ENTITIES_BB }).also {
									it.onChange {
										Engine.DEBUG_ENTITIES_BB = !Engine.DEBUG_ENTITIES_BB
									}
								})

								it.add(Utils.visUI_customCheckBox("X-RAY", { Engine.DEBUG_ENTITIES_BB_X_RAY }).also {
									it.onChange {
										Engine.DEBUG_ENTITIES_BB_X_RAY = !Engine.DEBUG_ENTITIES_BB_X_RAY
									}
								})
							})
							it.row()
							it.add(Utils.visUI_customCheckBox("ONLY DYNAMIC ENTS", { Engine.DEBUG_ONLY_DYNAMIC_ENTS }).also {
								it.onChange {
									Engine.DEBUG_ONLY_DYNAMIC_ENTS = !Engine.DEBUG_ONLY_DYNAMIC_ENTS
								}
							})
							it.row()
							it.add(Utils.visUI_customCheckBox("ONLY PRIORITY ENTS", { Engine.DEBUG_ONLY_PRIORITY_ENTS }).also {
								it.onChange {
									Engine.DEBUG_ONLY_PRIORITY_ENTS = !Engine.DEBUG_ONLY_PRIORITY_ENTS
								}
							})

							it.row()

							it.add(Utils.visUI_customCheckBox("ENTITY_PHYSICS", { Engine.DEBUG_ENTITY_PHYSICS }).also {
								it.onChange {
									Engine.DEBUG_ENTITY_PHYSICS = !Engine.DEBUG_ENTITY_PHYSICS
								}
							})

							it.row().padTop(5f)
							it.add(Utils.visUI_customCheckBox("ENTITY_INFO_POPUP", { Engine.DEBUG_UI_ENTITY_INFO_POPUP }).also {
								it.onChange {
									Engine.DEBUG_UI_ENTITY_INFO_POPUP = !Engine.DEBUG_UI_ENTITY_INFO_POPUP
								}
							})
							it.row()
							it.add(VisLabel("key to pin popups: ${Input.Keys.toString(Engine.DEBUG_UI_ENTITY_POPUP_PIN_KEY)}", Color.SKY))
							it.row().padTop(10f)
							it.add(Utils.visUI_customCheckBox("ENTITY_INFO_NAMES", { Engine.DEBUG_UI_ENTITY_INFO_NAMES }).also {
								it.onChange {
									Engine.DEBUG_UI_ENTITY_INFO_NAMES = !Engine.DEBUG_UI_ENTITY_INFO_NAMES
								}
							})
							it.row()
							it.add(Utils.visUI_customCheckBox("ENTITY_INFO_ABOVE", { Engine.DEBUG_UI_ENTITY_INFO_ABOVE }).also {
								it.onChange {
									Engine.DEBUG_UI_ENTITY_INFO_ABOVE = !Engine.DEBUG_UI_ENTITY_INFO_ABOVE
								}
							})
						})
					}, false)
					it.add(VisCheckBox("[CYAN]Entities", true).also { it.onChange { coll2.isCollapsed = !coll2.isCollapsed } })
					it.row(); it.add(coll2).growX(); it.row(); it.separator()

					it.row().padTop(10f)
					it.add(Utils.visUI_customCheckBox("DEUBG UI DRAWING", { Engine.DEBUG_DEBUGUI_DRAWING }).also {
						it.onChange {
							Engine.DEBUG_DEBUGUI_DRAWING = !Engine.DEBUG_DEBUGUI_DRAWING
						}
					})

					it.row().padTop(10f)
					it.add(Utils.visUI_customCheckBox("COLLISION_TREE_UPDATES", { Engine.DEBUG_COLLISION_TREE_UPDATES }).also {
						it.onChange {
							Engine.DEBUG_COLLISION_TREE_UPDATES = !Engine.DEBUG_COLLISION_TREE_UPDATES
						}
					})

					it.row()
					it.add(Utils.visUI_customCheckBox("COLLISION_TREE_CELLS", { Engine.DEBUG_COLLISION_TREE_CELLS }).also {
						it.onChange {
							Engine.DEBUG_COLLISION_TREE_CELLS = !Engine.DEBUG_COLLISION_TREE_CELLS
						}
					})

					it.row()
					it.row()
					it.add(Table().also {
						it.add(VisLabel("debugFloat: "))
						it.add(Utils.visUI_customFloatSpinner("debugFloat",
								SimpleFloatSpinnerModel(Engine.debugFloat, -1000f, 1000f, 0.05f, 3),
								{ Engine.debugFloat = it }, { Engine.debugFloat }))
					})

					it.row()
					it.add(Table().also {
						it.add(VisLabel("debugInt: "))
						it.add(Utils.visUI_customIntSpinner("debugInt",
								IntSpinnerModel(Engine.debugInt, -1000, 1000, 1),
								{ Engine.debugInt = it }, { Engine.debugInt }))
					})

					ExtLabel.defaultFont = oldDefault
				}).growY()
			}

			override fun close() {
				closeDebugWindow()
			}
		}

		stage.addActor(window)

		(engine.screen as? BaseScreen)?.openDebugWindow(table)
		window.pack()

		stage.keyboardFocus = null

		ExtLabel.defaultFont = oldLabelFont
	}

	private fun closeDebugWindow() {
		debugWindow?.remove()
		debugWindow = null
	}

	class DebugActor(val table: ResizableActorTable, val worldX: Float, val worldY: Float, val worldViewport: Viewport?, val projectToWorld: Boolean = true)

	private val debugActors = GdxArray<DebugActor>()

	fun addDebugActorInWorld(actor: Actor, worldX: Float, worldY: Float, worldViewport: Viewport) {
		ResizableActorTable().also {
			stage.addActor(it)
			it.add(actor)

			debugActors.add(DebugActor(it, worldX, worldY, worldViewport))
		}
	}

	private fun Table.scaleInFadeOut(duration: Float, fadeOutDuration: Float) {
		this.isTransform = true
		this.setScale(0f)
		this.addAction(Actions.sequence(
				Actions.scaleTo(1f, 1f, 0.2f, Interpolation.pow2),
				Actions.delay(duration), Actions.fadeOut(fadeOutDuration, Interpolation.pow2),
				Actions.run { this.remove() }))
	}

	fun addDebugActorInWorldScaleInFadeOut(actor: Actor, worldX: Float, worldY: Float, worldViewport: Viewport,
										   duration: Float = 2f, fadeOutDuration: Float = 1f) {
		ResizableActorTable().also {
			stage.addActor(it)
			it.add(actor)

			debugActors.add(DebugActor(it, worldX, worldY, worldViewport))

			it.scaleInFadeOut(duration, fadeOutDuration)
		}
	}

	fun addDebugLabelInWorldScaleInFadeOut(text: String, color: Color, worldX: Float, worldY: Float, worldViewport: Viewport,
										   duration: Float = 2f, fadeOutDuration: Float = 1f) {
		this.addDebugActorInWorldScaleInFadeOut(ExtLabel(text, Engine.debugFont, color),
				worldX, worldY, worldViewport, duration, fadeOutDuration)
	}

	fun addDebugActor(actor: Actor, stageX: Float, stageY: Float) {
		ResizableActorTable().also {
			stage.addActor(it)
			it.add(actor)
			it.setPosition(stageX, stageY)

			debugActors.add(DebugActor(it, stageX, stageY, null, false))
		}
	}

	fun addDebugActorScaleInFadeOut(actor: Actor, stageX: Float, stageY: Float, duration: Float = 2f, fadeOutDuration: Float = 1f) {
		ResizableActorTable().also {
			stage.addActor(it)
			it.add(actor)
			it.setPosition(stageX, stageY)

			debugActors.add(DebugActor(it, stageX, stageY, null, false))

			it.scaleInFadeOut(duration, fadeOutDuration)
		}
	}

	fun addDebugLabel(text: String, color: Color, stageX: Float, stageY: Float) {
		this.addDebugActor(ExtLabel(text, Engine.debugFont, color), stageX, stageY)
	}

	fun addDebugLabelScaleInFadeOut(text: String, color: Color, stageX: Float, stageY: Float, duration: Float = 2f, fadeOutDuration: Float = 1f) {
		this.addDebugActorScaleInFadeOut(ExtLabel(text, Engine.debugFont, color), stageX, stageY, duration, fadeOutDuration)
	}

	init {
		// make it poolable
		Pools.set(TextDrawOrder::class.java, object : Pool<TextDrawOrder>(5) {
			override fun newObject(): TextDrawOrder {
				return TextDrawOrder()
			}
		})

		Pools.set(LineDrawOrder::class.java, object : Pool<LineDrawOrder>(5) {
			override fun newObject(): LineDrawOrder {
				return LineDrawOrder()
			}
		})

		Pools.set(RectangleDrawOrder::class.java, object : Pool<RectangleDrawOrder>(5) {
			override fun newObject(): RectangleDrawOrder {
				return RectangleDrawOrder()
			}
		})

		Pools.set(CircleDrawOrder::class.java, object : Pool<CircleDrawOrder>(5) {
			override fun newObject(): CircleDrawOrder {
				return CircleDrawOrder()
			}
		})
	}

	private inner class TextDrawOrder() {
		var text: Any? = "";
		var x = 0f;
		var y = 0f;
		var color: Color = Color.WHITE
		var alpha = 1f;
		var worldViewport: Viewport? = null
		var disappearTime = 0f;
		var currentTime = 0f

		var finalX = 0f;
		var finalY = 0f

		fun update() {
			if (worldViewport == null) return
			val pos = Utils.projectPosition(x, y, worldViewport, stage.viewport)
			finalX = pos.x; finalY = pos.y
		}
	}

	private inner class CircleDrawOrder() {
		var x = 0f;
		var y = 0f;
		var color = Color.WHITE;
		var radius = 0f
		var alpha = 1f;
		var worldViewport: Viewport? = null
		var disappearTime = 0f;
		var currentTime = 0f

		var finalX = 0f;
		var finalY = 0f;
		var finalRadius = 0f

		fun update() {
			if (worldViewport == null) return
			var pos = Utils.projectPosition(x, y, worldViewport, stage.viewport)
			finalX = pos.x; finalY = pos.y
			pos = Utils.projectPosition(x + radius, y, worldViewport, stage.viewport)
			finalRadius = finalX - pos.x
		}
	}

	/** All values are in world coords */
	private inner class LineDrawOrder() {
		var originX: Float = 0f;
		var originY: Float = 0f;
		var objectiveX: Float = 0f;
		var objectiveY: Float = 0f;
		var isArrow: Boolean = false;
		var thickness: Float = 1f;
		var color: Color = Color.WHITE;
		var disappearTime: Float = 0f;
		var currentTime: Float = 0f
		var alpha: Float = 1f;
		var worldViewport: Viewport? = null

		var finalOriginX: Float = 0f;
		var finalOriginY: Float = 0f;
		var finalObjectiveX: Float = 0f;
		var finalObjectiveY: Float = 0f

		fun update() {
			if (worldViewport == null) return
			var pos = Utils.projectPosition(originX, originY, worldViewport, stage.viewport)
			finalOriginX = pos.x; finalOriginY = pos.y
			pos = Utils.projectPosition(objectiveX, objectiveY, worldViewport, stage.viewport)
			finalObjectiveX = pos.x; finalObjectiveY = pos.y
		}
	}

	private inner class RectangleDrawOrder() {
		var x: Float = 0f;
		var y: Float = 0f;
		var width: Float = 0f;
		var height: Float = 0f;
		var fill: Boolean = true;
		var borderThickness: Float = 1f;
		var color: Color = Color.WHITE
		var disappearTime: Float = 0f;
		var currentTime: Float = 0f;
		var alpha: Float = 1f
		var worldViewport: Viewport? = null

		var finalX: Float = 0f;
		var finalY: Float = 0f;
		var finalWidth: Float = 0f;
		var finalHeight: Float = 0f

		fun update() {
			if (worldViewport == null) return
			var pos = Utils.projectPosition(x, y, worldViewport, stage.viewport)
			finalX = pos.x; finalY = pos.y
			pos = Utils.projectPosition(x + width, y + height, worldViewport, stage.viewport)
			finalWidth = pos.x - finalX; finalHeight = pos.y - finalY
		}
	}

	private val textDrawStack = GdxArray<TextDrawOrder>()
	fun drawTextInWorld(text: Any?, worldX: Float, worldY: Float, worldViewport: Viewport,
						color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		if (!Engine.DEBUG || !Engine.DEBUG_DEBUGUI_DRAWING) return
		val order = Pools.obtain(TextDrawOrder::class.java)
		order.text = text; order.x = worldX; order.y = worldY
		order.color = color
		order.alpha = alpha
		order.currentTime = disappearTime
		order.disappearTime = disappearTime
		order.worldViewport = worldViewport
		textDrawStack.add(order)
	}

	private val circleDrawStack = GdxArray<CircleDrawOrder>()
	fun drawCircleInWorld(worldX: Float, worldY: Float, radius: Float, worldViewport: Viewport,
						  color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		if (!Engine.DEBUG || !Engine.DEBUG_DEBUGUI_DRAWING) return
		val order = Pools.obtain(CircleDrawOrder::class.java)
		order.radius = radius; order.x = worldX; order.y = worldY
		order.color = color
		order.alpha = alpha
		order.currentTime = disappearTime
		order.disappearTime = disappearTime
		order.worldViewport = worldViewport
		circleDrawStack.add(order)
	}

	private val lineDrawStack = GdxArray<LineDrawOrder>()
	fun drawLineInWorld(x1: Float, y1: Float, x2: Float, y2: Float, worldViewport: Viewport,
						thickness: Float = 1.1f, isArrow: Boolean = false, color: Color = Color.WHITE, alpha: Float = 1f,
						disappearTime: Float = 0f) {
		if (!Engine.DEBUG || !Engine.DEBUG_DEBUGUI_DRAWING) return
		val order = Pools.obtain(LineDrawOrder::class.java)
		order.originX = x1; order.originY = y1
		order.objectiveX = x2; order.objectiveY = y2
		order.thickness = thickness
		order.color = color
		order.currentTime = disappearTime
		order.disappearTime = disappearTime
		order.isArrow = isArrow
		order.alpha = alpha
		order.worldViewport = worldViewport
		lineDrawStack.add(order)
	}

	private val rectangleDrawStack = GdxArray<RectangleDrawOrder>()

	fun drawRectInWorld(x: Number = 0f, y: Number = 0f, width: Number = 0f, height: Number = 0f, worldViewport: Viewport, fill: Boolean = true,
						borderThickness: Float = Engine.DEBUG_LINE_THICKNESS, color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		if (!Engine.DEBUG || !Engine.DEBUG_DEBUGUI_DRAWING) return
		val order = Pools.obtain(RectangleDrawOrder::class.java)
		order.x = x.toFloat(); order.y = y.toFloat(); order.width = width.toFloat(); order.height = height.toFloat()
		order.borderThickness = borderThickness.unitsToPixels
		order.color = color
		order.fill = fill
		order.currentTime = disappearTime
		order.disappearTime = disappearTime
		order.worldViewport = worldViewport
		order.alpha = alpha
		rectangleDrawStack.add(order)
	}

	fun drawRectInWorld(rectangle: Rectangle, worldViewport: Viewport, fill: Boolean = true,
						borderThickness: Float = Engine.DEBUG_LINE_THICKNESS, color: Color = Color.WHITE, alpha: Float = 1f, disappearTime: Float = 0f) {
		this.drawRectInWorld(rectangle.x, rectangle.y, rectangle.width, rectangle.height, worldViewport, fill, borderThickness, color, alpha, disappearTime)
	}

	private val gd = GameDrawer(stage.batch, engine.assets, stage.viewport)

	private val drawOrders = Stack<(GameDrawer) -> Unit>()

	fun drawInStage(draw: (GameDrawer) -> Unit) {
		if (!Engine.DEBUG || !Engine.DEBUG_DEBUGUI_DRAWING) return
		drawOrders.push(draw)
	}

	init {
		gd.useCustomPPM = true
		gd.customPPM = 1
	}

	private var lastCommandCached = "-"
	private val toBeRemoved = GdxArray<DebugActor>()
	private val toBeRemovedLines = GdxArray<LineDrawOrder>()
	private val toBeRemovedRect = GdxArray<RectangleDrawOrder>()
	private val toBeRemovedText = GdxArray<TextDrawOrder>()
	private val toBeRemovedCircle = GdxArray<CircleDrawOrder>()
	override fun render(delta: Float) {
		super.render(delta)

		toBeRemoved.clear()
		for (actor in debugActors) {
			if (actor.table.stage == null || !actor.table.hasChildren()) {
				toBeRemoved.add(actor)
				continue
			}

			if (actor.projectToWorld) {
				val newPos = Utils.projectPosition(actor.worldX, actor.worldY, actor.worldViewport, stage.viewport)
				actor.table.setPosition(newPos.x, newPos.y)
			}
		}

		for (actor in toBeRemoved) {
			debugActors.removeValue(actor, true)
		}

		stage.viewport.apply()

		stage.batch.projectionMatrix = stage.camera.combined
		stage.batch.use {
			gd.reset()
			while (!drawOrders.isEmpty()) {
				drawOrders.pop()(gd)
			}

			for (text in textDrawStack) {
				gd.color = text.color

				val alpha = if (text.currentTime <= 0f) text.alpha
				else {
					if (text.currentTime >= text.disappearTime / 1.3f)
						map(text.currentTime, text.disappearTime / 1.3f, text.disappearTime, text.alpha, 0f)
					else
						map(text.currentTime, 0f, text.disappearTime / 2f, 0f, text.alpha)
				}

				text.currentTime -= delta
				gd.alpha = alpha
				text.update()
				gd.drawText(text.text, text.finalX, text.finalY, Engine.debugFont,
						scaleX = Engine.debugFont.scaleX, scaleY = Engine.debugFont.scaleY)
				if (text.currentTime <= 0f) {
					toBeRemovedText.add(text)
					Pools.free(text)
				}
			}

			textDrawStack.removeAll(toBeRemovedText, true)
			toBeRemovedText.clear()

			for (circle in circleDrawStack) {
				gd.color = circle.color

				val alpha = if (circle.currentTime <= 0f) circle.alpha
				else {
					if (circle.currentTime >= circle.disappearTime / 1.3f)
						map(circle.currentTime, circle.disappearTime / 1.3f, circle.disappearTime, circle.alpha, 0f)
					else
						map(circle.currentTime, 0f, circle.disappearTime / 2f, 0f, circle.alpha)
				}

				circle.currentTime -= delta
				gd.alpha = alpha
				circle.update()
				gd.drawCircle(circle.finalX, circle.finalY, circle.finalRadius)

				if (circle.currentTime <= 0f) {
					toBeRemovedCircle.add(circle)
					Pools.free(circle)
				}
			}

			circleDrawStack.removeAll(toBeRemovedCircle, true)
			toBeRemovedCircle.clear()

			for (line in lineDrawStack) {
				gd.color = line.color

				val alpha = if (line.currentTime <= 0f) line.alpha
				else {
					if (line.currentTime >= line.disappearTime / 1.3f)
						map(line.currentTime, line.disappearTime / 1.3f, line.disappearTime, line.alpha, 0f)
					else
						map(line.currentTime, 0f, line.disappearTime / 2f, 0f, line.alpha)
				}

				line.currentTime -= delta
				gd.alpha = alpha
				line.update()
				gd.drawLine(line.finalOriginX, line.finalOriginY, line.finalObjectiveX, line.finalObjectiveY, line.thickness, line.isArrow,
						arrowSize = line.thickness * 8)
				if (line.currentTime <= 0f) {
					toBeRemovedLines.add(line)
					Pools.free(line)
				}
			}

			lineDrawStack.removeAll(toBeRemovedLines, true)
			toBeRemovedLines.clear()

			for (rect in rectangleDrawStack) {
				gd.color = rect.color
				val alpha = if (rect.currentTime <= 0f) rect.alpha
				else {
					if (rect.currentTime >= rect.disappearTime / 1.3f)
						map(rect.currentTime, rect.disappearTime / 1.3f, rect.disappearTime, rect.alpha, 0f)
					else
						map(rect.currentTime, 0f, rect.disappearTime / 2f, 0f, rect.alpha)
				}

				rect.currentTime -= delta
				gd.alpha = alpha
				rect.update()
				gd.drawRectangle(rect.finalX, rect.finalY, rect.finalWidth, rect.finalHeight, rect.fill, rect.borderThickness)

				if (rect.currentTime <= 0f) {
					toBeRemovedRect.add(rect)
					Pools.free(rect)
				}
			}

			rectangleDrawStack.removeAll(toBeRemovedRect, true)
			toBeRemovedRect.clear()
		}

		stage.act(delta)
		stage.draw()

		if (!commandsUIClosed) {
			val s = debugCommandField.text

			if (s == lastCommandCached) {
				return
			}

			lastCommandCached = s
			debugCommandUpTable.clearChildren()

			val childrenOfTable = debugCommandUpTable.children.toArray()
			fun isCommandIn(command: TextDebugCommand) = childrenOfTable.any { actor ->
				actor is Label && command.names.any {
					it == actor.name?.toString()
				}
			}

			fun isCommandIn(command: DebugCommand) = childrenOfTable.any { actor ->
				actor is Label && command.displayString == actor.name?.toString()
			}

			fun isCommandIn(command: String) = childrenOfTable.any { actor ->
				actor is Label && actor.name?.toString() == command
			}

			var times = 0

			fun processCommand(c: DebugCommand) {
				if (c is TextDebugCommand) {
					if (c.names.any { it.startsWith(s, true) }) {
						if (!isCommandIn(c)) {
							var text = ""
							for (n in c.names) text += if (text.isBlank()) n else " / $n"
							debugCommandUpTable.add(ExtLabel(
									text + if (c.description.isNotBlank()) "[CYAN] (${c.description})[]" else "",
									Engine.debugFont.also { it.data.markupEnabled = true }, Color.WHITE, Align.left)).padLeftRight(20f).growX().minWidth(50f)

							if (true) {
								times++

								if (times > 2) {
									debugCommandUpTable.row()
									times = 0
								}
							} else {
								debugCommandUpTable.row()
							}
						}
					}
				} else {
					if (c.displayString.isBlank()) return
					if (c.displayString.startsWith(s, true)) {
						if (!isCommandIn(c)) {
							var text = c.displayString
							debugCommandUpTable.add(ExtLabel(
									text + if (c.description.isNotBlank()) "[CYAN] (${c.description})[]" else "",
									Engine.debugFont.also { it.data.markupEnabled = true }, Color.WHITE, Align.left)).padLeftRight(20f).growX().minWidth(50f)

							if (true) {
								times++

								if (times > 2) {
									debugCommandUpTable.row()
									times = 0
								}
							} else {
								debugCommandUpTable.row()
							}
						}
					}
				}
			}

			for (c in engine.engineDebugCommands) {
				processCommand(c)
			}

			if (engine.debugCommands.isNotEmpty()) {
				debugCommandUpTable.row().padTop(32f)

				times = 0
				for (c in engine.debugCommands) processCommand(c)
			}

			for (child in childrenOfTable) {
				if (child is Label) {
					if (!isCommandIn(child.text.toString())) child.remove()
				}
			}
		} else {
			lastCommandCached = "-"
		}
	}

	override fun resize(width: Int, height: Int) {
		super.resize(width, height)
		stage.viewport.update(width, height, true)
	}

	override fun dispose() {
		stage.dispose()
	}
}
