package com.dcostap.engine.debug

import com.badlogic.gdx.Gdx

/** Created by Darius on 04-Jan-20. */
private val debugLog = DebugLogger(9999)

fun log(text: Any? = "", type: DebugLogger.Type = DebugLogger.Type.INFO, origin: String = "") {
	debugLog.log(text, type, origin)
}

fun logBug(text: Any? = "", origin: String = "") {
	debugLog.log(text, DebugLogger.Type.BUG, origin)
}

fun logWarning(text: Any?, origin: String = "") {
	debugLog.log(text, DebugLogger.Type.WARNING, origin)
}

fun saveDebugLog(name: String = "debugLog.txt") {
	Gdx.files.local(name).writeString(debugLog.toString(), false)
}

/** Represents a command that may be executed while debugging the game. Success of command is determined by return value of [action]
 * @param displayString used when displaying a preview in UI of the command, and as hint of autocompletion. If empty, it won't be displayed. */
open class DebugCommand(val displayString: String = "", val description: String = "", var action: (String) -> Boolean) {

}

open class TextDebugCommand(vararg names: String, description: String = "", action: () -> Unit) : DebugCommand("", description, { false }) {
	val names = names

	init {
		super.action = {
			if (this.names.contains(it)) {
				action()
				true
			} else
				false

		}
	}
}

/** Execute something based on the first group of a Regex string */
open class RegexDebugCommand(private val regex: Regex, displayString: String, description: String = "", val regexAction: (String) -> Unit)
	: DebugCommand(displayString, description, { false }) {
	init {
		super.action = {
			var finished = false
			regex.find(it)?.groupValues?.get(1)?.let {
				regexAction(it)
				finished = true
			}
			finished
		}
	}
}