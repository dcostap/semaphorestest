package com.dcostap

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonValue
import com.dcostap.engine.debug.*
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.actions.ActionsUpdater
import com.dcostap.engine.utils.font_loaders.smart_font_generator.SmartFontGenerator
import com.dcostap.engine.utils.screens.BaseScreen
import com.dcostap.engine.utils.screens.BaseScreenWithUI
import com.dcostap.game.Main
import com.kotcrab.vis.ui.VisUI
import ktx.collections.*
import java.text.SimpleDateFormat
import java.util.*

class Engine : ApplicationListener {
	private fun loadFirstScreen(): Screen {
		return LoadingScreen(this) { Main(this) }.also { it.startLoading = true }
	}

	companion object Info {
		// region Configuration stuff
		const val IS_RELEASE = true

		/** Pixels per game unit, [BaseScreen] may change this if it specifies [BaseScreen.PPU] */
		var PPU: Int = 1
			get() = if (usingCustomPPU) customPPU else field

		init {
			SmartFontGenerator.fontVersion = "1.0"
			SmartFontGenerator.alwaysRegenerateFonts = false
		}
		// endregion

		// region DEBUG
		/** set to false to make it easier to selectively choose which entities are included in map's colliding trees */
		var ENTITIES_PROVIDE_COLL_INFO_DEFAULT = true

		/** password that, when typed, switches debug mode */
		const val DEBUG_PASSWORD = "DEBUG"
		const val ALLOW_DEBUG_PASSWORD = true

		var MUTE_LOGGING: Boolean = false // log strings won't be printed to System.out, except loading info
		const val PATHFINDING_SEARCH_COUNT_WARNING_THRESHOLD = 300 // how big pathfinding searches need to be for warning logs to be printed

		/** Allows you to stop time and advance frame by frame. Numpad 5 = stop / resume time. Numpad 6 = advance frame */
		var DEBUG_FRAME_CONTROL = false

		/** Modifies the global delta value passed down to the [screen]
		 * Numpad +, -, * = more speed, less speed, reset speed */
		var DEBUG_DELTA_SPEED_CONTROL = true

		var DEBUG_SCREEN_INFO = true

		var DEBUG_COMMAND_WINDOW_KEY = Input.Keys.ENTER

		var DEBUG = true

		var DEBUG_MAP_CELLS = false
		var DEBUG_MAP_CELLS_SOLID_ONLY = false
		var DEBUG_MAP_CELLS_INFO = false

		var DEBUG_DEBUGUI_DRAWING = true

		var DEBUG_SOUNDS = false

		var DEBUG_ENTITIES_BB = false
		var DEBUG_ENTITIES_BB_X_RAY = true // debug drawing of Entities will be above normal drawing

		var DEBUG_ONLY_PRIORITY_ENTS = true
		var DEBUG_ONLY_DYNAMIC_ENTS = true

		var DEBUG_UI_ENTITY_INFO_ABOVE = false
		var DEBUG_UI_ENTITY_INFO_POPUP = true
		var DEBUG_UI_ENTITY_INFO_NAMES = false
		var DEBUG_UI_ENTITY_POPUP_PIN_KEY = Input.Keys.NUMPAD_5

		var DEBUG_ENTITY_PHYSICS = false

		var DEBUG_COLLISION_TREE_CELLS = false
		var DEBUG_COLLISION_TREE_UPDATES = false

		var DEBUG_PATHFINDING = true
		var DEBUG_CELL_FLOOD_FILL = false

		var DEBUG_LINE_THICKNESS = 1.1f
		var DEBUG_TRANSPARENCY = 0.43f

		var debugFloat = 0f
		var debugInt = 0
		// endregion

		//region moar stuff
		/** Resolution Factor: difference of current resolution's width compared to a base width.
		 * If current resolution is double than the base, this would return 2.
		 *
		 * Use it to scale things up to all resolutions similar to a FitViewport.  */
		fun getResolutionFactor(baseResolutionWidth: Float): Float {
			return Gdx.graphics.width / baseResolutionWidth
		}

		val json = Json()
		lateinit var debugUI: DebugUI
			private set
		lateinit var debugFont: BitmapFont
			private set
		lateinit var whitePixel: TextureRegion
			private set
		lateinit var whiteCircle: TextureRegion
			private set

		/** Measures render calls performed on each frame when [render] is called */
		var renderCalls = 0
			private set

		var globalEntityCounter: Int = 0

		var keyboardFocusInDebugUI: Boolean = false
			get() {
				if (!field) return false
				return debugUI.stage.keyboardFocus !is Window
			}

		private var usingCustomPPU = false
		private var customPPU = 1
		//endregion
	}

	/** If true, it will ignore the app's delta */
	var useFixedDelta = false
	var fixedDelta = 1 / 60f

	override fun create() {
		var muteLoggingSetting = MUTE_LOGGING
		MUTE_LOGGING = false // temporary disabling

		if (!IS_RELEASE) {
			ExportedImagesProcessor.processExportedImages()
		}

		//region commands
		engineDebugCommands.addAll(
				DebugCommand {
					it.toIntOrNull().ifNotNull {
						debugInt = it
						return@DebugCommand true
					}
					it.toFloatOrNull().ifNotNull {
						debugFloat = it
						return@DebugCommand true
					}
					false
				},
				TextDebugCommand("debug") { DEBUG = !DEBUG },
				TextDebugCommand("ent") { DEBUG_ENTITIES_BB = !DEBUG_ENTITIES_BB },
				TextDebugCommand("entpop") { DEBUG_UI_ENTITY_INFO_POPUP = !DEBUG_UI_ENTITY_INFO_POPUP; DEBUG_ENTITIES_BB = DEBUG_UI_ENTITY_INFO_POPUP },
				TextDebugCommand("cells") { DEBUG_MAP_CELLS = !DEBUG_MAP_CELLS },
				TextDebugCommand("cellsinfo") { DEBUG_MAP_CELLS_INFO = !DEBUG_MAP_CELLS_INFO },
				TextDebugCommand("atlas", description = "reloads atlas") { reloadAtlas() },
				TextDebugCommand("maps", description = "reloads maps") { assets.loadMaps() },
				TextDebugCommand("window", "win", description = "debug window") { debugUI.openCloseDebugWindow() },
				TextDebugCommand("vsync_off") { Gdx.app.graphics.setVSync(false) },
				TextDebugCommand("vsync_on") { Gdx.app.graphics.setVSync(true) },
				TextDebugCommand("frame", description = "numpad 5, 6 = pause, step 1 frame") { DEBUG_FRAME_CONTROL = !DEBUG_FRAME_CONTROL }
		)
		//endregion

		loadProjectVersion()

		log("version $projectVersion\n")

		batch = SpriteBatch()
		batch.enableBlending()
		fboBatch = SpriteBatch()
		fboBatch.enableBlending()

		VisUI.load("skins/visUI-tinted/x1/tinted.json")

		assets = GameAssets(this)
		assets.initAssetLoading()

		debugUI = DebugUI(this)

		screen = loadFirstScreen()

		MUTE_LOGGING = muteLoggingSetting
	}

	private val frameStepKeyRepeat = KeyRepeat(floatArrayOf(22f, 4f))
	private var frameStep = false
	private var frameStop = false

	var debugDeltaSpeedControl = 1f
		private set

	private val moreSpeedRepeat = KeyRepeat(floatArrayOf(15f, 5f))
	private val lessSpeedRepeat = KeyRepeat(floatArrayOf(15f, 5f))

	override fun render() {
		updatePPU()

		batch.totalRenderCalls = 0
		(debugUI.stage.batch as SpriteBatch).totalRenderCalls = 0
		if (screen != null && screen is BaseScreenWithUI) {
			((screen as BaseScreenWithUI).stage.batch as SpriteBatch).totalRenderCalls = 0
		}

		updateDelta()
		var delta = if (useFixedDelta) fixedDelta else smoothedDelta

		if (DEBUG && DEBUG_FRAME_CONTROL) {
			if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_5)) {
				frameStop = !frameStop
				if (frameStop) frameStep = false
			}
			frameStep = frameStepKeyRepeat.update(Gdx.input.isKeyPressed(Input.Keys.NUMPAD_6), 1f)
		}
		if (!frameStop) {
			if (DEBUG && DEBUG_DELTA_SPEED_CONTROL) {
				if (Gdx.input.isKeyPressed(Input.Keys.STAR)) debugDeltaSpeedControl = 1f
				if (moreSpeedRepeat.update(Gdx.input.isKeyPressed(Input.Keys.PLUS), 1f)) debugDeltaSpeedControl += 0.07f
				if (lessSpeedRepeat.update(Gdx.input.isKeyPressed(Input.Keys.MINUS), 1f)) debugDeltaSpeedControl -= 0.07f

				delta *= debugDeltaSpeedControl
			} else {
				debugDeltaSpeedControl = 1f
			}
		}

		if (frameStop && !frameStep) delta = 0f

		if (DEBUG && Input.Keys.CONTROL_LEFT.isKeyPressed() && Input.Keys.SHIFT_LEFT.isKeyPressed() && Input.Keys.A.isKeyJustPressed()) {
			debugUI.openCloseDebugWindow()
		}

		if (DEBUG && Input.Keys.CONTROL_LEFT.isKeyPressed() && Input.Keys.E.isKeyJustPressed()) {
			debugUI.openCloseDebugWindow()
		}

		actions.update(delta)

		try {
			screen?.render(delta)
		} catch (e: Exception) {
			if (IS_RELEASE)
				saveDebugLog("crashLog-${SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Calendar.getInstance().time)}.txt")
			dispose()

			throw e
		}

		delta = if (useFixedDelta) fixedDelta else smoothedDelta

		if (frameStep) frameStep = false

		if (DEBUG && DEBUG_DEBUGUI_DRAWING) {
//			debugUI.render(delta)
		}

		renderCalls = batch.totalRenderCalls
		renderCalls += (debugUI.stage.batch as SpriteBatch).totalRenderCalls
		if (screen != null && screen is BaseScreenWithUI) {
			renderCalls += ((screen as BaseScreenWithUI).stage.batch as SpriteBatch).totalRenderCalls
		}

		if (DEBUG && Gdx.input.isKeyJustPressed(DEBUG_COMMAND_WINDOW_KEY) && !keyboardFocusInDebugUI) {
//			debugUI.openCloseDebugCommandsWindow()
		}

		if (ALLOW_DEBUG_PASSWORD && debugUI.commandsUIClosed) {
			if (Gdx.input.isKeyJustPressed(Input.Keys.valueOf(DEBUG_PASSWORD[debugCurrentWritten].toString()))) {
				debugCurrentWritten++
				if (debugCurrentWritten >= DEBUG_PASSWORD.length) {
					debugCurrentWritten = 0
					DEBUG = !DEBUG
				}
				debugCurrentWrittenTimer = 1f
			}

			if (debugCurrentWrittenTimer > 0f) {
				debugCurrentWrittenTimer -= delta
				if (debugCurrentWrittenTimer <= 0f) debugCurrentWritten = 0
			}
		}
	}

	fun justLoadedAssets() {
		whitePixel = assets.getWhitePixel()
		whiteCircle = assets.getWhiteCircle()
		debugFont = assets.getDebugFont()
		debugUI.reset()
	}

	lateinit var assets: GameAssets
		private set

	lateinit var fboBatch: SpriteBatch //todo might not be necessary, remove it
		private set

	private fun updatePPU() {
		(screen as? BaseScreen)?.PPU().ifNotNull {
			if (it > 0) {
				usingCustomPPU = true
				customPPU = it
			}
		}
	}

	var screen: Screen? = null
		set(value) {
			screen?.hide()
			field = value
			screen?.show()
			screen?.resize(Gdx.graphics.width, Gdx.graphics.height)
			updatePPU()

			if (assets.isLoaded)
				debugUI.reset()
		}

	override fun pause() {
		screen?.pause()
	}

	override fun resume() {
		screen?.resume()
	}

	lateinit var batch: SpriteBatch
		private set

	var projectName: String? = null
		private set

	var projectDescription: String? = null
		private set

	var projectVersion: String? = null
		private set

	private fun loadProjectVersion(): JsonValue? {
		return JsonReader().parse(Gdx.files.internal("version.json")).also {
			projectVersion = it.getString("version")
			projectName = it.getString("projectName")
			projectDescription = it.getString("description")
		}
	}

	override fun resize(width: Int, height: Int) {
		screen?.resize(width, height)
		debugUI.resize(width, height)
	}

	private var debugCurrentWritten = 0
	private var debugCurrentWrittenTimer = 0f

	fun reloadAtlas() {
		ExportedImagesProcessor.processExportedImages()
		assets.reloadTextureAtlas()
	}

	private var smoothedDelta = 1 / 60f
	private fun updateDelta() {
		// limit delta value
		var delta = Gdx.graphics.deltaTime
		delta = clamp(delta, delta, 1 / 25f)

		// smooth delta to avoid wonky movement at high speeds
		val smoothIncrement = 1 / 1000f
		if (smoothedDelta < delta) {
			smoothedDelta = Math.min(delta, smoothedDelta + smoothIncrement)
		} else if (smoothedDelta > delta) {
			smoothedDelta = Math.max(delta, smoothedDelta - smoothIncrement)
		}
	}

	override fun dispose() {
		assets.dispose()
		batch.dispose()
		screen?.dispose()
		fboBatch.dispose()
		debugUI.dispose()

		disposeShaders()

		VisUI.dispose()
	}

	/** Note that actions added here can't pause its progress. Note that Entities have a local
	 * ActionsUpdater which might be a better fit*/
	val actions = ActionsUpdater()

	/** Don't add anything here, these are specific to the engine */
	val engineDebugCommands = GdxArray<DebugCommand>()

	/** Add your custom commands here */
	val debugCommands = GdxArray<DebugCommand>()

	fun runDebugCommand(string: String): Boolean {
		// region setup
		if (string.isBlank()) return false
		var string = string
		val regex = Regex(".* (x([0-9]+))")
		var times = 1
		regex.getGroup(string, 2)?.also {
			times = it.toInt()
			string = string.replace(regex.getGroup(string, 1)!!, "")
			string = string.trim()
		}
		//endregion

		var success = false
		repeat(times) {
			for (cc in arrayOf(debugCommands, engineDebugCommands))
				for (c in cc)
					if (c.action(string)) {
						success = true; break
					}
		}

		log((if (success) "Executed" else "(!) Unrecognized")
				+ " command '$string'" + if (times == 1) "" else " ($times times)")

		return success
	}
}