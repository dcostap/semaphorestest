package com.dcostap

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.dcostap.engine.Assets
import com.dcostap.engine.map.map_loading.JsonMapInfo
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.font_loaders.smart_font_generator.SmartFontGenerator

class GameAssets(engine: Engine) : Assets(engine) {
	lateinit var fontDare: BitmapFont
		private set

	lateinit var fontDareS: BitmapFont
		private set

	lateinit var fontEquipment: BitmapFont
		private set

	lateinit var fontEquipmentNoShadow: BitmapFont
		private set

	lateinit var fontDareOutline: BitmapFont
		private set

	lateinit var fontDareSmallOutline: BitmapFont
		private set

	lateinit var fontDareSmallOutline2x: BitmapFont
		private set

	lateinit var fontEquipmentOutline: BitmapFont
		private set

	lateinit var fontRoboto: BitmapFont
		private set

	val mapsFolder = "maps"
	val prefabsFolder = "maps/prefabs"
	val objectTemplatesFolder = "maps"
	val tilesetsFolder = "maps/tilesets"

//	lateinit var boom: Boom
//		private set

	override fun loadMaps() {
		super.loadMaps()
	}

	override fun setupAssetManagerLoading() {
		SmartFontGenerator.pageSize = 256
		val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
		parameter.color = Color.WHITE
		parameter.borderColor = Color.BLACK
		parameter.borderStraight = true
		parameter.borderWidth = 1f

//        addFontLoader(FreeTypeFontLoader("fonts/true_type", "darepixel.ttf", fontDarenAtlas, "darePixel",
//                parameter, false, false))
	}

	override fun getDebugFont() = fontDareSmallOutline2x //VisUI.getSkin().getFont("default-font")
	override fun getWhitePixel() = getRegion("pixel")
	override fun getWhiteCircle() = getRegion("circle")

	override fun justFinishedAssetLoading() {
//		boom = Boom.init(true)

		fontDare = loadBitmapFont("darePixel_s10")
		fontDareS = loadBitmapFont("darePixelSmall_s7")
		fontEquipment = loadBitmapFont("equipment_s16")
		fontEquipmentNoShadow = loadBitmapFont("equipment_s16_noShadow")
		fontDareOutline = loadBitmapFont("fontDareOutline")
		fontDareSmallOutline = loadBitmapFont("fontDareSmallOutline")
		fontDareSmallOutline2x = loadBitmapFont("fontDareSmallOutline").also { it.data.setScale(2f) }
		fontEquipmentOutline = loadBitmapFont("equipment_outline")
		fontRoboto = loadBitmapFont("roboto")

		fontDare.setFixedWidthGlyphs("1234567890")
		fontDareS.setFixedWidthGlyphs("1234567890")
//        fontEquipment.setFixedWidthGlyphs("1234567890")

//        fontDare.data.markupEnabled = true

		// pixelated scaling on the font
		fontDare.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)
		fontDareS.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)
		fontEquipment.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)
		fontEquipmentNoShadow.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)

//        fontDarenAtlas.font.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)
//        fontDarenSAtlas.font.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)
//        fontEquipmentAtlas.font.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)
	}
}