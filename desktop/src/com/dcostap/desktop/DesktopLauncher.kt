package com.dcostap.desktop

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import com.dcostap.Engine
import com.dcostap.engine.debug.log
import com.dcostap.engine.utils.ExportedImagesProcessor
import java.nio.charset.Charset
import java.util.*

/** Launches the desktop (LWJGL3) application.  */
object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = Lwjgl3ApplicationConfiguration().apply {
            setWindowedMode(1000, 800)
            useVsync(true)
            setTitle("Darío Costa Pena - Simulación Gráfica Problema Filósofos")
//            setWindowIcon("icons/libgdx128.png", "icons/libgdx64.png", "icons/libgdx32.png", "icons/libgdx16.png")
        }

		log("time: " + System.currentTimeMillis())
		log("default_locale: " + Locale.getDefault())
		log("default_charset: " + Charset.defaultCharset())
		log("default_encoding: " + System.getProperty("file.encoding"))
		log("java_version: " + System.getProperty("java.version"))
		log("os_arch: " + System.getProperty("os.arch"))
		log("os_name: " + System.getProperty("os.name"))
		log("os_version: " + System.getProperty("os.version"))

        Lwjgl3Application(Engine(), config)
    }
}

object UpdateAssets {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = Lwjgl3ApplicationConfiguration().apply {
            setWindowedMode(10, 10)
        }

        Lwjgl3Application(object : ApplicationAdapter() {
            override fun create() {
                ExportedImagesProcessor.processExportedImages()
                Gdx.app.exit()
            }
        }, config)
    }
}